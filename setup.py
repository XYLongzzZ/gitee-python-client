# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


import sys
from setuptools import setup, find_packages

NAME = "gitee-client"
VERSION = "1.0.0"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["urllib3 >= 1.15", "six >= 1.10", "certifi", "python-dateutil"]

setup(
    name=NAME,
    version=VERSION,
    description="码云 Open API",
    author_email="",
    url="",
    keywords=["Swagger", "码云 Open API"],
    install_requires=REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    long_description="""\

    """
)

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.users_api import UsersApi


class TestUsersApi(unittest.TestCase):
    """ UsersApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.users_api.UsersApi()

    def tearDown(self):
        pass

    def test_delete_v5_user_following_username(self):
        """
        Test case for delete_v5_user_following_username

        取消关注一个用户
        """
        pass

    def test_delete_v5_user_keys_id(self):
        """
        Test case for delete_v5_user_keys_id

        删除一个公钥
        """
        pass

    def test_delete_v5_user_unconfirmed_email(self):
        """
        Test case for delete_v5_user_unconfirmed_email

        删除授权用户未激活的邮箱地址
        """
        pass

    def test_get_v5_user(self):
        """
        Test case for get_v5_user

        获取授权用户的资料
        """
        pass

    def test_get_v5_user_address(self):
        """
        Test case for get_v5_user_address

        获取授权用户的地理信息
        """
        pass

    def test_get_v5_user_emails(self):
        """
        Test case for get_v5_user_emails

        获取授权用户的邮箱地址
        """
        pass

    def test_get_v5_user_followers(self):
        """
        Test case for get_v5_user_followers

        列出授权用户的关注者
        """

        # create an instance of the API class
        api_instance = gitee_client.UsersApi()
        access_token = 'access_token_example'  # str | 用户授权码 (optional)
        page = 1  # int | 当前的页码 (optional) (default to 1)
        per_page = 20  # int | 每页的数量 (optional) (default to 20)

        try:
            # 列出授权用户的关注者
            api_response = api_instance.get_v5_user_followers(access_token=access_token, page=page, per_page=per_page)
            print(api_response)
        except ApiException as e:
            print("Exception when calling UsersApi->get_v5_user_followers: %s\n" % e)
        pass

    def test_get_v5_user_following(self):
        """
        Test case for get_v5_user_following

        列出授权用户正关注的用户
        """
        pass

    def test_get_v5_user_following_username(self):
        """
        Test case for get_v5_user_following_username

        检查授权用户是否关注了一个用户
        """
        pass

    def test_get_v5_user_keys(self):
        """
        Test case for get_v5_user_keys

        列出授权用户的所有公钥
        """
        pass

    def test_get_v5_user_keys_id(self):
        """
        Test case for get_v5_user_keys_id

        获取一个公钥
        """
        pass

    def test_get_v5_users_username(self):
        """
        Test case for get_v5_users_username

        获取一个用户
        """
        pass

    def test_get_v5_users_username_followers(self):
        """
        Test case for get_v5_users_username_followers

        列出指定用户的关注者
        """
        pass

    def test_get_v5_users_username_following(self):
        """
        Test case for get_v5_users_username_following

        列出指定用户正在关注的用户
        """
        pass

    def test_get_v5_users_username_following_target_user(self):
        """
        Test case for get_v5_users_username_following_target_user

        检查指定用户是否关注目标用户
        """
        pass

    def test_get_v5_users_username_keys(self):
        """
        Test case for get_v5_users_username_keys

        列出指定用户的所有公钥
        """
        pass

    def test_patch_v5_user(self):
        """
        Test case for patch_v5_user

        更新授权用户的资料
        """
        pass

    def test_patch_v5_user_address(self):
        """
        Test case for patch_v5_user_address

        更新授权用户的地理信息
        """
        pass

    def test_post_v5_user_emails(self):
        """
        Test case for post_v5_user_emails

        添加授权用户的新邮箱地址
        """
        pass

    def test_post_v5_user_keys(self):
        """
        Test case for post_v5_user_keys

        添加一个公钥
        """
        pass

    def test_put_v5_user_following_username(self):
        """
        Test case for put_v5_user_following_username

        关注一个用户
        """
        pass


if __name__ == '__main__':
    unittest.main()

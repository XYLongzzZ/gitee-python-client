# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.milestones_api import MilestonesApi


class TestMilestonesApi(unittest.TestCase):
    """ MilestonesApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.milestones_api.MilestonesApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo_milestones_number(self):
        """
        Test case for delete_v5_repos_owner_repo_milestones_number

        删除项目单个里程碑
        """
        pass

    def test_get_v5_repos_owner_repo_milestones(self):
        """
        Test case for get_v5_repos_owner_repo_milestones

        获取项目所有里程碑
        """
        pass

    def test_get_v5_repos_owner_repo_milestones_number(self):
        """
        Test case for get_v5_repos_owner_repo_milestones_number

        获取项目单个里程碑
        """
        pass

    def test_patch_v5_repos_owner_repo_milestones_number(self):
        """
        Test case for patch_v5_repos_owner_repo_milestones_number

        更新项目里程碑
        """
        pass

    def test_post_v5_repos_owner_repo_milestones(self):
        """
        Test case for post_v5_repos_owner_repo_milestones

        创建项目里程碑
        """
        pass


if __name__ == '__main__':
    unittest.main()

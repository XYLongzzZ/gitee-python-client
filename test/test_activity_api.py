# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.activity_api import ActivityApi


class TestActivityApi(unittest.TestCase):
    """ ActivityApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.activity_api.ActivityApi()

    def tearDown(self):
        pass

    def test_delete_v5_user_starred_owner_repo(self):
        """
        Test case for delete_v5_user_starred_owner_repo

        取消 star 一个项目
        """
        pass

    def test_delete_v5_user_subscriptions_owner_repo(self):
        """
        Test case for delete_v5_user_subscriptions_owner_repo

        取消 watch 一个项目
        """
        pass

    def test_get_v5_events(self):
        """
        Test case for get_v5_events

        获取站内所有公开动态
        """
        pass

    def test_get_v5_networks_owner_repo_events(self):
        """
        Test case for get_v5_networks_owner_repo_events

        列出项目的所有公开动态
        """
        pass

    def test_get_v5_notifications_messages(self):
        """
        Test case for get_v5_notifications_messages

        列出授权用户的所有私信
        """
        pass

    def test_get_v5_notifications_messages_id(self):
        """
        Test case for get_v5_notifications_messages_id

        获取一个私信
        """
        pass

    def test_get_v5_notifications_threads(self):
        """
        Test case for get_v5_notifications_threads

        列出授权用户的所有通知
        """
        pass

    def test_get_v5_notifications_threads_id(self):
        """
        Test case for get_v5_notifications_threads_id

        获取一个通知
        """
        pass

    def test_get_v5_orgs_org_events(self):
        """
        Test case for get_v5_orgs_org_events

        列出组织的公开动态
        """
        pass

    def test_get_v5_repos_owner_repo_events(self):
        """
        Test case for get_v5_repos_owner_repo_events

        列出项目的所有动态
        """
        pass

    def test_get_v5_repos_owner_repo_notifications(self):
        """
        Test case for get_v5_repos_owner_repo_notifications

        列出一个项目里的通知
        """
        pass

    def test_get_v5_repos_owner_repo_stargazers(self):
        """
        Test case for get_v5_repos_owner_repo_stargazers

        列出 star 了项目的用户
        """
        pass

    def test_get_v5_repos_owner_repo_subscribers(self):
        """
        Test case for get_v5_repos_owner_repo_subscribers

        列出 watch 了项目的用户
        """
        pass

    def test_get_v5_user_starred(self):
        """
        Test case for get_v5_user_starred

        列出授权用户 star 了的项目
        """
        pass

    def test_get_v5_user_starred_owner_repo(self):
        """
        Test case for get_v5_user_starred_owner_repo

        检查授权用户是否 star 了一个项目
        """
        pass

    def test_get_v5_user_subscriptions(self):
        """
        Test case for get_v5_user_subscriptions

        列出授权用户 watch 了的项目
        """
        pass

    def test_get_v5_user_subscriptions_owner_repo(self):
        """
        Test case for get_v5_user_subscriptions_owner_repo

        检查授权用户是否 watch 了一个项目
        """
        pass

    def test_get_v5_users_username_events(self):
        """
        Test case for get_v5_users_username_events

        列出用户的动态
        """
        pass

    def test_get_v5_users_username_events_orgs_org(self):
        """
        Test case for get_v5_users_username_events_orgs_org

        列出用户所属组织的动态
        """
        pass

    def test_get_v5_users_username_events_public(self):
        """
        Test case for get_v5_users_username_events_public

        列出用户的公开动态
        """
        pass

    def test_get_v5_users_username_received_events(self):
        """
        Test case for get_v5_users_username_received_events

        列出一个用户收到的动态
        """
        pass

    def test_get_v5_users_username_received_events_public(self):
        """
        Test case for get_v5_users_username_received_events_public

        列出一个用户收到的公开动态
        """
        pass

    def test_get_v5_users_username_starred(self):
        """
        Test case for get_v5_users_username_starred

        列出用户 star 了的项目
        """
        pass

    def test_get_v5_users_username_subscriptions(self):
        """
        Test case for get_v5_users_username_subscriptions

        列出用户 watch 了的项目
        """
        pass

    def test_patch_v5_notifications_messages_id(self):
        """
        Test case for patch_v5_notifications_messages_id

        标记一个私信为已读
        """
        pass

    def test_patch_v5_notifications_threads_id(self):
        """
        Test case for patch_v5_notifications_threads_id

        标记一个通知为已读
        """
        pass

    def test_post_v5_notifications_messages(self):
        """
        Test case for post_v5_notifications_messages

        发送私信给指定用户
        """
        pass

    def test_put_v5_notifications_messages(self):
        """
        Test case for put_v5_notifications_messages

        标记所有私信为已读
        """
        pass

    def test_put_v5_notifications_threads(self):
        """
        Test case for put_v5_notifications_threads

        标记所有通知为已读
        """
        pass

    def test_put_v5_repos_owner_repo_notifications(self):
        """
        Test case for put_v5_repos_owner_repo_notifications

        标记一个项目里的通知为已读
        """
        pass

    def test_put_v5_user_starred_owner_repo(self):
        """
        Test case for put_v5_user_starred_owner_repo

        star 一个项目
        """
        pass

    def test_put_v5_user_subscriptions_owner_repo(self):
        """
        Test case for put_v5_user_subscriptions_owner_repo

        watch 一个项目
        """
        pass


if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.issues_api import IssuesApi


class TestIssuesApi(unittest.TestCase):
    """ IssuesApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.issues_api.IssuesApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo_issues_comments_id(self):
        """
        Test case for delete_v5_repos_owner_repo_issues_comments_id

        删除Issue某条评论
        """
        pass

    def test_get_v5_issues(self):
        """
        Test case for get_v5_issues

        获取当前授权用户的所有Issue
        """
        pass

    def test_get_v5_orgs_org_issues(self):
        """
        Test case for get_v5_orgs_org_issues

        获取当前用户某个组织的Issues
        """
        pass

    def test_get_v5_repos_owner_repo_issues(self):
        """
        Test case for get_v5_repos_owner_repo_issues

        项目的所有Issues
        """
        pass

    def test_get_v5_repos_owner_repo_issues_comments(self):
        """
        Test case for get_v5_repos_owner_repo_issues_comments

        获取项目所有Issue的评论
        """
        pass

    def test_get_v5_repos_owner_repo_issues_comments_id(self):
        """
        Test case for get_v5_repos_owner_repo_issues_comments_id

        获取项目Issue某条评论
        """
        pass

    def test_get_v5_repos_owner_repo_issues_number(self):
        """
        Test case for get_v5_repos_owner_repo_issues_number

        项目的某个Issue
        """
        pass

    def test_get_v5_repos_owner_repo_issues_number_comments(self):
        """
        Test case for get_v5_repos_owner_repo_issues_number_comments

        获取项目某个Issue所有的评论
        """
        pass

    def test_get_v5_user_issues(self):
        """
        Test case for get_v5_user_issues

        获取当前授权用户的所有Issues
        """
        pass

    def test_patch_v5_repos_owner_repo_issues_comments_id(self):
        """
        Test case for patch_v5_repos_owner_repo_issues_comments_id

        更新Issue某条评论
        """
        pass

    def test_patch_v5_repos_owner_repo_issues_number(self):
        """
        Test case for patch_v5_repos_owner_repo_issues_number

        更新Issue
        """
        pass

    def test_post_v5_repos_owner_repo_issues(self):
        """
        Test case for post_v5_repos_owner_repo_issues

        创建Issue
        """
        pass

    def test_post_v5_repos_owner_repo_issues_number_comments(self):
        """
        Test case for post_v5_repos_owner_repo_issues_number_comments

        创建某个Issue评论
        """
        pass


if __name__ == '__main__':
    unittest.main()

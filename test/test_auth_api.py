# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""

from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.users_api import UsersApi


class TestAuthApi(unittest.TestCase):
    """ UsersApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.auth_api.AuthApi()

    def tearGetToken(self):
        response = self.api.get_token("邮箱",
                                      "密码",
                                      "回调地址",
                                      "clientID",
                                      "client_secert",
                                      "user_info projects pull_requests issues notes keys hook groups gists")
        print(response["access_token"])
        pass


if __name__ == '__main__':
    unittest.main()

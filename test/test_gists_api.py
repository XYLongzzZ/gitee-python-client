# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.gists_api import GistsApi


class TestGistsApi(unittest.TestCase):
    """ GistsApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.gists_api.GistsApi()

    def tearDown(self):
        pass

    def test_delete_v5_gists_gist_id_comments_id(self):
        """
        Test case for delete_v5_gists_gist_id_comments_id

        删除代码片段的评论
        """
        pass

    def test_delete_v5_gists_id(self):
        """
        Test case for delete_v5_gists_id

        删除该条代码片段
        """
        pass

    def test_delete_v5_gists_id_star(self):
        """
        Test case for delete_v5_gists_id_star

        取消Star代码片段
        """
        pass

    def test_get_v5_gists(self):
        """
        Test case for get_v5_gists

        获取代码片段
        """
        pass

    def test_get_v5_gists_gist_id_comments(self):
        """
        Test case for get_v5_gists_gist_id_comments

        获取代码片段的评论
        """
        pass

    def test_get_v5_gists_gist_id_comments_id(self):
        """
        Test case for get_v5_gists_gist_id_comments_id

        获取单条代码片段的评论
        """
        pass

    def test_get_v5_gists_id(self):
        """
        Test case for get_v5_gists_id

        获取单条代码片段
        """
        pass

    def test_get_v5_gists_id_commits(self):
        """
        Test case for get_v5_gists_id_commits

        获取代码片段的commit
        """
        pass

    def test_get_v5_gists_id_forks(self):
        """
        Test case for get_v5_gists_id_forks

        获取Fork该条代码片段的列表
        """
        pass

    def test_get_v5_gists_id_star(self):
        """
        Test case for get_v5_gists_id_star

        判断代码片段是否已Star
        """
        pass

    def test_get_v5_gists_public(self):
        """
        Test case for get_v5_gists_public

        获取公开的代码片段
        """
        pass

    def test_get_v5_gists_starred(self):
        """
        Test case for get_v5_gists_starred

        获取用户Star的代码片段
        """
        pass

    def test_get_v5_users_username_gists(self):
        """
        Test case for get_v5_users_username_gists

        获取指定用户的公开代码片段
        """
        pass

    def test_patch_v5_gists_gist_id_comments_id(self):
        """
        Test case for patch_v5_gists_gist_id_comments_id

        修改代码片段的评论
        """
        pass

    def test_patch_v5_gists_id(self):
        """
        Test case for patch_v5_gists_id

        修改代码片段
        """
        pass

    def test_post_v5_gists(self):
        """
        Test case for post_v5_gists

        创建代码片段
        """
        pass

    def test_post_v5_gists_gist_id_comments(self):
        """
        Test case for post_v5_gists_gist_id_comments

        增加代码片段的评论
        """
        pass

    def test_post_v5_gists_id_forks(self):
        """
        Test case for post_v5_gists_id_forks

        Fork代码片段
        """
        pass

    def test_put_v5_gists_id_star(self):
        """
        Test case for put_v5_gists_id_star

        Star代码片段
        """
        pass


if __name__ == '__main__':
    unittest.main()

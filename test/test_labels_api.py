# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.labels_api import LabelsApi


class TestLabelsApi(unittest.TestCase):
    """ LabelsApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.labels_api.LabelsApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo_issues_number_labels(self):
        """
        Test case for delete_v5_repos_owner_repo_issues_number_labels

        删除Issue所有标签
        """
        pass

    def test_delete_v5_repos_owner_repo_issues_number_labels_name(self):
        """
        Test case for delete_v5_repos_owner_repo_issues_number_labels_name

        删除Issue标签
        """
        pass

    def test_delete_v5_repos_owner_repo_labels_name(self):
        """
        Test case for delete_v5_repos_owner_repo_labels_name

        删除一个项目标签
        """
        pass

    def test_get_v5_repos_owner_repo_issues_number_labels(self):
        """
        Test case for get_v5_repos_owner_repo_issues_number_labels

        获取项目Issue的所有标签
        """
        pass

    def test_get_v5_repos_owner_repo_labels(self):
        """
        Test case for get_v5_repos_owner_repo_labels

        获取项目所有标签
        """
        pass

    def test_get_v5_repos_owner_repo_labels_name(self):
        """
        Test case for get_v5_repos_owner_repo_labels_name

        根据标签名称获取单个标签
        """
        pass

    def test_patch_v5_repos_owner_repo_labels_original_name(self):
        """
        Test case for patch_v5_repos_owner_repo_labels_original_name

        更新一个项目标签
        """
        pass

    def test_post_v5_repos_owner_repo_issues_number_labels(self):
        """
        Test case for post_v5_repos_owner_repo_issues_number_labels

        创建Issue标签
        """
        pass

    def test_post_v5_repos_owner_repo_labels(self):
        """
        Test case for post_v5_repos_owner_repo_labels

        创建项目标签
        """
        pass

    def test_put_v5_repos_owner_repo_issues_number_labels(self):
        """
        Test case for put_v5_repos_owner_repo_issues_number_labels

        替换Issue所有标签
        """
        pass


if __name__ == '__main__':
    unittest.main()

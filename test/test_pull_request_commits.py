# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.models.pull_request_commits import PullRequestCommits


class TestPullRequestCommits(unittest.TestCase):
    """ PullRequestCommits unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPullRequestCommits(self):
        """
        Test PullRequestCommits
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = gitee_client.models.pull_request_commits.PullRequestCommits()
        pass


if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.repositories_api import RepositoriesApi


class TestRepositoriesApi(unittest.TestCase):
    """ RepositoriesApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.repositories_api.RepositoriesApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo(self):
        """
        Test case for delete_v5_repos_owner_repo

        删除一个项目
        """
        pass

    def test_delete_v5_repos_owner_repo_branches_branch_protection(self):
        """
        Test case for delete_v5_repos_owner_repo_branches_branch_protection

        取消保护分支的设置
        """
        pass

    def test_delete_v5_repos_owner_repo_collaborators_username(self):
        """
        Test case for delete_v5_repos_owner_repo_collaborators_username

        移除项目成员
        """
        pass

    def test_delete_v5_repos_owner_repo_comments_id(self):
        """
        Test case for delete_v5_repos_owner_repo_comments_id

        删除Commit评论
        """
        pass

    def test_delete_v5_repos_owner_repo_contents_path(self):
        """
        Test case for delete_v5_repos_owner_repo_contents_path

        删除文件
        """
        pass

    def test_delete_v5_repos_owner_repo_keys_id(self):
        """
        Test case for delete_v5_repos_owner_repo_keys_id

        删除一个项目公钥
        """
        pass

    def test_delete_v5_repos_owner_repo_releases_id(self):
        """
        Test case for delete_v5_repos_owner_repo_releases_id

        删除项目Release
        """
        pass

    def test_get_v5_orgs_org_repos(self):
        """
        Test case for get_v5_orgs_org_repos

        获取一个组织的项目
        """
        pass

    def test_get_v5_repos_owner_repo(self):
        """
        Test case for get_v5_repos_owner_repo

        列出授权用户的某个项目
        """
        pass

    def test_get_v5_repos_owner_repo_branches(self):
        """
        Test case for get_v5_repos_owner_repo_branches

        获取所有分支
        """
        pass

    def test_get_v5_repos_owner_repo_branches_branch(self):
        """
        Test case for get_v5_repos_owner_repo_branches_branch

        获取单个分支
        """
        pass

    def test_get_v5_repos_owner_repo_collaborators(self):
        """
        Test case for get_v5_repos_owner_repo_collaborators

        获取项目的所有成员
        """
        pass

    def test_get_v5_repos_owner_repo_collaborators_username(self):
        """
        Test case for get_v5_repos_owner_repo_collaborators_username

        判断用户是否为项目成员
        """
        pass

    def test_get_v5_repos_owner_repo_collaborators_username_permission(self):
        """
        Test case for get_v5_repos_owner_repo_collaborators_username_permission

        查看项目成员的权限
        """
        pass

    def test_get_v5_repos_owner_repo_comments(self):
        """
        Test case for get_v5_repos_owner_repo_comments

        获取项目的Commit评论
        """
        pass

    def test_get_v5_repos_owner_repo_comments_id(self):
        """
        Test case for get_v5_repos_owner_repo_comments_id

        获取项目的某条Commit评论
        """
        pass

    def test_get_v5_repos_owner_repo_commits(self):
        """
        Test case for get_v5_repos_owner_repo_commits

        项目的所有提交
        """
        pass

    def test_get_v5_repos_owner_repo_commits_ref_comments(self):
        """
        Test case for get_v5_repos_owner_repo_commits_ref_comments

        获取单个Commit的评论
        """
        pass

    def test_get_v5_repos_owner_repo_commits_sha(self):
        """
        Test case for get_v5_repos_owner_repo_commits_sha

        项目的某个提交
        """
        pass

    def test_get_v5_repos_owner_repo_compare_base___head(self):
        """
        Test case for get_v5_repos_owner_repo_compare_base___head

        两个Commits之间对比的版本差异
        """
        pass

    def test_get_v5_repos_owner_repo_contents_path(self):
        """
        Test case for get_v5_repos_owner_repo_contents_path

        获取仓库具体路径下的内容
        """
        pass

    def test_get_v5_repos_owner_repo_contributors(self):
        """
        Test case for get_v5_repos_owner_repo_contributors

        获取项目贡献者
        """
        pass

    def test_get_v5_repos_owner_repo_forks(self):
        """
        Test case for get_v5_repos_owner_repo_forks

        查看项目的Forks
        """
        pass

    def test_get_v5_repos_owner_repo_keys(self):
        """
        Test case for get_v5_repos_owner_repo_keys

        展示项目的公钥
        """
        pass

    def test_get_v5_repos_owner_repo_keys_id(self):
        """
        Test case for get_v5_repos_owner_repo_keys_id

        获取项目的单个公钥
        """
        pass

    def test_get_v5_repos_owner_repo_pages(self):
        """
        Test case for get_v5_repos_owner_repo_pages

        获取Pages信息
        """
        pass

    def test_get_v5_repos_owner_repo_readme(self):
        """
        Test case for get_v5_repos_owner_repo_readme

        获取仓库README
        """
        pass

    def test_get_v5_repos_owner_repo_releases(self):
        """
        Test case for get_v5_repos_owner_repo_releases

        获取项目的所有Releases
        """
        pass

    def test_get_v5_repos_owner_repo_releases_id(self):
        """
        Test case for get_v5_repos_owner_repo_releases_id

        获取项目的单个Releases
        """
        pass

    def test_get_v5_repos_owner_repo_releases_latest(self):
        """
        Test case for get_v5_repos_owner_repo_releases_latest

        获取项目的最后更新的Release
        """
        pass

    def test_get_v5_repos_owner_repo_releases_tags_tag(self):
        """
        Test case for get_v5_repos_owner_repo_releases_tags_tag

        根据Tag名称获取项目的Release
        """
        pass

    def test_get_v5_repos_owner_repo_tags(self):
        """
        Test case for get_v5_repos_owner_repo_tags

        列出项目所有的tags
        """
        pass

    def test_get_v5_user_repos(self):
        """
        Test case for get_v5_user_repos

        列出授权用户的所有项目
        """
        pass

    def test_get_v5_users_username_repos(self):
        """
        Test case for get_v5_users_username_repos

        获取某个用户的公开项目
        """
        pass

    def test_patch_v5_repos_owner_repo(self):
        """
        Test case for patch_v5_repos_owner_repo

        更新项目设置
        """
        pass

    def test_patch_v5_repos_owner_repo_comments_id(self):
        """
        Test case for patch_v5_repos_owner_repo_comments_id

        更新Commit评论
        """
        pass

    def test_patch_v5_repos_owner_repo_releases_id(self):
        """
        Test case for patch_v5_repos_owner_repo_releases_id

        更新项目Release
        """
        pass

    def test_post_v5_orgs_org_repos(self):
        """
        Test case for post_v5_orgs_org_repos

        创建组织项目
        """
        pass

    def test_post_v5_repos_owner_repo_commits_sha_comments(self):
        """
        Test case for post_v5_repos_owner_repo_commits_sha_comments

        创建Commit评论
        """
        pass

    def test_post_v5_repos_owner_repo_contents_path(self):
        """
        Test case for post_v5_repos_owner_repo_contents_path

        新建文件
        """
        pass

    def test_post_v5_repos_owner_repo_forks(self):
        """
        Test case for post_v5_repos_owner_repo_forks

        Fork一个项目
        """
        pass

    def test_post_v5_repos_owner_repo_keys(self):
        """
        Test case for post_v5_repos_owner_repo_keys

        为项目添加公钥
        """
        pass

    def test_post_v5_repos_owner_repo_pages_builds(self):
        """
        Test case for post_v5_repos_owner_repo_pages_builds

        请求建立Pages
        """
        pass

    def test_post_v5_repos_owner_repo_releases(self):
        """
        Test case for post_v5_repos_owner_repo_releases

        创建项目Release
        """
        pass

    def test_post_v5_user_repos(self):
        """
        Test case for post_v5_user_repos

        创建一个项目
        """
        pass

    def test_put_v5_repos_owner_repo_branches_branch_protection(self):
        """
        Test case for put_v5_repos_owner_repo_branches_branch_protection

        设置分支保护
        """
        pass

    def test_put_v5_repos_owner_repo_collaborators_username(self):
        """
        Test case for put_v5_repos_owner_repo_collaborators_username

        添加项目成员
        """
        pass

    def test_put_v5_repos_owner_repo_contents_path(self):
        """
        Test case for put_v5_repos_owner_repo_contents_path

        更新文件
        """
        pass


if __name__ == '__main__':
    unittest.main()

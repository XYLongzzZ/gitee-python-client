# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.models.pull_request_files import PullRequestFiles


class TestPullRequestFiles(unittest.TestCase):
    """ PullRequestFiles unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPullRequestFiles(self):
        """
        Test PullRequestFiles
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = gitee_client.models.pull_request_files.PullRequestFiles()
        pass


if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.pull_requests_api import PullRequestsApi


class TestPullRequestsApi(unittest.TestCase):
    """ PullRequestsApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.pull_requests_api.PullRequestsApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo_pulls_comments_id(self):
        """
        Test case for delete_v5_repos_owner_repo_pulls_comments_id

        删除评论
        """
        pass

    def test_delete_v5_repos_owner_repo_pulls_number_requested_reviewers(self):
        """
        Test case for delete_v5_repos_owner_repo_pulls_number_requested_reviewers

        移除审查人员
        """
        pass

    def test_get_v5_repos_owner_repo_pulls(self):
        """
        Test case for get_v5_repos_owner_repo_pulls

        获取Pull Request列表
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_comments(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_comments

        获取该项目下的所有Pull Request评论
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_comments_id(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_comments_id

        获取Pull Request的某个评论
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number

        获取单个Pull Request
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number_comments(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number_comments

        获取某个Pull Request的所有评论
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number_commits(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number_commits

        获取某Pull Request的所有Commit信息。最多显示250条Commit
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number_files(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number_files

        Pull Request Commit文件列表。最多显示300条diff
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number_merge(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number_merge

        判断Pull Request是否已经合并
        """
        pass

    def test_get_v5_repos_owner_repo_pulls_number_requested_reviewers(self):
        """
        Test case for get_v5_repos_owner_repo_pulls_number_requested_reviewers

        获取审查人员的列表
        """
        pass

    def test_patch_v5_repos_owner_repo_pulls_comments_id(self):
        """
        Test case for patch_v5_repos_owner_repo_pulls_comments_id

        编辑评论
        """
        pass

    def test_patch_v5_repos_owner_repo_pulls_number(self):
        """
        Test case for patch_v5_repos_owner_repo_pulls_number

        更新Pull Request信息
        """
        pass

    def test_post_v5_repos_owner_repo_pulls(self):
        """
        Test case for post_v5_repos_owner_repo_pulls

        创建Pull Request
        """
        pass

    def test_post_v5_repos_owner_repo_pulls_number_comments(self):
        """
        Test case for post_v5_repos_owner_repo_pulls_number_comments

        提交Pull Request评论
        """
        pass

    def test_post_v5_repos_owner_repo_pulls_number_requested_reviewers(self):
        """
        Test case for post_v5_repos_owner_repo_pulls_number_requested_reviewers

        增加审查人员
        """
        pass

    def test_put_v5_repos_owner_repo_pulls_number_merge(self):
        """
        Test case for put_v5_repos_owner_repo_pulls_number_merge

        合并Pull Request
        """
        pass


if __name__ == '__main__':
    unittest.main()

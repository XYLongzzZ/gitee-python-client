# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.models.content import Content


class TestContent(unittest.TestCase):
    """ Content unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testContent(self):
        """
        Test Content
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = gitee_client.models.content.Content()
        pass


if __name__ == '__main__':
    unittest.main()

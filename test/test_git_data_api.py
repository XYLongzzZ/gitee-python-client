# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.git_data_api import GitDataApi


class TestGitDataApi(unittest.TestCase):
    """ GitDataApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.git_data_api.GitDataApi()

    def tearDown(self):
        pass

    def test_get_v5_repos_owner_repo_git_blobs_sha(self):
        """
        Test case for get_v5_repos_owner_repo_git_blobs_sha

        获取文件Blob
        """
        pass

    def test_get_v5_repos_owner_repo_git_trees_sha(self):
        """
        Test case for get_v5_repos_owner_repo_git_trees_sha

        获取目录Tree
        """
        pass


if __name__ == '__main__':
    unittest.main()

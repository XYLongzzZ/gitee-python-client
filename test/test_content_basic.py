# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.models.content_basic import ContentBasic


class TestContentBasic(unittest.TestCase):
    """ ContentBasic unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testContentBasic(self):
        """
        Test ContentBasic
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = gitee_client.models.content_basic.ContentBasic()
        pass


if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import os
import sys
import unittest

import gitee_client
from gitee_client.rest import ApiException
from gitee_client.apis.webhooks_api import WebhooksApi


class TestWebhooksApi(unittest.TestCase):
    """ WebhooksApi unit test stubs """

    def setUp(self):
        self.api = gitee_client.apis.webhooks_api.WebhooksApi()

    def tearDown(self):
        pass

    def test_delete_v5_repos_owner_repo_hooks_id(self):
        """
        Test case for delete_v5_repos_owner_repo_hooks_id

        删除一个项目WebHook
        """
        pass

    def test_get_v5_repos_owner_repo_hooks(self):
        """
        Test case for get_v5_repos_owner_repo_hooks

        列出项目的WebHooks
        """
        pass

    def test_get_v5_repos_owner_repo_hooks_id(self):
        """
        Test case for get_v5_repos_owner_repo_hooks_id

        获取项目单个WebHook
        """
        pass

    def test_patch_v5_repos_owner_repo_hooks_id(self):
        """
        Test case for patch_v5_repos_owner_repo_hooks_id

        更新一个项目WebHook
        """
        pass

    def test_post_v5_repos_owner_repo_hooks(self):
        """
        Test case for post_v5_repos_owner_repo_hooks

        创建一个项目WebHook
        """
        pass

    def test_post_v5_repos_owner_repo_hooks_id_tests(self):
        """
        Test case for post_v5_repos_owner_repo_hooks_id_tests

        测试WebHook是否发送成功
        """
        pass


if __name__ == '__main__':
    unittest.main()

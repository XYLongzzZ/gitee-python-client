# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class IssuesApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_repos_owner_repo_issues_comments_id(self, owner, repo, id, **kwargs):
        """
        删除Issue某条评论
        删除Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_issues_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def delete_v5_repos_owner_repo_issues_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        删除Issue某条评论
        删除Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_issues_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_repos_owner_repo_issues_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/comments/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_issues(self, **kwargs):
        """
        获取当前授权用户的所有Issue
        获取当前授权用户的所有Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_issues(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_issues_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_issues_with_http_info(**kwargs)
            return data

    def get_v5_issues_with_http_info(self, **kwargs):
        """
        获取当前授权用户的所有Issue
        获取当前授权用户的所有Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_issues_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'filter', 'state', 'labels', 'sort', 'direction', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_issues" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'filter' in params:
            query_params.append(('filter', params['filter']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'labels' in params:
            query_params.append(('labels', params['labels']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/issues', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Issue]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org_issues(self, org, **kwargs):
        """
        获取当前用户某个组织的Issues
        获取当前用户某个组织的Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_issues(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_issues_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_issues_with_http_info(org, **kwargs)
            return data

    def get_v5_orgs_org_issues_with_http_info(self, org, **kwargs):
        """
        获取当前用户某个组织的Issues
        获取当前用户某个组织的Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_issues_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'filter', 'state', 'labels', 'sort', 'direction', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org_issues" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org_issues`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'filter' in params:
            query_params.append(('filter', params['filter']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'labels' in params:
            query_params.append(('labels', params['labels']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/issues', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Issue]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_issues(self, owner, repo, **kwargs):
        """
        项目的所有Issues
        项目的所有Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str milestone: 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
        :param str assignee: 用户的username。 none为没指派者, *为所有带有指派者的
        :param str creator: 创建Issues的用户username
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_issues_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_issues_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_issues_with_http_info(self, owner, repo, **kwargs):
        """
        项目的所有Issues
        项目的所有Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str milestone: 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
        :param str assignee: 用户的username。 none为没指派者, *为所有带有指派者的
        :param str creator: 创建Issues的用户username
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'state', 'labels', 'sort', 'direction', 'since', 'page', 'per_page', 'milestone', 'assignee', 'creator']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_issues" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_issues`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_issues`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'labels' in params:
            query_params.append(('labels', params['labels']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))
        if 'milestone' in params:
            query_params.append(('milestone', params['milestone']))
        if 'assignee' in params:
            query_params.append(('assignee', params['assignee']))
        if 'creator' in params:
            query_params.append(('creator', params['creator']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Issue]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_issues_comments(self, owner, repo, **kwargs):
        """
        获取项目所有Issue的评论
        获取项目所有Issue的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_comments(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: Either created or updated. Default: created
        :param str direction: Either asc or desc. Ignored without the sort parameter.
        :param str since: Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_issues_comments_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_issues_comments_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_issues_comments_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目所有Issue的评论
        获取项目所有Issue的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_comments_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: Either created or updated. Default: created
        :param str direction: Either asc or desc. Ignored without the sort parameter.
        :param str since: Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'sort', 'direction', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_issues_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_issues_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_issues_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_issues_comments_id(self, owner, repo, id, **kwargs):
        """
        获取项目Issue某条评论
        获取项目Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def get_v5_repos_owner_repo_issues_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        获取项目Issue某条评论
        获取项目Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_issues_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_repos_owner_repo_issues_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/comments/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_issues_number(self, owner, repo, number, **kwargs):
        """
        项目的某个Issue
        项目的某个Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_number(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str access_token: 用户授权码
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_issues_number_with_http_info(self, owner, repo, number, **kwargs):
        """
        项目的某个Issue
        项目的某个Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str access_token: 用户授权码
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_issues_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_issues_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_issues_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_issues_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/{number}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Issue',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_issues_number_comments(self, owner, repo, number, **kwargs):
        """
        获取项目某个Issue所有的评论
        获取项目某个Issue所有的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_number_comments(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str access_token: 用户授权码
        :param str since: Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_issues_number_comments_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取项目某个Issue所有的评论
        获取项目某个Issue所有的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str access_token: 用户授权码
        :param str since: Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_issues_number_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_issues_number_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_issues_number_comments`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_issues_number_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/{number}/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_issues(self, **kwargs):
        """
        获取当前授权用户的所有Issues
        获取当前授权用户的所有Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_issues(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_issues_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_issues_with_http_info(**kwargs)
            return data

    def get_v5_user_issues_with_http_info(self, **kwargs):
        """
        获取当前授权用户的所有Issues
        获取当前授权用户的所有Issues
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_issues_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str filter: 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        :param str state: Issue的状态: open, closed, or all。 默认: open
        :param str labels: 用逗号分开的标签。如: bug,performance
        :param str sort: 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        :param str direction: 排序方式: 升序(asc)，降序(desc)。默认: desc
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Issue]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'filter', 'state', 'labels', 'sort', 'direction', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_issues" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'filter' in params:
            query_params.append(('filter', params['filter']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'labels' in params:
            query_params.append(('labels', params['labels']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/issues', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Issue]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_issues_comments_id(self, owner, repo, id, body, **kwargs):
        """
        更新Issue某条评论
        更新Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_issues_comments_id(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: The contents of the comment. (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, body, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, body, **kwargs)
            return data

    def patch_v5_repos_owner_repo_issues_comments_id_with_http_info(self, owner, repo, id, body, **kwargs):
        """
        更新Issue某条评论
        更新Issue某条评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_issues_comments_id_with_http_info(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: The contents of the comment. (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_issues_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_repos_owner_repo_issues_comments_id`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `patch_v5_repos_owner_repo_issues_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/comments/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_issues_number(self, owner, repo, number, title, **kwargs):
        """
        更新Issue
        更新Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_issues_number(owner, repo, number, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str title: Issue标题 (required)
        :param str access_token: 用户授权码
        :param str state: Issue 状态，open、started、closed 或 approved
        :param str body: Issue描述
        :param str assignee: Issue负责人的username
        :param int milestone: 所属里程碑的number(第几个)
        :param list[str] labels: 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, title, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, title, **kwargs)
            return data

    def patch_v5_repos_owner_repo_issues_number_with_http_info(self, owner, repo, number, title, **kwargs):
        """
        更新Issue
        更新Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_issues_number_with_http_info(owner, repo, number, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str title: Issue标题 (required)
        :param str access_token: 用户授权码
        :param str state: Issue 状态，open、started、closed 或 approved
        :param str body: Issue描述
        :param str assignee: Issue负责人的username
        :param int milestone: 所属里程碑的number(第几个)
        :param list[str] labels: 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'title', 'access_token', 'state', 'body', 'assignee', 'milestone', 'labels']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_issues_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_issues_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_issues_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `patch_v5_repos_owner_repo_issues_number`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `patch_v5_repos_owner_repo_issues_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'state' in params:
            form_params.append(('state', params['state']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'assignee' in params:
            form_params.append(('assignee', params['assignee']))
        if 'milestone' in params:
            form_params.append(('milestone', params['milestone']))
        if 'labels' in params:
            form_params.append(('labels', params['labels']))
            collection_formats['labels'] = 'csv'

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/{number}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Issue',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_issues(self, owner, repo, title, **kwargs):
        """
        创建Issue
        创建Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_issues(owner, repo, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: Issue标题 (required)
        :param str access_token: 用户授权码
        :param str body: Issue描述
        :param str assignee: Issue负责人的username
        :param int milestone: 所属里程碑的number(第几个)
        :param list[str] labels: 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_issues_with_http_info(owner, repo, title, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_issues_with_http_info(owner, repo, title, **kwargs)
            return data

    def post_v5_repos_owner_repo_issues_with_http_info(self, owner, repo, title, **kwargs):
        """
        创建Issue
        创建Issue
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_issues_with_http_info(owner, repo, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: Issue标题 (required)
        :param str access_token: 用户授权码
        :param str body: Issue描述
        :param str assignee: Issue负责人的username
        :param int milestone: 所属里程碑的number(第几个)
        :param list[str] labels: 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
        :return: Issue
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'title', 'access_token', 'body', 'assignee', 'milestone', 'labels']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_issues" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_issues`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_issues`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `post_v5_repos_owner_repo_issues`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'assignee' in params:
            form_params.append(('assignee', params['assignee']))
        if 'milestone' in params:
            form_params.append(('milestone', params['milestone']))
        if 'labels' in params:
            form_params.append(('labels', params['labels']))
            collection_formats['labels'] = 'csv'

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Issue',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_issues_number_comments(self, owner, repo, number, body, **kwargs):
        """
        创建某个Issue评论
        创建某个Issue评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_issues_number_comments(owner, repo, number, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str body: The contents of the comment. (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, body, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, body, **kwargs)
            return data

    def post_v5_repos_owner_repo_issues_number_comments_with_http_info(self, owner, repo, number, body, **kwargs):
        """
        创建某个Issue评论
        创建某个Issue评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_issues_number_comments_with_http_info(owner, repo, number, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str number: Issue 编号(区分大小写) (required)
        :param str body: The contents of the comment. (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_issues_number_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_issues_number_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_issues_number_comments`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `post_v5_repos_owner_repo_issues_number_comments`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_v5_repos_owner_repo_issues_number_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/issues/{number}/comments', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

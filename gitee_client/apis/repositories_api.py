# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class RepositoriesApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_repos_owner_repo(self, owner, repo, **kwargs):
        """
        删除一个项目
        删除一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def delete_v5_repos_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        删除一个项目
        删除一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_branches_branch_protection(self, owner, repo, branch, **kwargs):
        """
        取消保护分支的设置
        取消保护分支的设置
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, **kwargs)
            return data

    def delete_v5_repos_owner_repo_branches_branch_protection_with_http_info(self, owner, repo, branch, **kwargs):
        """
        取消保护分支的设置
        取消保护分支的设置
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'branch', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_branches_branch_protection" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_branches_branch_protection`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_branches_branch_protection`")
        # verify the required parameter 'branch' is set
        if ('branch' not in params) or (params['branch'] is None):
            raise ValueError("Missing the required parameter `branch` when calling `delete_v5_repos_owner_repo_branches_branch_protection`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'branch' in params:
            path_params['branch'] = params['branch']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/branches/{branch}/protection', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_collaborators_username(self, owner, repo, username, **kwargs):
        """
        移除项目成员
        移除项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_collaborators_username(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, **kwargs)
            return data

    def delete_v5_repos_owner_repo_collaborators_username_with_http_info(self, owner, repo, username, **kwargs):
        """
        移除项目成员
        移除项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_collaborators_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `delete_v5_repos_owner_repo_collaborators_username`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/collaborators/{username}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_comments_id(self, owner, repo, id, **kwargs):
        """
        删除Commit评论
        删除Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def delete_v5_repos_owner_repo_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        删除Commit评论
        删除Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_repos_owner_repo_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/comments/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_contents_path(self, owner, repo, path, sha, message, **kwargs):
        """
        删除文件
        删除文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_contents_path(owner, repo, path, sha, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str sha: 文件被替换的sha值 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, sha, message, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, sha, message, **kwargs)
            return data

    def delete_v5_repos_owner_repo_contents_path_with_http_info(self, owner, repo, path, sha, message, **kwargs):
        """
        删除文件
        删除文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, sha, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str sha: 文件被替换的sha值 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'path', 'sha', 'message', 'access_token', 'branch', 'committer_name', 'committer_email', 'author_name', 'author_email']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_contents_path" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'path' is set
        if ('path' not in params) or (params['path'] is None):
            raise ValueError("Missing the required parameter `path` when calling `delete_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'sha' is set
        if ('sha' not in params) or (params['sha'] is None):
            raise ValueError("Missing the required parameter `sha` when calling `delete_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'message' is set
        if ('message' not in params) or (params['message'] is None):
            raise ValueError("Missing the required parameter `message` when calling `delete_v5_repos_owner_repo_contents_path`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'path' in params:
            path_params['path'] = params['path']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sha' in params:
            query_params.append(('sha', params['sha']))
        if 'message' in params:
            query_params.append(('message', params['message']))
        if 'branch' in params:
            query_params.append(('branch', params['branch']))
        if 'committer_name' in params:
            query_params.append(('committer[name]', params['committer_name']))
        if 'committer_email' in params:
            query_params.append(('committer[email]', params['committer_email']))
        if 'author_name' in params:
            query_params.append(('author[name]', params['author_name']))
        if 'author_email' in params:
            query_params.append(('author[email]', params['author_email']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/contents/{path}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CommitContent',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_keys_id(self, owner, repo, id, **kwargs):
        """
        删除一个项目公钥
        删除一个项目公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_keys_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def delete_v5_repos_owner_repo_keys_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        删除一个项目公钥
        删除一个项目公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_keys_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_keys_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_keys_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_repos_owner_repo_keys_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/keys/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_releases_id(self, owner, repo, id, **kwargs):
        """
        删除项目Release
        删除项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_releases_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def delete_v5_repos_owner_repo_releases_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        删除项目Release
        删除项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_releases_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_repos_owner_repo_releases_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org_repos(self, org, **kwargs):
        """
        获取一个组织的项目
        获取一个组织的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_repos(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str type: 筛选项目的类型，可以是 all, public, private。默认: all
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: Project
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_repos_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_repos_with_http_info(org, **kwargs)
            return data

    def get_v5_orgs_org_repos_with_http_info(self, org, **kwargs):
        """
        获取一个组织的项目
        获取一个组织的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_repos_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str type: 筛选项目的类型，可以是 all, public, private。默认: all
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: Project
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'type', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org_repos" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org_repos`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'type' in params:
            query_params.append(('type', params['type']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/repos', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Project',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo(self, owner, repo, **kwargs):
        """
        列出授权用户的某个项目
        列出授权用户的某个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        列出授权用户的某个项目
        列出授权用户的某个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_branches(self, owner, repo, **kwargs):
        """
        获取所有分支
        获取所有分支
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_branches(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: list[Branch]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_branches_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_branches_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_branches_with_http_info(self, owner, repo, **kwargs):
        """
        获取所有分支
        获取所有分支
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_branches_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: list[Branch]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_branches" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_branches`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_branches`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/branches', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Branch]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_branches_branch(self, owner, repo, branch, **kwargs):
        """
        获取单个分支
        获取单个分支
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_branches_branch(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: CompleteBranch
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_branches_branch_with_http_info(owner, repo, branch, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_branches_branch_with_http_info(owner, repo, branch, **kwargs)
            return data

    def get_v5_repos_owner_repo_branches_branch_with_http_info(self, owner, repo, branch, **kwargs):
        """
        获取单个分支
        获取单个分支
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_branches_branch_with_http_info(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: CompleteBranch
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'branch', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_branches_branch" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_branches_branch`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_branches_branch`")
        # verify the required parameter 'branch' is set
        if ('branch' not in params) or (params['branch'] is None):
            raise ValueError("Missing the required parameter `branch` when calling `get_v5_repos_owner_repo_branches_branch`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'branch' in params:
            path_params['branch'] = params['branch']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/branches/{branch}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CompleteBranch',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_collaborators(self, owner, repo, **kwargs):
        """
        获取项目的所有成员
        获取项目的所有成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_collaborators_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_collaborators_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_collaborators_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目的所有成员
        获取项目的所有成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_collaborators" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_collaborators`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_collaborators`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/collaborators', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_collaborators_username(self, owner, repo, username, **kwargs):
        """
        判断用户是否为项目成员
        判断用户是否为项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators_username(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, **kwargs)
            return data

    def get_v5_repos_owner_repo_collaborators_username_with_http_info(self, owner, repo, username, **kwargs):
        """
        判断用户是否为项目成员
        判断用户是否为项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_collaborators_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_repos_owner_repo_collaborators_username`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/collaborators/{username}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_collaborators_username_permission(self, owner, repo, username, **kwargs):
        """
        查看项目成员的权限
        查看项目成员的权限
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators_username_permission(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_collaborators_username_permission_with_http_info(owner, repo, username, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_collaborators_username_permission_with_http_info(owner, repo, username, **kwargs)
            return data

    def get_v5_repos_owner_repo_collaborators_username_permission_with_http_info(self, owner, repo, username, **kwargs):
        """
        查看项目成员的权限
        查看项目成员的权限
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_collaborators_username_permission_with_http_info(owner, repo, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_collaborators_username_permission" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_collaborators_username_permission`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_collaborators_username_permission`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_repos_owner_repo_collaborators_username_permission`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/collaborators/{username}/permission', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_comments(self, owner, repo, **kwargs):
        """
        获取项目的Commit评论
        获取项目的Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_comments(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_comments_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_comments_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_comments_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目的Commit评论
        获取项目的Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_comments_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_comments_id(self, owner, repo, id, **kwargs):
        """
        获取项目的某条Commit评论
        获取项目的某条Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def get_v5_repos_owner_repo_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        获取项目的某条Commit评论
        获取项目的某条Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_repos_owner_repo_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/comments/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_commits(self, owner, repo, **kwargs):
        """
        项目的所有提交
        项目的所有提交
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sha: 提交起始的SHA值或者分支名. 默认: 项目的默认分支
        :param str path: 包含该文件的提交
        :param str author: 提交作者的邮箱或个性地址(username/login)
        :param str since: 提交的起始时间，时间格式为 ISO 8601
        :param str until: 提交的最后时间，时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[RepoCommit]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_commits_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_commits_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_commits_with_http_info(self, owner, repo, **kwargs):
        """
        项目的所有提交
        项目的所有提交
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sha: 提交起始的SHA值或者分支名. 默认: 项目的默认分支
        :param str path: 包含该文件的提交
        :param str author: 提交作者的邮箱或个性地址(username/login)
        :param str since: 提交的起始时间，时间格式为 ISO 8601
        :param str until: 提交的最后时间，时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[RepoCommit]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'sha', 'path', 'author', 'since', 'until', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_commits" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_commits`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_commits`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sha' in params:
            query_params.append(('sha', params['sha']))
        if 'path' in params:
            query_params.append(('path', params['path']))
        if 'author' in params:
            query_params.append(('author', params['author']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'until' in params:
            query_params.append(('until', params['until']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/commits', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[RepoCommit]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_commits_ref_comments(self, owner, repo, ref, **kwargs):
        """
        获取单个Commit的评论
        获取单个Commit的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits_ref_comments(owner, repo, ref, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str ref: Commit的Reference (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_commits_ref_comments_with_http_info(owner, repo, ref, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_commits_ref_comments_with_http_info(owner, repo, ref, **kwargs)
            return data

    def get_v5_repos_owner_repo_commits_ref_comments_with_http_info(self, owner, repo, ref, **kwargs):
        """
        获取单个Commit的评论
        获取单个Commit的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits_ref_comments_with_http_info(owner, repo, ref, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str ref: Commit的Reference (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'ref', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_commits_ref_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_commits_ref_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_commits_ref_comments`")
        # verify the required parameter 'ref' is set
        if ('ref' not in params) or (params['ref'] is None):
            raise ValueError("Missing the required parameter `ref` when calling `get_v5_repos_owner_repo_commits_ref_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'ref' in params:
            path_params['ref'] = params['ref']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/commits/{ref}/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_commits_sha(self, owner, repo, sha, **kwargs):
        """
        项目的某个提交
        项目的某个提交
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits_sha(owner, repo, sha, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str sha: 提交的SHA值或者分支名 (required)
        :param str access_token: 用户授权码
        :return: RepoCommit
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_commits_sha_with_http_info(owner, repo, sha, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_commits_sha_with_http_info(owner, repo, sha, **kwargs)
            return data

    def get_v5_repos_owner_repo_commits_sha_with_http_info(self, owner, repo, sha, **kwargs):
        """
        项目的某个提交
        项目的某个提交
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_commits_sha_with_http_info(owner, repo, sha, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str sha: 提交的SHA值或者分支名 (required)
        :param str access_token: 用户授权码
        :return: RepoCommit
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'sha', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_commits_sha" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_commits_sha`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_commits_sha`")
        # verify the required parameter 'sha' is set
        if ('sha' not in params) or (params['sha'] is None):
            raise ValueError("Missing the required parameter `sha` when calling `get_v5_repos_owner_repo_commits_sha`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'sha' in params:
            path_params['sha'] = params['sha']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/commits/{sha}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='RepoCommit',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_compare_base___head(self, owner, repo, base, head, **kwargs):
        """
        两个Commits之间对比的版本差异
        两个Commits之间对比的版本差异
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_compare_base___head(owner, repo, base, head, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str base: Commit提交的SHA值或者分支名作为对比起点 (required)
        :param str head: Commit提交的SHA值或者分支名作为对比终点 (required)
        :param str access_token: 用户授权码
        :return: Compare
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_compare_base___head_with_http_info(owner, repo, base, head, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_compare_base___head_with_http_info(owner, repo, base, head, **kwargs)
            return data

    def get_v5_repos_owner_repo_compare_base___head_with_http_info(self, owner, repo, base, head, **kwargs):
        """
        两个Commits之间对比的版本差异
        两个Commits之间对比的版本差异
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_compare_base___head_with_http_info(owner, repo, base, head, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str base: Commit提交的SHA值或者分支名作为对比起点 (required)
        :param str head: Commit提交的SHA值或者分支名作为对比终点 (required)
        :param str access_token: 用户授权码
        :return: Compare
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'base', 'head', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_compare_base___head" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_compare_base___head`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_compare_base___head`")
        # verify the required parameter 'base' is set
        if ('base' not in params) or (params['base'] is None):
            raise ValueError("Missing the required parameter `base` when calling `get_v5_repos_owner_repo_compare_base___head`")
        # verify the required parameter 'head' is set
        if ('head' not in params) or (params['head'] is None):
            raise ValueError("Missing the required parameter `head` when calling `get_v5_repos_owner_repo_compare_base___head`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'base' in params:
            path_params['base'] = params['base']
        if 'head' in params:
            path_params['head'] = params['head']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/compare/{base}...{head}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Compare',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_contents_path(self, owner, repo, path, **kwargs):
        """
        获取仓库具体路径下的内容
        获取仓库具体路径下的内容
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_contents_path(owner, repo, path, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str access_token: 用户授权码
        :param str ref: 分支、tag或commit。默认: 项目的默认分支(通常是master)
        :return: list[Content]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, **kwargs)
            return data

    def get_v5_repos_owner_repo_contents_path_with_http_info(self, owner, repo, path, **kwargs):
        """
        获取仓库具体路径下的内容
        获取仓库具体路径下的内容
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str access_token: 用户授权码
        :param str ref: 分支、tag或commit。默认: 项目的默认分支(通常是master)
        :return: list[Content]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'path', 'access_token', 'ref']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_contents_path" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'path' is set
        if ('path' not in params) or (params['path'] is None):
            raise ValueError("Missing the required parameter `path` when calling `get_v5_repos_owner_repo_contents_path`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'path' in params:
            path_params['path'] = params['path']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'ref' in params:
            query_params.append(('ref', params['ref']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/contents(/{path})', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Content]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_contributors(self, owner, repo, **kwargs):
        """
        获取项目贡献者
        获取项目贡献者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_contributors(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_contributors_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_contributors_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_contributors_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目贡献者
        获取项目贡献者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_contributors_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_contributors" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_contributors`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_contributors`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/contributors', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_forks(self, owner, repo, **kwargs):
        """
        查看项目的Forks
        查看项目的Forks
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_forks(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: 排序方式: fork的时间(newest, oldest)，star的人数(stargazers)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_forks_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_forks_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_forks_with_http_info(self, owner, repo, **kwargs):
        """
        查看项目的Forks
        查看项目的Forks
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_forks_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: 排序方式: fork的时间(newest, oldest)，star的人数(stargazers)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'sort', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_forks" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_forks`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_forks`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/forks', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_keys(self, owner, repo, **kwargs):
        """
        展示项目的公钥
        展示项目的公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_keys(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKey]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_keys_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_keys_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_keys_with_http_info(self, owner, repo, **kwargs):
        """
        展示项目的公钥
        展示项目的公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_keys_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKey]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_keys" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_keys`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_keys`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/keys', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[SSHKey]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_keys_id(self, owner, repo, id, **kwargs):
        """
        获取项目的单个公钥
        获取项目的单个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_keys_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def get_v5_repos_owner_repo_keys_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        获取项目的单个公钥
        获取项目的单个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_keys_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_keys_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_keys_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_keys_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_repos_owner_repo_keys_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/keys/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='SSHKey',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pages(self, owner, repo, **kwargs):
        """
        获取Pages信息
        获取Pages信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pages(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pages_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pages_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_pages_with_http_info(self, owner, repo, **kwargs):
        """
        获取Pages信息
        获取Pages信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pages_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pages" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pages`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pages`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pages', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_readme(self, owner, repo, **kwargs):
        """
        获取仓库README
        获取仓库README
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_readme(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str ref: 分支、tag或commit。默认: 项目的默认分支(通常是master)
        :return: Content
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_readme_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_readme_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_readme_with_http_info(self, owner, repo, **kwargs):
        """
        获取仓库README
        获取仓库README
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_readme_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str ref: 分支、tag或commit。默认: 项目的默认分支(通常是master)
        :return: Content
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'ref']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_readme" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_readme`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_readme`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'ref' in params:
            query_params.append(('ref', params['ref']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/readme', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Content',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_releases(self, owner, repo, **kwargs):
        """
        获取项目的所有Releases
        获取项目的所有Releases
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Release]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_releases_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_releases_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_releases_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目的所有Releases
        获取项目的所有Releases
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Release]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_releases" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_releases`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_releases`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Release]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_releases_id(self, owner, repo, id, **kwargs):
        """
        获取项目的单个Releases
        获取项目的单个Releases
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 发行版本的ID (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def get_v5_repos_owner_repo_releases_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        获取项目的单个Releases
        获取项目的单个Releases
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 发行版本的ID (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_releases_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_repos_owner_repo_releases_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Release',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_releases_latest(self, owner, repo, **kwargs):
        """
        获取项目的最后更新的Release
        获取项目的最后更新的Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_latest(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_releases_latest_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_releases_latest_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_releases_latest_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目的最后更新的Release
        获取项目的最后更新的Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_latest_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_releases_latest" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_releases_latest`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_releases_latest`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases/latest', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Release',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_releases_tags_tag(self, owner, repo, tag, **kwargs):
        """
        根据Tag名称获取项目的Release
        根据Tag名称获取项目的Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_tags_tag(owner, repo, tag, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag: Tag 名称 (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_releases_tags_tag_with_http_info(owner, repo, tag, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_releases_tags_tag_with_http_info(owner, repo, tag, **kwargs)
            return data

    def get_v5_repos_owner_repo_releases_tags_tag_with_http_info(self, owner, repo, tag, **kwargs):
        """
        根据Tag名称获取项目的Release
        根据Tag名称获取项目的Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_releases_tags_tag_with_http_info(owner, repo, tag, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag: Tag 名称 (required)
        :param str access_token: 用户授权码
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'tag', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_releases_tags_tag" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_releases_tags_tag`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_releases_tags_tag`")
        # verify the required parameter 'tag' is set
        if ('tag' not in params) or (params['tag'] is None):
            raise ValueError("Missing the required parameter `tag` when calling `get_v5_repos_owner_repo_releases_tags_tag`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'tag' in params:
            path_params['tag'] = params['tag']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases/tags/{tag}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Release',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_tags(self, owner, repo, **kwargs):
        """
        列出项目所有的tags
        列出项目所有的tags
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_tags(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_tags_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_tags_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_tags_with_http_info(self, owner, repo, **kwargs):
        """
        列出项目所有的tags
        列出项目所有的tags
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_tags_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_tags" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_tags`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_tags`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/tags', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_repos(self, **kwargs):
        """
        列出授权用户的所有项目
        列出授权用户的所有项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_repos(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str visibility: 公开(public)、私有(private)或者所有(all)，默认: 所有(all)
        :param str affiliation: owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member
        :param str type: 不能与visibility或affiliation参数一并使用，否则会报422错误
        :param str sort: 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
        :param str direction: 如果sort参数为full_name，用升序(asc)。否则降序(desc)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_repos_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_repos_with_http_info(**kwargs)
            return data

    def get_v5_user_repos_with_http_info(self, **kwargs):
        """
        列出授权用户的所有项目
        列出授权用户的所有项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_repos_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str visibility: 公开(public)、私有(private)或者所有(all)，默认: 所有(all)
        :param str affiliation: owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member
        :param str type: 不能与visibility或affiliation参数一并使用，否则会报422错误
        :param str sort: 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
        :param str direction: 如果sort参数为full_name，用升序(asc)。否则降序(desc)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'visibility', 'affiliation', 'type', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_repos" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'visibility' in params:
            query_params.append(('visibility', params['visibility']))
        if 'affiliation' in params:
            query_params.append(('affiliation', params['affiliation']))
        if 'type' in params:
            query_params.append(('type', params['type']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/repos', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_repos(self, username, **kwargs):
        """
        获取某个用户的公开项目
        获取某个用户的公开项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_repos(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param str type: 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all)
        :param str sort: 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
        :param str direction: 如果sort参数为full_name，用升序(asc)。否则降序(desc)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_repos_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_repos_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_repos_with_http_info(self, username, **kwargs):
        """
        获取某个用户的公开项目
        获取某个用户的公开项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_repos_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param str type: 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all)
        :param str sort: 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
        :param str direction: 如果sort参数为full_name，用升序(asc)。否则降序(desc)
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'type', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_repos" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_repos`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'type' in params:
            query_params.append(('type', params['type']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/repos', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo(self, owner, repo, name, **kwargs):
        """
        更新项目设置
        更新项目设置
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo(owner, repo, name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str name: 项目名称 (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。
        :param str default_branch: 更新默认分支
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_with_http_info(owner, repo, name, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_with_http_info(owner, repo, name, **kwargs)
            return data

    def patch_v5_repos_owner_repo_with_http_info(self, owner, repo, name, **kwargs):
        """
        更新项目设置
        更新项目设置
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_with_http_info(owner, repo, name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str name: 项目名称 (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。
        :param str default_branch: 更新默认分支
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'name', 'access_token', 'description', 'homepage', 'has_issues', 'has_wiki', 'private', 'default_branch']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo`")
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `patch_v5_repos_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'homepage' in params:
            form_params.append(('homepage', params['homepage']))
        if 'has_issues' in params:
            form_params.append(('has_issues', params['has_issues']))
        if 'has_wiki' in params:
            form_params.append(('has_wiki', params['has_wiki']))
        if 'private' in params:
            form_params.append(('private', params['private']))
        if 'default_branch' in params:
            form_params.append(('default_branch', params['default_branch']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_comments_id(self, owner, repo, id, body, **kwargs):
        """
        更新Commit评论
        更新Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_comments_id(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: 评论的内容 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, body, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, body, **kwargs)
            return data

    def patch_v5_repos_owner_repo_comments_id_with_http_info(self, owner, repo, id, body, **kwargs):
        """
        更新Commit评论
        更新Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_comments_id_with_http_info(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: 评论的内容 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_repos_owner_repo_comments_id`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `patch_v5_repos_owner_repo_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/comments/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_releases_id(self, owner, repo, tag_name, name, body, id, **kwargs):
        """
        更新项目Release
        更新项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_releases_id(owner, repo, tag_name, name, body, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag_name: Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
        :param str name: Release 名称 (required)
        :param str body: Release 描述 (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :param bool prerelease: 是否为预览版本。默认: false（非预览版本）
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, tag_name, name, body, id, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, tag_name, name, body, id, **kwargs)
            return data

    def patch_v5_repos_owner_repo_releases_id_with_http_info(self, owner, repo, tag_name, name, body, id, **kwargs):
        """
        更新项目Release
        更新项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_releases_id_with_http_info(owner, repo, tag_name, name, body, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag_name: Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
        :param str name: Release 名称 (required)
        :param str body: Release 描述 (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :param bool prerelease: 是否为预览版本。默认: false（非预览版本）
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'tag_name', 'name', 'body', 'id', 'access_token', 'prerelease']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_releases_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'tag_name' is set
        if ('tag_name' not in params) or (params['tag_name'] is None):
            raise ValueError("Missing the required parameter `tag_name` when calling `patch_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `patch_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `patch_v5_repos_owner_repo_releases_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_repos_owner_repo_releases_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'tag_name' in params:
            form_params.append(('tag_name', params['tag_name']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'prerelease' in params:
            form_params.append(('prerelease', params['prerelease']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Release',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_orgs_org_repos(self, name, org, **kwargs):
        """
        创建组织项目
        创建组织项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_orgs_org_repos(name, org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: 项目名称 (required)
        :param int org: (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。默认: 公开(false)
        :param bool auto_init: 值为true时则会用README初始化仓库。默认: 不初始化(false)
        :param str gitignore_template: Git Ingore模版
        :param str license_template: Git Ingore模版
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_orgs_org_repos_with_http_info(name, org, **kwargs)
        else:
            (data) = self.post_v5_orgs_org_repos_with_http_info(name, org, **kwargs)
            return data

    def post_v5_orgs_org_repos_with_http_info(self, name, org, **kwargs):
        """
        创建组织项目
        创建组织项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_orgs_org_repos_with_http_info(name, org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: 项目名称 (required)
        :param int org: (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。默认: 公开(false)
        :param bool auto_init: 值为true时则会用README初始化仓库。默认: 不初始化(false)
        :param str gitignore_template: Git Ingore模版
        :param str license_template: Git Ingore模版
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['name', 'org', 'access_token', 'description', 'homepage', 'has_issues', 'has_wiki', 'private', 'auto_init', 'gitignore_template', 'license_template']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_orgs_org_repos" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `post_v5_orgs_org_repos`")
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `post_v5_orgs_org_repos`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'homepage' in params:
            form_params.append(('homepage', params['homepage']))
        if 'has_issues' in params:
            form_params.append(('has_issues', params['has_issues']))
        if 'has_wiki' in params:
            form_params.append(('has_wiki', params['has_wiki']))
        if 'private' in params:
            form_params.append(('private', params['private']))
        if 'auto_init' in params:
            form_params.append(('auto_init', params['auto_init']))
        if 'gitignore_template' in params:
            form_params.append(('gitignore_template', params['gitignore_template']))
        if 'license_template' in params:
            form_params.append(('license_template', params['license_template']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/repos', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_commits_sha_comments(self, owner, repo, sha, body, **kwargs):
        """
        创建Commit评论
        创建Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_commits_sha_comments(owner, repo, sha, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str sha: 评论的sha值 (required)
        :param str body: 评论的内容 (required)
        :param str access_token: 用户授权码
        :param str path: 文件的相对路径
        :param int position: Diff的相对行数
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_commits_sha_comments_with_http_info(owner, repo, sha, body, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_commits_sha_comments_with_http_info(owner, repo, sha, body, **kwargs)
            return data

    def post_v5_repos_owner_repo_commits_sha_comments_with_http_info(self, owner, repo, sha, body, **kwargs):
        """
        创建Commit评论
        创建Commit评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_commits_sha_comments_with_http_info(owner, repo, sha, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str sha: 评论的sha值 (required)
        :param str body: 评论的内容 (required)
        :param str access_token: 用户授权码
        :param str path: 文件的相对路径
        :param int position: Diff的相对行数
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'sha', 'body', 'access_token', 'path', 'position']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_commits_sha_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_commits_sha_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_commits_sha_comments`")
        # verify the required parameter 'sha' is set
        if ('sha' not in params) or (params['sha'] is None):
            raise ValueError("Missing the required parameter `sha` when calling `post_v5_repos_owner_repo_commits_sha_comments`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_v5_repos_owner_repo_commits_sha_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'sha' in params:
            path_params['sha'] = params['sha']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'path' in params:
            form_params.append(('path', params['path']))
        if 'position' in params:
            form_params.append(('position', params['position']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/commits/{sha}/comments', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_contents_path(self, owner, repo, path, content, message, **kwargs):
        """
        新建文件
        新建文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_contents_path(owner, repo, path, content, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str content: 文件内容, 要用base64编码 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, message, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, message, **kwargs)
            return data

    def post_v5_repos_owner_repo_contents_path_with_http_info(self, owner, repo, path, content, message, **kwargs):
        """
        新建文件
        新建文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str content: 文件内容, 要用base64编码 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'path', 'content', 'message', 'access_token', 'branch', 'committer_name', 'committer_email', 'author_name', 'author_email']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_contents_path" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'path' is set
        if ('path' not in params) or (params['path'] is None):
            raise ValueError("Missing the required parameter `path` when calling `post_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'content' is set
        if ('content' not in params) or (params['content'] is None):
            raise ValueError("Missing the required parameter `content` when calling `post_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'message' is set
        if ('message' not in params) or (params['message'] is None):
            raise ValueError("Missing the required parameter `message` when calling `post_v5_repos_owner_repo_contents_path`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'path' in params:
            path_params['path'] = params['path']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'content' in params:
            form_params.append(('content', params['content']))
        if 'message' in params:
            form_params.append(('message', params['message']))
        if 'branch' in params:
            form_params.append(('branch', params['branch']))
        if 'committer_name' in params:
            form_params.append(('committer[name]', params['committer_name']))
        if 'committer_email' in params:
            form_params.append(('committer[email]', params['committer_email']))
        if 'author_name' in params:
            form_params.append(('author[name]', params['author_name']))
        if 'author_email' in params:
            form_params.append(('author[email]', params['author_email']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/contents/{path}', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CommitContent',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_forks(self, owner, repo, **kwargs):
        """
        Fork一个项目
        Fork一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_forks(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str organization: 组织地址，不填写默认Fork到用户个性地址
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_forks_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_forks_with_http_info(owner, repo, **kwargs)
            return data

    def post_v5_repos_owner_repo_forks_with_http_info(self, owner, repo, **kwargs):
        """
        Fork一个项目
        Fork一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_forks_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str organization: 组织地址，不填写默认Fork到用户个性地址
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'organization']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_forks" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_forks`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_forks`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'organization' in params:
            form_params.append(('organization', params['organization']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/forks', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_keys(self, owner, repo, **kwargs):
        """
        为项目添加公钥
        为项目添加公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_keys(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_keys_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_keys_with_http_info(owner, repo, **kwargs)
            return data

    def post_v5_repos_owner_repo_keys_with_http_info(self, owner, repo, **kwargs):
        """
        为项目添加公钥
        为项目添加公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_keys_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_keys" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_keys`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_keys`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/keys', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='SSHKey',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_pages_builds(self, owner, repo, **kwargs):
        """
        请求建立Pages
        请求建立Pages
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pages_builds(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_pages_builds_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_pages_builds_with_http_info(owner, repo, **kwargs)
            return data

    def post_v5_repos_owner_repo_pages_builds_with_http_info(self, owner, repo, **kwargs):
        """
        请求建立Pages
        请求建立Pages
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pages_builds_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_pages_builds" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_pages_builds`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_pages_builds`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pages/builds', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_releases(self, owner, repo, tag_name, name, body, target_commitish, **kwargs):
        """
        创建项目Release
        创建项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_releases(owner, repo, tag_name, name, body, target_commitish, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag_name: Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
        :param str name: Release 名称 (required)
        :param str body: Release 描述 (required)
        :param str target_commitish: 分支名称或者commit SHA, 默认是当前默认分支 (required)
        :param str access_token: 用户授权码
        :param bool prerelease: 是否为预览版本。默认: false（非预览版本）
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_releases_with_http_info(owner, repo, tag_name, name, body, target_commitish, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_releases_with_http_info(owner, repo, tag_name, name, body, target_commitish, **kwargs)
            return data

    def post_v5_repos_owner_repo_releases_with_http_info(self, owner, repo, tag_name, name, body, target_commitish, **kwargs):
        """
        创建项目Release
        创建项目Release
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_releases_with_http_info(owner, repo, tag_name, name, body, target_commitish, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str tag_name: Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
        :param str name: Release 名称 (required)
        :param str body: Release 描述 (required)
        :param str target_commitish: 分支名称或者commit SHA, 默认是当前默认分支 (required)
        :param str access_token: 用户授权码
        :param bool prerelease: 是否为预览版本。默认: false（非预览版本）
        :return: Release
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'tag_name', 'name', 'body', 'target_commitish', 'access_token', 'prerelease']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_releases" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_releases`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_releases`")
        # verify the required parameter 'tag_name' is set
        if ('tag_name' not in params) or (params['tag_name'] is None):
            raise ValueError("Missing the required parameter `tag_name` when calling `post_v5_repos_owner_repo_releases`")
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `post_v5_repos_owner_repo_releases`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_v5_repos_owner_repo_releases`")
        # verify the required parameter 'target_commitish' is set
        if ('target_commitish' not in params) or (params['target_commitish'] is None):
            raise ValueError("Missing the required parameter `target_commitish` when calling `post_v5_repos_owner_repo_releases`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'tag_name' in params:
            form_params.append(('tag_name', params['tag_name']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'prerelease' in params:
            form_params.append(('prerelease', params['prerelease']))
        if 'target_commitish' in params:
            form_params.append(('target_commitish', params['target_commitish']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/releases', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Release',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_user_repos(self, name, **kwargs):
        """
        创建一个项目
        创建一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_repos(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: 项目名称 (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。默认: 公开(false)
        :param bool auto_init: 值为true时则会用README初始化仓库。默认: 不初始化(false)
        :param str gitignore_template: Git Ingore模版
        :param str license_template: License模版
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_user_repos_with_http_info(name, **kwargs)
        else:
            (data) = self.post_v5_user_repos_with_http_info(name, **kwargs)
            return data

    def post_v5_user_repos_with_http_info(self, name, **kwargs):
        """
        创建一个项目
        创建一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_repos_with_http_info(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: 项目名称 (required)
        :param str access_token: 用户授权码
        :param str description: 项目描述
        :param str homepage: 项目所在地址
        :param bool has_issues: 允许提Issue与否。默认: 允许(true)
        :param bool has_wiki: 提供Wiki与否。默认: 提供(true)
        :param bool private: 项目公开或私有。默认: 公开(false)
        :param bool auto_init: 值为true时则会用README初始化仓库。默认: 不初始化(false)
        :param str gitignore_template: Git Ingore模版
        :param str license_template: License模版
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['name', 'access_token', 'description', 'homepage', 'has_issues', 'has_wiki', 'private', 'auto_init', 'gitignore_template', 'license_template']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_user_repos" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `post_v5_user_repos`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'homepage' in params:
            form_params.append(('homepage', params['homepage']))
        if 'has_issues' in params:
            form_params.append(('has_issues', params['has_issues']))
        if 'has_wiki' in params:
            form_params.append(('has_wiki', params['has_wiki']))
        if 'private' in params:
            form_params.append(('private', params['private']))
        if 'auto_init' in params:
            form_params.append(('auto_init', params['auto_init']))
        if 'gitignore_template' in params:
            form_params.append(('gitignore_template', params['gitignore_template']))
        if 'license_template' in params:
            form_params.append(('license_template', params['license_template']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/repos', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_repos_owner_repo_branches_branch_protection(self, owner, repo, branch, **kwargs):
        """
        设置分支保护
        设置分支保护
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: CompleteBranch
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, **kwargs)
        else:
            (data) = self.put_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, **kwargs)
            return data

    def put_v5_repos_owner_repo_branches_branch_protection_with_http_info(self, owner, repo, branch, **kwargs):
        """
        设置分支保护
        设置分支保护
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_branches_branch_protection_with_http_info(owner, repo, branch, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str branch: 分支名称 (required)
        :param str access_token: 用户授权码
        :return: CompleteBranch
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'branch', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_repos_owner_repo_branches_branch_protection" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_repos_owner_repo_branches_branch_protection`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_repos_owner_repo_branches_branch_protection`")
        # verify the required parameter 'branch' is set
        if ('branch' not in params) or (params['branch'] is None):
            raise ValueError("Missing the required parameter `branch` when calling `put_v5_repos_owner_repo_branches_branch_protection`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'branch' in params:
            path_params['branch'] = params['branch']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/branches/{branch}/protection', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CompleteBranch',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_repos_owner_repo_collaborators_username(self, owner, repo, username, permission, **kwargs):
        """
        添加项目成员
        添加项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_collaborators_username(owner, repo, username, permission, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str permission: 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, permission, **kwargs)
        else:
            (data) = self.put_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, permission, **kwargs)
            return data

    def put_v5_repos_owner_repo_collaborators_username_with_http_info(self, owner, repo, username, permission, **kwargs):
        """
        添加项目成员
        添加项目成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_collaborators_username_with_http_info(owner, repo, username, permission, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str username: 用户名(username/login) (required)
        :param str permission: 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'username', 'permission', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_repos_owner_repo_collaborators_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `put_v5_repos_owner_repo_collaborators_username`")
        # verify the required parameter 'permission' is set
        if ('permission' not in params) or (params['permission'] is None):
            raise ValueError("Missing the required parameter `permission` when calling `put_v5_repos_owner_repo_collaborators_username`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'permission' in params:
            form_params.append(('permission', params['permission']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/collaborators/{username}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_repos_owner_repo_contents_path(self, owner, repo, path, content, sha, message, **kwargs):
        """
        更新文件
        更新文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_contents_path(owner, repo, path, content, sha, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str content: 文件内容, 要用base64编码 (required)
        :param str sha: 文件被替换的sha值 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, sha, message, **kwargs)
        else:
            (data) = self.put_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, sha, message, **kwargs)
            return data

    def put_v5_repos_owner_repo_contents_path_with_http_info(self, owner, repo, path, content, sha, message, **kwargs):
        """
        更新文件
        更新文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_contents_path_with_http_info(owner, repo, path, content, sha, message, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str path: 文件的路径 (required)
        :param str content: 文件内容, 要用base64编码 (required)
        :param str sha: 文件被替换的sha值 (required)
        :param str message: 提交信息 (required)
        :param str access_token: 用户授权码
        :param str branch: 分支名称。默认为项目对默认分支
        :param str committer_name: Committer的名字，默认为当前用户的名字
        :param str committer_email: Committer的邮箱，默认为当前用户的邮箱
        :param str author_name: Author的名字，默认为当前用户的名字
        :param str author_email: Author的邮箱，默认为当前用户的邮箱
        :return: CommitContent
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'path', 'content', 'sha', 'message', 'access_token', 'branch', 'committer_name', 'committer_email', 'author_name', 'author_email']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_repos_owner_repo_contents_path" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'path' is set
        if ('path' not in params) or (params['path'] is None):
            raise ValueError("Missing the required parameter `path` when calling `put_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'content' is set
        if ('content' not in params) or (params['content'] is None):
            raise ValueError("Missing the required parameter `content` when calling `put_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'sha' is set
        if ('sha' not in params) or (params['sha'] is None):
            raise ValueError("Missing the required parameter `sha` when calling `put_v5_repos_owner_repo_contents_path`")
        # verify the required parameter 'message' is set
        if ('message' not in params) or (params['message'] is None):
            raise ValueError("Missing the required parameter `message` when calling `put_v5_repos_owner_repo_contents_path`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'path' in params:
            path_params['path'] = params['path']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'content' in params:
            form_params.append(('content', params['content']))
        if 'sha' in params:
            form_params.append(('sha', params['sha']))
        if 'message' in params:
            form_params.append(('message', params['message']))
        if 'branch' in params:
            form_params.append(('branch', params['branch']))
        if 'committer_name' in params:
            form_params.append(('committer[name]', params['committer_name']))
        if 'committer_email' in params:
            form_params.append(('committer[email]', params['committer_email']))
        if 'author_name' in params:
            form_params.append(('author[name]', params['author_name']))
        if 'author_email' in params:
            form_params.append(('author[email]', params['author_email']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/contents/{path}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CommitContent',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

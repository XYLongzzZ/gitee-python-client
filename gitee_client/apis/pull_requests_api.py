# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class PullRequestsApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_repos_owner_repo_pulls_comments_id(self, owner, repo, id, **kwargs):
        """
        删除评论
        删除评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def delete_v5_repos_owner_repo_pulls_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        删除评论
        删除评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_pulls_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_repos_owner_repo_pulls_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/comments/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_repos_owner_repo_pulls_number_requested_reviewers(self, owner, repo, number, reviewers, **kwargs):
        """
        移除审查人员
        移除审查人员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param list[str] reviewers: 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对username换行即可 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, **kwargs)
            return data

    def delete_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(self, owner, repo, number, reviewers, **kwargs):
        """
        移除审查人员
        移除审查人员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param list[str] reviewers: 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对username换行即可 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'reviewers', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_pulls_number_requested_reviewers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `delete_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'reviewers' is set
        if ('reviewers' not in params) or (params['reviewers'] is None):
            raise ValueError("Missing the required parameter `reviewers` when calling `delete_v5_repos_owner_repo_pulls_number_requested_reviewers`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'reviewers' in params:
            form_params.append(('reviewers', params['reviewers']))
            collection_formats['reviewers'] = 'csv'

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls(self, owner, repo, **kwargs):
        """
        获取Pull Request列表
        获取Pull Request列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: 可选。Pull Request 状态
        :param str head: 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
        :param str base: 可选。Pull Request 提交目标分支的名称。
        :param str sort: 可选。排序字段，默认按创建时间
        :param str direction: 可选。升序/降序
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequest]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_with_http_info(self, owner, repo, **kwargs):
        """
        获取Pull Request列表
        获取Pull Request列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: 可选。Pull Request 状态
        :param str head: 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
        :param str base: 可选。Pull Request 提交目标分支的名称。
        :param str sort: 可选。排序字段，默认按创建时间
        :param str direction: 可选。升序/降序
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequest]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'state', 'head', 'base', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'head' in params:
            query_params.append(('head', params['head']))
        if 'base' in params:
            query_params.append(('base', params['base']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[PullRequest]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_comments(self, owner, repo, **kwargs):
        """
        获取该项目下的所有Pull Request评论
        获取该项目下的所有Pull Request评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_comments(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: 可选。按创建时间/更新时间排序
        :param str direction: 可选。升序/降序
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequestComments]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_comments_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_comments_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_comments_with_http_info(self, owner, repo, **kwargs):
        """
        获取该项目下的所有Pull Request评论
        获取该项目下的所有Pull Request评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_comments_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str sort: 可选。按创建时间/更新时间排序
        :param str direction: 可选。升序/降序
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequestComments]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'sort', 'direction', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[PullRequestComments]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_comments_id(self, owner, repo, id, **kwargs):
        """
        获取Pull Request的某个评论
        获取Pull Request的某个评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_comments_id_with_http_info(self, owner, repo, id, **kwargs):
        """
        获取Pull Request的某个评论
        获取Pull Request的某个评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: (required)
        :param str access_token: 用户授权码
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_repos_owner_repo_pulls_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/comments/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequestComments',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number(self, owner, repo, number, **kwargs):
        """
        获取单个Pull Request
        获取单个Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取单个Pull Request
        获取单个Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequest',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number_comments(self, owner, repo, number, **kwargs):
        """
        获取某个Pull Request的所有评论
        获取某个Pull Request的所有评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequestComments]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_comments_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取某个Pull Request的所有评论
        获取某个Pull Request的所有评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[PullRequestComments]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number_comments`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[PullRequestComments]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number_commits(self, owner, repo, number, **kwargs):
        """
        获取某Pull Request的所有Commit信息。最多显示250条Commit
        获取某Pull Request的所有Commit信息。最多显示250条Commit
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_commits(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: list[PullRequestCommits]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_commits_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_commits_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_commits_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取某Pull Request的所有Commit信息。最多显示250条Commit
        获取某Pull Request的所有Commit信息。最多显示250条Commit
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_commits_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: list[PullRequestCommits]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number_commits" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number_commits`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number_commits`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number_commits`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/commits', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[PullRequestCommits]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number_files(self, owner, repo, number, **kwargs):
        """
        Pull Request Commit文件列表。最多显示300条diff
        Pull Request Commit文件列表。最多显示300条diff
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_files(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: list[PullRequestFiles]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_files_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_files_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_files_with_http_info(self, owner, repo, number, **kwargs):
        """
        Pull Request Commit文件列表。最多显示300条diff
        Pull Request Commit文件列表。最多显示300条diff
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_files_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: list[PullRequestFiles]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number_files" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number_files`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number_files`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number_files`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/files', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[PullRequestFiles]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number_merge(self, owner, repo, number, **kwargs):
        """
        判断Pull Request是否已经合并
        判断Pull Request是否已经合并
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_merge_with_http_info(self, owner, repo, number, **kwargs):
        """
        判断Pull Request是否已经合并
        判断Pull Request是否已经合并
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number_merge" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number_merge`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number_merge`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number_merge`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/merge', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_pulls_number_requested_reviewers(self, owner, repo, number, **kwargs):
        """
        获取审查人员的列表
        获取审查人员的列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: UserBasic
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取审查人员的列表
        获取审查人员的列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: UserBasic
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_pulls_number_requested_reviewers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_pulls_number_requested_reviewers`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserBasic',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_pulls_comments_id(self, owner, repo, id, body, **kwargs):
        """
        编辑评论
        编辑评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: 必填。评论内容 (required)
        :param str access_token: 用户授权码
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, body, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, body, **kwargs)
            return data

    def patch_v5_repos_owner_repo_pulls_comments_id_with_http_info(self, owner, repo, id, body, **kwargs):
        """
        编辑评论
        编辑评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_pulls_comments_id_with_http_info(owner, repo, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int id: 评论的ID (required)
        :param str body: 必填。评论内容 (required)
        :param str access_token: 用户授权码
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'id', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_pulls_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_repos_owner_repo_pulls_comments_id`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `patch_v5_repos_owner_repo_pulls_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/comments/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequestComments',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_pulls_number(self, owner, repo, number, **kwargs):
        """
        更新Pull Request信息
        更新Pull Request信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_pulls_number(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param str title: 可选。Pull Request 标题
        :param str body: 可选。Pull Request 内容
        :param str state: 可选。Pull Request 状态
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, **kwargs)
            return data

    def patch_v5_repos_owner_repo_pulls_number_with_http_info(self, owner, repo, number, **kwargs):
        """
        更新Pull Request信息
        更新Pull Request信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_pulls_number_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :param str title: 可选。Pull Request 标题
        :param str body: 可选。Pull Request 内容
        :param str state: 可选。Pull Request 状态
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token', 'title', 'body', 'state']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_pulls_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_pulls_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_pulls_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `patch_v5_repos_owner_repo_pulls_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'state' in params:
            form_params.append(('state', params['state']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequest',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_pulls(self, owner, repo, title, head, base, **kwargs):
        """
        创建Pull Request
        创建Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls(owner, repo, title, head, base, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: 必填。Pull Request 标题 (required)
        :param str head: 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch (required)
        :param str base: 必填。Pull Request 提交目标分支的名称 (required)
        :param str access_token: 用户授权码
        :param str body: 可选。Pull Request 内容
        :param str issue: 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_pulls_with_http_info(owner, repo, title, head, base, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_pulls_with_http_info(owner, repo, title, head, base, **kwargs)
            return data

    def post_v5_repos_owner_repo_pulls_with_http_info(self, owner, repo, title, head, base, **kwargs):
        """
        创建Pull Request
        创建Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls_with_http_info(owner, repo, title, head, base, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: 必填。Pull Request 标题 (required)
        :param str head: 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch (required)
        :param str base: 必填。Pull Request 提交目标分支的名称 (required)
        :param str access_token: 用户授权码
        :param str body: 可选。Pull Request 内容
        :param str issue: 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'title', 'head', 'base', 'access_token', 'body', 'issue']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_pulls" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_pulls`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_pulls`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `post_v5_repos_owner_repo_pulls`")
        # verify the required parameter 'head' is set
        if ('head' not in params) or (params['head'] is None):
            raise ValueError("Missing the required parameter `head` when calling `post_v5_repos_owner_repo_pulls`")
        # verify the required parameter 'base' is set
        if ('base' not in params) or (params['base'] is None):
            raise ValueError("Missing the required parameter `base` when calling `post_v5_repos_owner_repo_pulls`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'head' in params:
            form_params.append(('head', params['head']))
        if 'base' in params:
            form_params.append(('base', params['base']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'issue' in params:
            form_params.append(('issue', params['issue']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequest',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_pulls_number_comments(self, owner, repo, number, body, **kwargs):
        """
        提交Pull Request评论
        提交Pull Request评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str body: 必填。评论内容 (required)
        :param str access_token: 用户授权码
        :param str commit_id: 可选。PR代码评论的commit id
        :param str path: 可选。PR代码评论的文件名
        :param int position: 可选。PR代码评论diff中的行数
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, body, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, body, **kwargs)
            return data

    def post_v5_repos_owner_repo_pulls_number_comments_with_http_info(self, owner, repo, number, body, **kwargs):
        """
        提交Pull Request评论
        提交Pull Request评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls_number_comments_with_http_info(owner, repo, number, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str body: 必填。评论内容 (required)
        :param str access_token: 用户授权码
        :param str commit_id: 可选。PR代码评论的commit id
        :param str path: 可选。PR代码评论的文件名
        :param int position: 可选。PR代码评论diff中的行数
        :return: PullRequestComments
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'body', 'access_token', 'commit_id', 'path', 'position']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_pulls_number_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_pulls_number_comments`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_pulls_number_comments`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `post_v5_repos_owner_repo_pulls_number_comments`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_v5_repos_owner_repo_pulls_number_comments`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))
        if 'commit_id' in params:
            form_params.append(('commit_id', params['commit_id']))
        if 'path' in params:
            form_params.append(('path', params['path']))
        if 'position' in params:
            form_params.append(('position', params['position']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/comments', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequestComments',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_pulls_number_requested_reviewers(self, owner, repo, number, reviewers, **kwargs):
        """
        增加审查人员
        增加审查人员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param list[str] reviewers: 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对标签名换行即可 (required)
        :param str access_token: 用户授权码
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, **kwargs)
            return data

    def post_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(self, owner, repo, number, reviewers, **kwargs):
        """
        增加审查人员
        增加审查人员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_pulls_number_requested_reviewers_with_http_info(owner, repo, number, reviewers, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param list[str] reviewers: 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对标签名换行即可 (required)
        :param str access_token: 用户授权码
        :return: PullRequest
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'reviewers', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_pulls_number_requested_reviewers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `post_v5_repos_owner_repo_pulls_number_requested_reviewers`")
        # verify the required parameter 'reviewers' is set
        if ('reviewers' not in params) or (params['reviewers'] is None):
            raise ValueError("Missing the required parameter `reviewers` when calling `post_v5_repos_owner_repo_pulls_number_requested_reviewers`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'reviewers' in params:
            form_params.append(('reviewers', params['reviewers']))
            collection_formats['reviewers'] = 'csv'

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='PullRequest',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_repos_owner_repo_pulls_number_merge(self, owner, repo, number, **kwargs):
        """
        合并Pull Request
        合并Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.put_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, **kwargs)
            return data

    def put_v5_repos_owner_repo_pulls_number_merge_with_http_info(self, owner, repo, number, **kwargs):
        """
        合并Pull Request
        合并Pull Request
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_pulls_number_merge_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个PR，即本项目PR的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_repos_owner_repo_pulls_number_merge" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_repos_owner_repo_pulls_number_merge`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_repos_owner_repo_pulls_number_merge`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `put_v5_repos_owner_repo_pulls_number_merge`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/pulls/{number}/merge', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

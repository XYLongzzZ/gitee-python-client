# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class MilestonesApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_repos_owner_repo_milestones_number(self, owner, repo, number, **kwargs):
        """
        删除项目单个里程碑
        删除项目单个里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_milestones_number(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.delete_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, **kwargs)
            return data

    def delete_v5_repos_owner_repo_milestones_number_with_http_info(self, owner, repo, number, **kwargs):
        """
        删除项目单个里程碑
        删除项目单个里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_repos_owner_repo_milestones_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `delete_v5_repos_owner_repo_milestones_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/milestones/{number}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_milestones(self, owner, repo, **kwargs):
        """
        获取项目所有里程碑
        获取项目所有里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_milestones(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str sort: 排序方式: due_on
        :param str direction: 升序(asc)或是降序(desc)。默认: asc
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Milestone]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_milestones_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_milestones_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_milestones_with_http_info(self, owner, repo, **kwargs):
        """
        获取项目所有里程碑
        获取项目所有里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_milestones_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str sort: 排序方式: due_on
        :param str direction: 升序(asc)或是降序(desc)。默认: asc
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Milestone]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'state', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_milestones" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_milestones`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_milestones`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'state' in params:
            query_params.append(('state', params['state']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/milestones', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Milestone]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_milestones_number(self, owner, repo, number, **kwargs):
        """
        获取项目单个里程碑
        获取项目单个里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_milestones_number(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str access_token: 用户授权码
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, **kwargs)
            return data

    def get_v5_repos_owner_repo_milestones_number_with_http_info(self, owner, repo, number, **kwargs):
        """
        获取项目单个里程碑
        获取项目单个里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str access_token: 用户授权码
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_milestones_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `get_v5_repos_owner_repo_milestones_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/milestones/{number}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Milestone',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_repos_owner_repo_milestones_number(self, owner, repo, number, title, **kwargs):
        """
        更新项目里程碑
        更新项目里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_milestones_number(owner, repo, number, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str title: 里程碑标题 (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str description: 里程碑具体描述
        :param str due_on: 里程碑的截止日期
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, title, **kwargs)
        else:
            (data) = self.patch_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, title, **kwargs)
            return data

    def patch_v5_repos_owner_repo_milestones_number_with_http_info(self, owner, repo, number, title, **kwargs):
        """
        更新项目里程碑
        更新项目里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_repos_owner_repo_milestones_number_with_http_info(owner, repo, number, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param int number: 第几个里程碑，即本项目里程碑的序数 (required)
        :param str title: 里程碑标题 (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str description: 里程碑具体描述
        :param str due_on: 里程碑的截止日期
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'number', 'title', 'access_token', 'state', 'description', 'due_on']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_repos_owner_repo_milestones_number" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `patch_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `patch_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'number' is set
        if ('number' not in params) or (params['number'] is None):
            raise ValueError("Missing the required parameter `number` when calling `patch_v5_repos_owner_repo_milestones_number`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `patch_v5_repos_owner_repo_milestones_number`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']
        if 'number' in params:
            path_params['number'] = params['number']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'state' in params:
            form_params.append(('state', params['state']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'due_on' in params:
            form_params.append(('due_on', params['due_on']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/milestones/{number}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Milestone',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_repos_owner_repo_milestones(self, owner, repo, title, **kwargs):
        """
        创建项目里程碑
        创建项目里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_milestones(owner, repo, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: 里程碑标题 (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str description: 里程碑具体描述
        :param str due_on: 里程碑的截止日期
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_repos_owner_repo_milestones_with_http_info(owner, repo, title, **kwargs)
        else:
            (data) = self.post_v5_repos_owner_repo_milestones_with_http_info(owner, repo, title, **kwargs)
            return data

    def post_v5_repos_owner_repo_milestones_with_http_info(self, owner, repo, title, **kwargs):
        """
        创建项目里程碑
        创建项目里程碑
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_repos_owner_repo_milestones_with_http_info(owner, repo, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str title: 里程碑标题 (required)
        :param str access_token: 用户授权码
        :param str state: 里程碑状态: open, closed, all。默认: open
        :param str description: 里程碑具体描述
        :param str due_on: 里程碑的截止日期
        :return: Milestone
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'title', 'access_token', 'state', 'description', 'due_on']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_repos_owner_repo_milestones" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `post_v5_repos_owner_repo_milestones`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `post_v5_repos_owner_repo_milestones`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `post_v5_repos_owner_repo_milestones`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'title' in params:
            form_params.append(('title', params['title']))
        if 'state' in params:
            form_params.append(('state', params['state']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'due_on' in params:
            form_params.append(('due_on', params['due_on']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/milestones', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Milestone',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

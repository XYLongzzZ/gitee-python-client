# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class OrganizationsApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_orgs_org_memberships_username(self, org, username, **kwargs):
        """
        移除授权用户所管理组织中的成员
        移除授权用户所管理组织中的成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_orgs_org_memberships_username(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
        else:
            (data) = self.delete_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
            return data

    def delete_v5_orgs_org_memberships_username_with_http_info(self, org, username, **kwargs):
        """
        移除授权用户所管理组织中的成员
        移除授权用户所管理组织中的成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_orgs_org_memberships_username_with_http_info(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_orgs_org_memberships_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `delete_v5_orgs_org_memberships_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `delete_v5_orgs_org_memberships_username`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/memberships/{username}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_user_memberships_orgs_org(self, org, **kwargs):
        """
        退出一个组织
        退出一个组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_memberships_orgs_org(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
        else:
            (data) = self.delete_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
            return data

    def delete_v5_user_memberships_orgs_org_with_http_info(self, org, **kwargs):
        """
        退出一个组织
        退出一个组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_memberships_orgs_org_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_memberships_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `delete_v5_user_memberships_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/memberships/orgs/{org}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org(self, org, **kwargs):
        """
        获取一个组织
        获取一个组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: Group
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_with_http_info(org, **kwargs)
            return data

    def get_v5_orgs_org_with_http_info(self, org, **kwargs):
        """
        获取一个组织
        获取一个组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: Group
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='Group',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org_members(self, org, **kwargs):
        """
        列出一个组织的所有成员
        列出一个组织的所有成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_members(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str role: 根据角色筛选成员
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_members_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_members_with_http_info(org, **kwargs)
            return data

    def get_v5_orgs_org_members_with_http_info(self, org, **kwargs):
        """
        列出一个组织的所有成员
        列出一个组织的所有成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_members_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str role: 根据角色筛选成员
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'page', 'per_page', 'role']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org_members" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org_members`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))
        if 'role' in params:
            query_params.append(('role', params['role']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/members', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org_memberships_username(self, org, username, **kwargs):
        """
        获取授权用户所属组织的一个成员
        获取授权用户所属组织的一个成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_memberships_username(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
            return data

    def get_v5_orgs_org_memberships_username_with_http_info(self, org, username, **kwargs):
        """
        获取授权用户所属组织的一个成员
        获取授权用户所属组织的一个成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_memberships_username_with_http_info(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org_memberships_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org_memberships_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_orgs_org_memberships_username`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/memberships/{username}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='GroupMember',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_memberships_orgs(self, **kwargs):
        """
        列出授权用户在所属组织的成员资料
        列出授权用户在所属组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_memberships_orgs(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool active: 根据成员是否已激活进行筛选资料，缺省返回所有资料
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[GroupMember]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_memberships_orgs_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_memberships_orgs_with_http_info(**kwargs)
            return data

    def get_v5_user_memberships_orgs_with_http_info(self, **kwargs):
        """
        列出授权用户在所属组织的成员资料
        列出授权用户在所属组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_memberships_orgs_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool active: 根据成员是否已激活进行筛选资料，缺省返回所有资料
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[GroupMember]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'active', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_memberships_orgs" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'active' in params:
            query_params.append(('active', params['active']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/memberships/orgs', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[GroupMember]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_memberships_orgs_org(self, org, **kwargs):
        """
        获取授权用户在一个组织的成员资料
        获取授权用户在一个组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_memberships_orgs_org(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
            return data

    def get_v5_user_memberships_orgs_org_with_http_info(self, org, **kwargs):
        """
        获取授权用户在一个组织的成员资料
        获取授权用户在一个组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_memberships_orgs_org_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_memberships_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_user_memberships_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/memberships/orgs/{org}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='GroupMember',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_orgs(self, **kwargs):
        """
        列出授权用户所属的组织
        列出授权用户所属的组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_orgs(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param bool admin: 只列出授权用户管理的组织
        :return: list[Group]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_orgs_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_orgs_with_http_info(**kwargs)
            return data

    def get_v5_user_orgs_with_http_info(self, **kwargs):
        """
        列出授权用户所属的组织
        列出授权用户所属的组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_orgs_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param bool admin: 只列出授权用户管理的组织
        :return: list[Group]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'page', 'per_page', 'admin']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_orgs" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))
        if 'admin' in params:
            query_params.append(('admin', params['admin']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/orgs', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Group]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_orgs(self, username, **kwargs):
        """
        列出用户所属的组织
        列出用户所属的组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_orgs(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Group]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_orgs_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_orgs_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_orgs_with_http_info(self, username, **kwargs):
        """
        列出用户所属的组织
        列出用户所属的组织
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_orgs_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Group]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_orgs" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_orgs`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/orgs', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Group]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_orgs_org(self, org, **kwargs):
        """
        更新授权用户所管理的组织资料
        更新授权用户所管理的组织资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_orgs_org(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str email: 组织公开的邮箱地址
        :param str location: 组织所在地
        :param str name: 组织名称
        :param str description: 组织简介
        :param str html_url: 组织站点
        :return: GroupDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_orgs_org_with_http_info(org, **kwargs)
        else:
            (data) = self.patch_v5_orgs_org_with_http_info(org, **kwargs)
            return data

    def patch_v5_orgs_org_with_http_info(self, org, **kwargs):
        """
        更新授权用户所管理的组织资料
        更新授权用户所管理的组织资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_orgs_org_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str email: 组织公开的邮箱地址
        :param str location: 组织所在地
        :param str name: 组织名称
        :param str description: 组织简介
        :param str html_url: 组织站点
        :return: GroupDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'email', 'location', 'name', 'description', 'html_url']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `patch_v5_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'email' in params:
            form_params.append(('email', params['email']))
        if 'location' in params:
            form_params.append(('location', params['location']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'html_url' in params:
            form_params.append(('html_url', params['html_url']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='GroupDetail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_user_memberships_orgs_org(self, org, **kwargs):
        """
        更新授权用户在一个组织的成员资料
        更新授权用户在一个组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user_memberships_orgs_org(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str remark: 在组织中的备注信息
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
        else:
            (data) = self.patch_v5_user_memberships_orgs_org_with_http_info(org, **kwargs)
            return data

    def patch_v5_user_memberships_orgs_org_with_http_info(self, org, **kwargs):
        """
        更新授权用户在一个组织的成员资料
        更新授权用户在一个组织的成员资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user_memberships_orgs_org_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param str remark: 在组织中的备注信息
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'remark']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_user_memberships_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `patch_v5_user_memberships_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'remark' in params:
            form_params.append(('remark', params['remark']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/memberships/orgs/{org}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='GroupMember',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_orgs_org_memberships_username(self, org, username, **kwargs):
        """
        增加或更新授权用户所管理组织的成员
        增加或更新授权用户所管理组织的成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_orgs_org_memberships_username(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param str role: 设置用户在组织的角色
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
        else:
            (data) = self.put_v5_orgs_org_memberships_username_with_http_info(org, username, **kwargs)
            return data

    def put_v5_orgs_org_memberships_username_with_http_info(self, org, username, **kwargs):
        """
        增加或更新授权用户所管理组织的成员
        增加或更新授权用户所管理组织的成员
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_orgs_org_memberships_username_with_http_info(org, username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param str role: 设置用户在组织的角色
        :return: GroupMember
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'username', 'access_token', 'role']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_orgs_org_memberships_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `put_v5_orgs_org_memberships_username`")
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `put_v5_orgs_org_memberships_username`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'role' in params:
            form_params.append(('role', params['role']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/memberships/{username}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='GroupMember',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class GistsApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_gists_gist_id_comments_id(self, gist_id, id, **kwargs):
        """
        删除代码片段的评论
        删除代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_gist_id_comments_id(gist_id, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, **kwargs)
        else:
            (data) = self.delete_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, **kwargs)
            return data

    def delete_v5_gists_gist_id_comments_id_with_http_info(self, gist_id, id, **kwargs):
        """
        删除代码片段的评论
        删除代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['gist_id', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_gists_gist_id_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'gist_id' is set
        if ('gist_id' not in params) or (params['gist_id'] is None):
            raise ValueError("Missing the required parameter `gist_id` when calling `delete_v5_gists_gist_id_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_gists_gist_id_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'gist_id' in params:
            path_params['gist_id'] = params['gist_id']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{gist_id}/comments/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_gists_id(self, id, **kwargs):
        """
        删除该条代码片段
        删除该条代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_gists_id_with_http_info(id, **kwargs)
        else:
            (data) = self.delete_v5_gists_id_with_http_info(id, **kwargs)
            return data

    def delete_v5_gists_id_with_http_info(self, id, **kwargs):
        """
        删除该条代码片段
        删除该条代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_gists_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_gists_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_gists_id_star(self, id, **kwargs):
        """
        取消Star代码片段
        取消Star代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_id_star(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_gists_id_star_with_http_info(id, **kwargs)
        else:
            (data) = self.delete_v5_gists_id_star_with_http_info(id, **kwargs)
            return data

    def delete_v5_gists_id_star_with_http_info(self, id, **kwargs):
        """
        取消Star代码片段
        取消Star代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_gists_id_star_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_gists_id_star" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_gists_id_star`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/star', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists(self, **kwargs):
        """
        获取代码片段
        获取代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_gists_with_http_info(**kwargs)
            return data

    def get_v5_gists_with_http_info(self, **kwargs):
        """
        获取代码片段
        获取代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Code]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_gist_id_comments(self, gist_id, **kwargs):
        """
        获取代码片段的评论
        获取代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_gist_id_comments(gist_id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[CodeComment]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_gist_id_comments_with_http_info(gist_id, **kwargs)
        else:
            (data) = self.get_v5_gists_gist_id_comments_with_http_info(gist_id, **kwargs)
            return data

    def get_v5_gists_gist_id_comments_with_http_info(self, gist_id, **kwargs):
        """
        获取代码片段的评论
        获取代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_gist_id_comments_with_http_info(gist_id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[CodeComment]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['gist_id', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_gist_id_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'gist_id' is set
        if ('gist_id' not in params) or (params['gist_id'] is None):
            raise ValueError("Missing the required parameter `gist_id` when calling `get_v5_gists_gist_id_comments`")


        collection_formats = {}

        path_params = {}
        if 'gist_id' in params:
            path_params['gist_id'] = params['gist_id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{gist_id}/comments', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[CodeComment]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_gist_id_comments_id(self, gist_id, id, **kwargs):
        """
        获取单条代码片段的评论
        获取单条代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_gist_id_comments_id(gist_id, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, **kwargs)
        else:
            (data) = self.get_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, **kwargs)
            return data

    def get_v5_gists_gist_id_comments_id_with_http_info(self, gist_id, id, **kwargs):
        """
        获取单条代码片段的评论
        获取单条代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['gist_id', 'id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_gist_id_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'gist_id' is set
        if ('gist_id' not in params) or (params['gist_id'] is None):
            raise ValueError("Missing the required parameter `gist_id` when calling `get_v5_gists_gist_id_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_gists_gist_id_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'gist_id' in params:
            path_params['gist_id'] = params['gist_id']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{gist_id}/comments/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeComment',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_id(self, id, **kwargs):
        """
        获取单条代码片段
        获取单条代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_id_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_gists_id_with_http_info(id, **kwargs)
            return data

    def get_v5_gists_id_with_http_info(self, id, **kwargs):
        """
        获取单条代码片段
        获取单条代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_gists_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeForksHistory',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_id_commits(self, id, **kwargs):
        """
        获取代码片段的commit
        获取代码片段的commit
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_commits(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_id_commits_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_gists_id_commits_with_http_info(id, **kwargs)
            return data

    def get_v5_gists_id_commits_with_http_info(self, id, **kwargs):
        """
        获取代码片段的commit
        获取代码片段的commit
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_commits_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_id_commits" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_gists_id_commits`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/commits', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeForksHistory',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_id_forks(self, id, **kwargs):
        """
        获取Fork该条代码片段的列表
        获取Fork该条代码片段的列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_forks(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: CodeForks
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_id_forks_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_gists_id_forks_with_http_info(id, **kwargs)
            return data

    def get_v5_gists_id_forks_with_http_info(self, id, **kwargs):
        """
        获取Fork该条代码片段的列表
        获取Fork该条代码片段的列表
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_forks_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: CodeForks
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_id_forks" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_gists_id_forks`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/forks', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeForks',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_id_star(self, id, **kwargs):
        """
        判断代码片段是否已Star
        判断代码片段是否已Star
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_star(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_id_star_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_gists_id_star_with_http_info(id, **kwargs)
            return data

    def get_v5_gists_id_star_with_http_info(self, id, **kwargs):
        """
        判断代码片段是否已Star
        判断代码片段是否已Star
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_id_star_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_id_star" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_gists_id_star`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/star', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_public(self, **kwargs):
        """
        获取公开的代码片段
        获取公开的代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_public(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_public_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_gists_public_with_http_info(**kwargs)
            return data

    def get_v5_gists_public_with_http_info(self, **kwargs):
        """
        获取公开的代码片段
        获取公开的代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_public_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_public" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/public', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Code]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gists_starred(self, **kwargs):
        """
        获取用户Star的代码片段
        获取用户Star的代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_starred(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gists_starred_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_gists_starred_with_http_info(**kwargs)
            return data

    def get_v5_gists_starred_with_http_info(self, **kwargs):
        """
        获取用户Star的代码片段
        获取用户Star的代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gists_starred_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str since: 起始的更新时间，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'since', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gists_starred" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/starred', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Code]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_gists(self, username, **kwargs):
        """
        获取指定用户的公开代码片段
        获取指定用户的公开代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_gists(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_gists_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_gists_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_gists_with_http_info(self, username, **kwargs):
        """
        获取指定用户的公开代码片段
        获取指定用户的公开代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_gists_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Code]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_gists" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_gists`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/gists', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Code]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_gists_gist_id_comments_id(self, gist_id, id, body, **kwargs):
        """
        修改代码片段的评论
        修改代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_gists_gist_id_comments_id(gist_id, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str body: 评论内容 (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, body, **kwargs)
        else:
            (data) = self.patch_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, body, **kwargs)
            return data

    def patch_v5_gists_gist_id_comments_id_with_http_info(self, gist_id, id, body, **kwargs):
        """
        修改代码片段的评论
        修改代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_gists_gist_id_comments_id_with_http_info(gist_id, id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param int id: 评论的ID (required)
        :param str body: 评论内容 (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['gist_id', 'id', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_gists_gist_id_comments_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'gist_id' is set
        if ('gist_id' not in params) or (params['gist_id'] is None):
            raise ValueError("Missing the required parameter `gist_id` when calling `patch_v5_gists_gist_id_comments_id`")
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_gists_gist_id_comments_id`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `patch_v5_gists_gist_id_comments_id`")


        collection_formats = {}

        path_params = {}
        if 'gist_id' in params:
            path_params['gist_id'] = params['gist_id']
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{gist_id}/comments/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeComment',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_gists_id(self, id, files, description, **kwargs):
        """
        修改代码片段
        修改代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_gists_id(id, files, description, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param dict(str, str) files: Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } } (required)
        :param str description: 代码片段描述，1~30个字符 (required)
        :param str access_token: 用户授权码
        :param bool public: 公开/私有，默认: 私有
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_gists_id_with_http_info(id, files, description, **kwargs)
        else:
            (data) = self.patch_v5_gists_id_with_http_info(id, files, description, **kwargs)
            return data

    def patch_v5_gists_id_with_http_info(self, id, files, description, **kwargs):
        """
        修改代码片段
        修改代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_gists_id_with_http_info(id, files, description, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param dict(str, str) files: Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } } (required)
        :param str description: 代码片段描述，1~30个字符 (required)
        :param str access_token: 用户授权码
        :param bool public: 公开/私有，默认: 私有
        :return: CodeForksHistory
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'files', 'description', 'access_token', 'public']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_gists_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_gists_id`")
        # verify the required parameter 'files' is set
        if ('files' not in params) or (params['files'] is None):
            raise ValueError("Missing the required parameter `files` when calling `patch_v5_gists_id`")
        # verify the required parameter 'description' is set
        if ('description' not in params) or (params['description'] is None):
            raise ValueError("Missing the required parameter `description` when calling `patch_v5_gists_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'files' in params:
            form_params.append(('files', params['files']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'public' in params:
            form_params.append(('public', params['public']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeForksHistory',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_gists(self, files, description, **kwargs):
        """
        创建代码片段
        创建代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists(files, description, async=True)
        >>> result = thread.get()

        :param async bool
        :param dict(str, str) files: Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } } (required)
        :param str description: 代码片段描述，1~30个字符 (required)
        :param str access_token: 用户授权码
        :param bool public: 公开/私有，默认: 私有
        :return: list[CodeForksHistory]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_gists_with_http_info(files, description, **kwargs)
        else:
            (data) = self.post_v5_gists_with_http_info(files, description, **kwargs)
            return data

    def post_v5_gists_with_http_info(self, files, description, **kwargs):
        """
        创建代码片段
        创建代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists_with_http_info(files, description, async=True)
        >>> result = thread.get()

        :param async bool
        :param dict(str, str) files: Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } } (required)
        :param str description: 代码片段描述，1~30个字符 (required)
        :param str access_token: 用户授权码
        :param bool public: 公开/私有，默认: 私有
        :return: list[CodeForksHistory]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['files', 'description', 'access_token', 'public']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_gists" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'files' is set
        if ('files' not in params) or (params['files'] is None):
            raise ValueError("Missing the required parameter `files` when calling `post_v5_gists`")
        # verify the required parameter 'description' is set
        if ('description' not in params) or (params['description'] is None):
            raise ValueError("Missing the required parameter `description` when calling `post_v5_gists`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'files' in params:
            form_params.append(('files', params['files']))
        if 'description' in params:
            form_params.append(('description', params['description']))
        if 'public' in params:
            form_params.append(('public', params['public']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[CodeForksHistory]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_gists_gist_id_comments(self, gist_id, body, **kwargs):
        """
        增加代码片段的评论
        增加代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists_gist_id_comments(gist_id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param str body: 评论内容 (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_gists_gist_id_comments_with_http_info(gist_id, body, **kwargs)
        else:
            (data) = self.post_v5_gists_gist_id_comments_with_http_info(gist_id, body, **kwargs)
            return data

    def post_v5_gists_gist_id_comments_with_http_info(self, gist_id, body, **kwargs):
        """
        增加代码片段的评论
        增加代码片段的评论
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists_gist_id_comments_with_http_info(gist_id, body, async=True)
        >>> result = thread.get()

        :param async bool
        :param str gist_id: 代码片段的ID (required)
        :param str body: 评论内容 (required)
        :param str access_token: 用户授权码
        :return: CodeComment
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['gist_id', 'body', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_gists_gist_id_comments" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'gist_id' is set
        if ('gist_id' not in params) or (params['gist_id'] is None):
            raise ValueError("Missing the required parameter `gist_id` when calling `post_v5_gists_gist_id_comments`")
        # verify the required parameter 'body' is set
        if ('body' not in params) or (params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_v5_gists_gist_id_comments`")


        collection_formats = {}

        path_params = {}
        if 'gist_id' in params:
            path_params['gist_id'] = params['gist_id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'body' in params:
            form_params.append(('body', params['body']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{gist_id}/comments', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='CodeComment',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_gists_id_forks(self, id, **kwargs):
        """
        Fork代码片段
        Fork代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists_id_forks(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_gists_id_forks_with_http_info(id, **kwargs)
        else:
            (data) = self.post_v5_gists_id_forks_with_http_info(id, **kwargs)
            return data

    def post_v5_gists_id_forks_with_http_info(self, id, **kwargs):
        """
        Fork代码片段
        Fork代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_gists_id_forks_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_gists_id_forks" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `post_v5_gists_id_forks`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/forks', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_gists_id_star(self, id, **kwargs):
        """
        Star代码片段
        Star代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_gists_id_star(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_gists_id_star_with_http_info(id, **kwargs)
        else:
            (data) = self.put_v5_gists_id_star_with_http_info(id, **kwargs)
            return data

    def put_v5_gists_id_star_with_http_info(self, id, **kwargs):
        """
        Star代码片段
        Star代码片段
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_gists_id_star_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param str id: 代码片段的ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_gists_id_star" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `put_v5_gists_id_star`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gists/{id}/star', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

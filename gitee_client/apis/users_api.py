# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class UsersApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_user_following_username(self, username, **kwargs):
        """
        取消关注一个用户
        取消关注一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_following_username(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_following_username_with_http_info(username, **kwargs)
        else:
            (data) = self.delete_v5_user_following_username_with_http_info(username, **kwargs)
            return data

    def delete_v5_user_following_username_with_http_info(self, username, **kwargs):
        """
        取消关注一个用户
        取消关注一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_following_username_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_following_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `delete_v5_user_following_username`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/following/{username}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_user_keys_id(self, id, **kwargs):
        """
        删除一个公钥
        删除一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_keys_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_keys_id_with_http_info(id, **kwargs)
        else:
            (data) = self.delete_v5_user_keys_id_with_http_info(id, **kwargs)
            return data

    def delete_v5_user_keys_id_with_http_info(self, id, **kwargs):
        """
        删除一个公钥
        删除一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_keys_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_keys_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_v5_user_keys_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/keys/{id}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_user_unconfirmed_email(self, **kwargs):
        """
        删除授权用户未激活的邮箱地址
        删除授权用户未激活的邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_unconfirmed_email(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_unconfirmed_email_with_http_info(**kwargs)
        else:
            (data) = self.delete_v5_user_unconfirmed_email_with_http_info(**kwargs)
            return data

    def delete_v5_user_unconfirmed_email_with_http_info(self, **kwargs):
        """
        删除授权用户未激活的邮箱地址
        删除授权用户未激活的邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_unconfirmed_email_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_unconfirmed_email" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/unconfirmed_email', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user(self, **kwargs):
        """
        获取授权用户的资料
        获取授权用户的资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_with_http_info(**kwargs)
            return data

    def get_v5_user_with_http_info(self, **kwargs):
        """
        获取授权用户的资料
        获取授权用户的资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserDetail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_address(self, **kwargs):
        """
        获取授权用户的地理信息
        获取授权用户的地理信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_address(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserAddress
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_address_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_address_with_http_info(**kwargs)
            return data

    def get_v5_user_address_with_http_info(self, **kwargs):
        """
        获取授权用户的地理信息
        获取授权用户的地理信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_address_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserAddress
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_address" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/address', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserAddress',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_emails(self, **kwargs):
        """
        获取授权用户的邮箱地址
        获取授权用户的邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_emails(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserEmail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_emails_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_emails_with_http_info(**kwargs)
            return data

    def get_v5_user_emails_with_http_info(self, **kwargs):
        """
        获取授权用户的邮箱地址
        获取授权用户的邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_emails_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: UserEmail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_emails" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/emails', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserEmail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_followers(self, **kwargs):
        """
        列出授权用户的关注者
        列出授权用户的关注者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_followers(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_followers_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_followers_with_http_info(**kwargs)
            return data

    def get_v5_user_followers_with_http_info(self, **kwargs):
        """
        列出授权用户的关注者
        列出授权用户的关注者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_followers_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_followers" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/followers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_following(self, **kwargs):
        """
        列出授权用户正关注的用户
        列出授权用户正关注的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_following(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_following_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_following_with_http_info(**kwargs)
            return data

    def get_v5_user_following_with_http_info(self, **kwargs):
        """
        列出授权用户正关注的用户
        列出授权用户正关注的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_following_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_following" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/following', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_following_username(self, username, **kwargs):
        """
        检查授权用户是否关注了一个用户
        检查授权用户是否关注了一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_following_username(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_following_username_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_user_following_username_with_http_info(username, **kwargs)
            return data

    def get_v5_user_following_username_with_http_info(self, username, **kwargs):
        """
        检查授权用户是否关注了一个用户
        检查授权用户是否关注了一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_following_username_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_following_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_user_following_username`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/following/{username}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_keys(self, **kwargs):
        """
        列出授权用户的所有公钥
        列出授权用户的所有公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_keys(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKey]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_keys_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_keys_with_http_info(**kwargs)
            return data

    def get_v5_user_keys_with_http_info(self, **kwargs):
        """
        列出授权用户的所有公钥
        列出授权用户的所有公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_keys_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKey]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_keys" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/keys', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[SSHKey]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_keys_id(self, id, **kwargs):
        """
        获取一个公钥
        获取一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_keys_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_keys_id_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_user_keys_id_with_http_info(id, **kwargs)
            return data

    def get_v5_user_keys_id_with_http_info(self, id, **kwargs):
        """
        获取一个公钥
        获取一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_keys_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 公钥 ID (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_keys_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_user_keys_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/keys/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='SSHKey',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username(self, username, **kwargs):
        """
        获取一个用户
        获取一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: User
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_with_http_info(self, username, **kwargs):
        """
        获取一个用户
        获取一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: User
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='User',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_followers(self, username, **kwargs):
        """
        列出指定用户的关注者
        列出指定用户的关注者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_followers(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_followers_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_followers_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_followers_with_http_info(self, username, **kwargs):
        """
        列出指定用户的关注者
        列出指定用户的关注者
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_followers_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_followers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_followers`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/followers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_following(self, username, **kwargs):
        """
        列出指定用户正在关注的用户
        列出指定用户正在关注的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_following(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_following_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_following_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_following_with_http_info(self, username, **kwargs):
        """
        列出指定用户正在关注的用户
        列出指定用户正在关注的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_following_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_following" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_following`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/following', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_following_target_user(self, username, target_user, **kwargs):
        """
        检查指定用户是否关注目标用户
        检查指定用户是否关注目标用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_following_target_user(username, target_user, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str target_user: 目标用户的用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_following_target_user_with_http_info(username, target_user, **kwargs)
        else:
            (data) = self.get_v5_users_username_following_target_user_with_http_info(username, target_user, **kwargs)
            return data

    def get_v5_users_username_following_target_user_with_http_info(self, username, target_user, **kwargs):
        """
        检查指定用户是否关注目标用户
        检查指定用户是否关注目标用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_following_target_user_with_http_info(username, target_user, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str target_user: 目标用户的用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'target_user', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_following_target_user" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_following_target_user`")
        # verify the required parameter 'target_user' is set
        if ('target_user' not in params) or (params['target_user'] is None):
            raise ValueError("Missing the required parameter `target_user` when calling `get_v5_users_username_following_target_user`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']
        if 'target_user' in params:
            path_params['target_user'] = params['target_user']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/following/{target_user}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_keys(self, username, **kwargs):
        """
        列出指定用户的所有公钥
        列出指定用户的所有公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_keys(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKeyBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_keys_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_keys_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_keys_with_http_info(self, username, **kwargs):
        """
        列出指定用户的所有公钥
        列出指定用户的所有公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_keys_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[SSHKeyBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_keys" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_keys`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/keys', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[SSHKeyBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_user(self, **kwargs):
        """
        更新授权用户的资料
        更新授权用户的资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str name: 昵称
        :param str blog: 微博链接
        :param str weibo: 博客站点
        :param str bio: 自我介绍
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_user_with_http_info(**kwargs)
        else:
            (data) = self.patch_v5_user_with_http_info(**kwargs)
            return data

    def patch_v5_user_with_http_info(self, **kwargs):
        """
        更新授权用户的资料
        更新授权用户的资料
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str name: 昵称
        :param str blog: 微博链接
        :param str weibo: 博客站点
        :param str bio: 自我介绍
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'name', 'blog', 'weibo', 'bio']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_user" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'blog' in params:
            form_params.append(('blog', params['blog']))
        if 'weibo' in params:
            form_params.append(('weibo', params['weibo']))
        if 'bio' in params:
            form_params.append(('bio', params['bio']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserDetail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_user_address(self, **kwargs):
        """
        更新授权用户的地理信息
        更新授权用户的地理信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user_address(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str name: 联系人名
        :param str tel: 联系电话
        :param str address: 联系地址
        :param str province: 省份
        :param str city: 城市
        :param str zip_code: 邮政编码
        :param str comment: 备注
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_user_address_with_http_info(**kwargs)
        else:
            (data) = self.patch_v5_user_address_with_http_info(**kwargs)
            return data

    def patch_v5_user_address_with_http_info(self, **kwargs):
        """
        更新授权用户的地理信息
        更新授权用户的地理信息
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_user_address_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str name: 联系人名
        :param str tel: 联系电话
        :param str address: 联系地址
        :param str province: 省份
        :param str city: 城市
        :param str zip_code: 邮政编码
        :param str comment: 备注
        :return: UserDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'name', 'tel', 'address', 'province', 'city', 'zip_code', 'comment']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_user_address" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'name' in params:
            form_params.append(('name', params['name']))
        if 'tel' in params:
            form_params.append(('tel', params['tel']))
        if 'address' in params:
            form_params.append(('address', params['address']))
        if 'province' in params:
            form_params.append(('province', params['province']))
        if 'city' in params:
            form_params.append(('city', params['city']))
        if 'zip_code' in params:
            form_params.append(('zip_code', params['zip_code']))
        if 'comment' in params:
            form_params.append(('comment', params['comment']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/address', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserDetail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_user_emails(self, email, **kwargs):
        """
        添加授权用户的新邮箱地址
        添加授权用户的新邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_emails(email, async=True)
        >>> result = thread.get()

        :param async bool
        :param str email: 新的邮箱地址 (required)
        :param str access_token: 用户授权码
        :return: UserEmail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_user_emails_with_http_info(email, **kwargs)
        else:
            (data) = self.post_v5_user_emails_with_http_info(email, **kwargs)
            return data

    def post_v5_user_emails_with_http_info(self, email, **kwargs):
        """
        添加授权用户的新邮箱地址
        添加授权用户的新邮箱地址
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_emails_with_http_info(email, async=True)
        >>> result = thread.get()

        :param async bool
        :param str email: 新的邮箱地址 (required)
        :param str access_token: 用户授权码
        :return: UserEmail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['email', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_user_emails" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'email' is set
        if ('email' not in params) or (params['email'] is None):
            raise ValueError("Missing the required parameter `email` when calling `post_v5_user_emails`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'email' in params:
            form_params.append(('email', params['email']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/emails', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserEmail',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_user_keys(self, key, title, **kwargs):
        """
        添加一个公钥
        添加一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_keys(key, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str key: 公钥内容. (required)
        :param str title: 公钥名称 (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_user_keys_with_http_info(key, title, **kwargs)
        else:
            (data) = self.post_v5_user_keys_with_http_info(key, title, **kwargs)
            return data

    def post_v5_user_keys_with_http_info(self, key, title, **kwargs):
        """
        添加一个公钥
        添加一个公钥
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_user_keys_with_http_info(key, title, async=True)
        >>> result = thread.get()

        :param async bool
        :param str key: 公钥内容. (required)
        :param str title: 公钥名称 (required)
        :param str access_token: 用户授权码
        :return: SSHKey
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['key', 'title', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_user_keys" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'key' is set
        if ('key' not in params) or (params['key'] is None):
            raise ValueError("Missing the required parameter `key` when calling `post_v5_user_keys`")
        # verify the required parameter 'title' is set
        if ('title' not in params) or (params['title'] is None):
            raise ValueError("Missing the required parameter `title` when calling `post_v5_user_keys`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'key' in params:
            form_params.append(('key', params['key']))
        if 'title' in params:
            form_params.append(('title', params['title']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/keys', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='SSHKey',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_user_following_username(self, username, **kwargs):
        """
        关注一个用户
        关注一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_following_username(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_user_following_username_with_http_info(username, **kwargs)
        else:
            (data) = self.put_v5_user_following_username_with_http_info(username, **kwargs)
            return data

    def put_v5_user_following_username_with_http_info(self, username, **kwargs):
        """
        关注一个用户
        关注一个用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_following_username_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_user_following_username" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `put_v5_user_following_username`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/following/{username}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class ActivityApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_v5_user_starred_owner_repo(self, owner, repo, **kwargs):
        """
        取消 star 一个项目
        取消 star 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_starred_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.delete_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def delete_v5_user_starred_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        取消 star 一个项目
        取消 star 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_starred_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_starred_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_user_starred_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_user_starred_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/starred/{owner}/{repo}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def delete_v5_user_subscriptions_owner_repo(self, owner, repo, **kwargs):
        """
        取消 watch 一个项目
        取消 watch 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_subscriptions_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.delete_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.delete_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def delete_v5_user_subscriptions_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        取消 watch 一个项目
        取消 watch 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.delete_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_v5_user_subscriptions_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `delete_v5_user_subscriptions_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `delete_v5_user_subscriptions_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/subscriptions/{owner}/{repo}', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_events(self, **kwargs):
        """
        获取站内所有公开动态
        获取站内所有公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_events(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_events_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_events_with_http_info(**kwargs)
            return data

    def get_v5_events_with_http_info(self, **kwargs):
        """
        获取站内所有公开动态
        获取站内所有公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_events_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_events" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_networks_owner_repo_events(self, owner, repo, **kwargs):
        """
        列出项目的所有公开动态
        列出项目的所有公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_networks_owner_repo_events(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_networks_owner_repo_events_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_networks_owner_repo_events_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_networks_owner_repo_events_with_http_info(self, owner, repo, **kwargs):
        """
        列出项目的所有公开动态
        列出项目的所有公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_networks_owner_repo_events_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_networks_owner_repo_events" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_networks_owner_repo_events`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_networks_owner_repo_events`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/networks/{owner}/{repo}/events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_notifications_messages(self, **kwargs):
        """
        列出授权用户的所有私信
        列出授权用户的所有私信
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_messages(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读私信，默认：否
        :param str since: 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserMessage]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_notifications_messages_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_notifications_messages_with_http_info(**kwargs)
            return data

    def get_v5_notifications_messages_with_http_info(self, **kwargs):
        """
        列出授权用户的所有私信
        列出授权用户的所有私信
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_messages_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读私信，默认：否
        :param str since: 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserMessage]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'unread', 'since', 'before', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_notifications_messages" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'unread' in params:
            query_params.append(('unread', params['unread']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'before' in params:
            query_params.append(('before', params['before']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/messages', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserMessage]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_notifications_messages_id(self, id, **kwargs):
        """
        获取一个私信
        获取一个私信
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_messages_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 私信的 ID (required)
        :param str access_token: 用户授权码
        :return: UserMessage
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_notifications_messages_id_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_notifications_messages_id_with_http_info(id, **kwargs)
            return data

    def get_v5_notifications_messages_id_with_http_info(self, id, **kwargs):
        """
        获取一个私信
        获取一个私信
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_messages_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 私信的 ID (required)
        :param str access_token: 用户授权码
        :return: UserMessage
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_notifications_messages_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_notifications_messages_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/messages/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserMessage',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_notifications_threads(self, **kwargs):
        """
        列出授权用户的所有通知
        列出授权用户的所有通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_threads(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读消息，默认：否
        :param bool participating: 是否只显示自己直接参与的消息，默认：否
        :param str since: 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserNotification]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_notifications_threads_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_notifications_threads_with_http_info(**kwargs)
            return data

    def get_v5_notifications_threads_with_http_info(self, **kwargs):
        """
        列出授权用户的所有通知
        列出授权用户的所有通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_threads_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读消息，默认：否
        :param bool participating: 是否只显示自己直接参与的消息，默认：否
        :param str since: 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserNotification]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'unread', 'participating', 'since', 'before', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_notifications_threads" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'unread' in params:
            query_params.append(('unread', params['unread']))
        if 'participating' in params:
            query_params.append(('participating', params['participating']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'before' in params:
            query_params.append(('before', params['before']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/threads', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserNotification]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_notifications_threads_id(self, id, **kwargs):
        """
        获取一个通知
        获取一个通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_threads_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 通知的 ID (required)
        :param str access_token: 用户授权码
        :return: UserNotification
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_notifications_threads_id_with_http_info(id, **kwargs)
        else:
            (data) = self.get_v5_notifications_threads_id_with_http_info(id, **kwargs)
            return data

    def get_v5_notifications_threads_id_with_http_info(self, id, **kwargs):
        """
        获取一个通知
        获取一个通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_notifications_threads_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 通知的 ID (required)
        :param str access_token: 用户授权码
        :return: UserNotification
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_notifications_threads_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_v5_notifications_threads_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/threads/{id}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='UserNotification',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_orgs_org_events(self, org, **kwargs):
        """
        列出组织的公开动态
        列出组织的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_events(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_orgs_org_events_with_http_info(org, **kwargs)
        else:
            (data) = self.get_v5_orgs_org_events_with_http_info(org, **kwargs)
            return data

    def get_v5_orgs_org_events_with_http_info(self, org, **kwargs):
        """
        列出组织的公开动态
        列出组织的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_orgs_org_events_with_http_info(org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['org', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_orgs_org_events" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_orgs_org_events`")


        collection_formats = {}

        path_params = {}
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/orgs/{org}/events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_events(self, owner, repo, **kwargs):
        """
        列出项目的所有动态
        列出项目的所有动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_events(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_events_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_events_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_events_with_http_info(self, owner, repo, **kwargs):
        """
        列出项目的所有动态
        列出项目的所有动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_events_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_events" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_events`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_events`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_notifications(self, owner, repo, **kwargs):
        """
        列出一个项目里的通知
        列出一个项目里的通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_notifications(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读消息，默认：否
        :param bool participating: 是否只显示自己直接参与的消息，默认：否
        :param str since: 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserNotification]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_notifications_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_notifications_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_notifications_with_http_info(self, owner, repo, **kwargs):
        """
        列出一个项目里的通知
        列出一个项目里的通知
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_notifications_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param bool unread: 是否只显示未读消息，默认：否
        :param bool participating: 是否只显示自己直接参与的消息，默认：否
        :param str since: 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
        :param str before: 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserNotification]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'unread', 'participating', 'since', 'before', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_notifications" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_notifications`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_notifications`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'unread' in params:
            query_params.append(('unread', params['unread']))
        if 'participating' in params:
            query_params.append(('participating', params['participating']))
        if 'since' in params:
            query_params.append(('since', params['since']))
        if 'before' in params:
            query_params.append(('before', params['before']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/notifications', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserNotification]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_stargazers(self, owner, repo, **kwargs):
        """
        列出 star 了项目的用户
        列出 star 了项目的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_stargazers(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_stargazers_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_stargazers_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_stargazers_with_http_info(self, owner, repo, **kwargs):
        """
        列出 star 了项目的用户
        列出 star 了项目的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_stargazers_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_stargazers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_stargazers`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_stargazers`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/stargazers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_subscribers(self, owner, repo, **kwargs):
        """
        列出 watch 了项目的用户
        列出 watch 了项目的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_subscribers(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_subscribers_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_subscribers_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_subscribers_with_http_info(self, owner, repo, **kwargs):
        """
        列出 watch 了项目的用户
        列出 watch 了项目的用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_subscribers_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[UserBasic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_subscribers" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_subscribers`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_subscribers`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/subscribers', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[UserBasic]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_starred(self, **kwargs):
        """
        列出授权用户 star 了的项目
        列出授权用户 star 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_starred(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_starred_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_starred_with_http_info(**kwargs)
            return data

    def get_v5_user_starred_with_http_info(self, **kwargs):
        """
        列出授权用户 star 了的项目
        列出授权用户 star 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_starred_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_starred" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/starred', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Project]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_starred_owner_repo(self, owner, repo, **kwargs):
        """
        检查授权用户是否 star 了一个项目
        检查授权用户是否 star 了一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_starred_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_user_starred_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        检查授权用户是否 star 了一个项目
        检查授权用户是否 star 了一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_starred_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_starred_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_user_starred_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_user_starred_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/starred/{owner}/{repo}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_subscriptions(self, **kwargs):
        """
        列出授权用户 watch 了的项目
        列出授权用户 watch 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_subscriptions(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_subscriptions_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_user_subscriptions_with_http_info(**kwargs)
            return data

    def get_v5_user_subscriptions_with_http_info(self, **kwargs):
        """
        列出授权用户 watch 了的项目
        列出授权用户 watch 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_subscriptions_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token', 'sort', 'direction', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_subscriptions" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/subscriptions', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Project]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_user_subscriptions_owner_repo(self, owner, repo, **kwargs):
        """
        检查授权用户是否 watch 了一个项目
        检查授权用户是否 watch 了一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_subscriptions_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_user_subscriptions_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        检查授权用户是否 watch 了一个项目
        检查授权用户是否 watch 了一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_user_subscriptions_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_user_subscriptions_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_user_subscriptions_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/subscriptions/{owner}/{repo}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_events(self, username, **kwargs):
        """
        列出用户的动态
        列出用户的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_events_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_events_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_events_with_http_info(self, username, **kwargs):
        """
        列出用户的动态
        列出用户的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_events" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_events`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_events_orgs_org(self, username, org, **kwargs):
        """
        列出用户所属组织的动态
        列出用户所属组织的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events_orgs_org(username, org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_events_orgs_org_with_http_info(username, org, **kwargs)
        else:
            (data) = self.get_v5_users_username_events_orgs_org_with_http_info(username, org, **kwargs)
            return data

    def get_v5_users_username_events_orgs_org_with_http_info(self, username, org, **kwargs):
        """
        列出用户所属组织的动态
        列出用户所属组织的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events_orgs_org_with_http_info(username, org, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str org: 组织的路径(path/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'org', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_events_orgs_org" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_events_orgs_org`")
        # verify the required parameter 'org' is set
        if ('org' not in params) or (params['org'] is None):
            raise ValueError("Missing the required parameter `org` when calling `get_v5_users_username_events_orgs_org`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']
        if 'org' in params:
            path_params['org'] = params['org']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/events/orgs/{org}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_events_public(self, username, **kwargs):
        """
        列出用户的公开动态
        列出用户的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events_public(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_events_public_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_events_public_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_events_public_with_http_info(self, username, **kwargs):
        """
        列出用户的公开动态
        列出用户的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_events_public_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_events_public" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_events_public`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/events/public', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_received_events(self, username, **kwargs):
        """
        列出一个用户收到的动态
        列出一个用户收到的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_received_events(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_received_events_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_received_events_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_received_events_with_http_info(self, username, **kwargs):
        """
        列出一个用户收到的动态
        列出一个用户收到的动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_received_events_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_received_events" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_received_events`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/received_events', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_received_events_public(self, username, **kwargs):
        """
        列出一个用户收到的公开动态
        列出一个用户收到的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_received_events_public(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_received_events_public_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_received_events_public_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_received_events_public_with_http_info(self, username, **kwargs):
        """
        列出一个用户收到的公开动态
        列出一个用户收到的公开动态
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_received_events_public_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :return: list[Event]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_received_events_public" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_received_events_public`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/received_events/public', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Event]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_starred(self, username, **kwargs):
        """
        列出用户 star 了的项目
        列出用户 star 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_starred(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_starred_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_starred_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_starred_with_http_info(self, username, **kwargs):
        """
        列出用户 star 了的项目
        列出用户 star 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_starred_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page', 'sort', 'direction']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_starred" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_starred`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/starred', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Project]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_users_username_subscriptions(self, username, **kwargs):
        """
        列出用户 watch 了的项目
        列出用户 watch 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_subscriptions(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_users_username_subscriptions_with_http_info(username, **kwargs)
        else:
            (data) = self.get_v5_users_username_subscriptions_with_http_info(username, **kwargs)
            return data

    def get_v5_users_username_subscriptions_with_http_info(self, username, **kwargs):
        """
        列出用户 watch 了的项目
        列出用户 watch 了的项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_users_username_subscriptions_with_http_info(username, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量
        :param str sort: 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
        :param str direction: 按递增(asc)或递减(desc)排序，默认：递减
        :return: list[Project]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'access_token', 'page', 'per_page', 'sort', 'direction']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_users_username_subscriptions" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `get_v5_users_username_subscriptions`")


        collection_formats = {}

        path_params = {}
        if 'username' in params:
            path_params['username'] = params['username']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))
        if 'page' in params:
            query_params.append(('page', params['page']))
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))
        if 'sort' in params:
            query_params.append(('sort', params['sort']))
        if 'direction' in params:
            query_params.append(('direction', params['direction']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/users/{username}/subscriptions', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[Project]',
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_notifications_messages_id(self, id, **kwargs):
        """
        标记一个私信为已读
        标记一个私信为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_notifications_messages_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 私信的 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_notifications_messages_id_with_http_info(id, **kwargs)
        else:
            (data) = self.patch_v5_notifications_messages_id_with_http_info(id, **kwargs)
            return data

    def patch_v5_notifications_messages_id_with_http_info(self, id, **kwargs):
        """
        标记一个私信为已读
        标记一个私信为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_notifications_messages_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 私信的 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_notifications_messages_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_notifications_messages_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/messages/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def patch_v5_notifications_threads_id(self, id, **kwargs):
        """
        标记一个通知为已读
        标记一个通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_notifications_threads_id(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 通知的 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.patch_v5_notifications_threads_id_with_http_info(id, **kwargs)
        else:
            (data) = self.patch_v5_notifications_threads_id_with_http_info(id, **kwargs)
            return data

    def patch_v5_notifications_threads_id_with_http_info(self, id, **kwargs):
        """
        标记一个通知为已读
        标记一个通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.patch_v5_notifications_threads_id_with_http_info(id, async=True)
        >>> result = thread.get()

        :param async bool
        :param int id: 通知的 ID (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method patch_v5_notifications_threads_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params) or (params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `patch_v5_notifications_threads_id`")


        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/threads/{id}', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_notifications_messages(self, username, content, **kwargs):
        """
        发送私信给指定用户
        发送私信给指定用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_notifications_messages(username, content, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str content: 私信内容 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_notifications_messages_with_http_info(username, content, **kwargs)
        else:
            (data) = self.post_v5_notifications_messages_with_http_info(username, content, **kwargs)
            return data

    def post_v5_notifications_messages_with_http_info(self, username, content, **kwargs):
        """
        发送私信给指定用户
        发送私信给指定用户
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_notifications_messages_with_http_info(username, content, async=True)
        >>> result = thread.get()

        :param async bool
        :param str username: 用户名(username/login) (required)
        :param str content: 私信内容 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['username', 'content', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_notifications_messages" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'username' is set
        if ('username' not in params) or (params['username'] is None):
            raise ValueError("Missing the required parameter `username` when calling `post_v5_notifications_messages`")
        # verify the required parameter 'content' is set
        if ('content' not in params) or (params['content'] is None):
            raise ValueError("Missing the required parameter `content` when calling `post_v5_notifications_messages`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'username' in params:
            form_params.append(('username', params['username']))
        if 'content' in params:
            form_params.append(('content', params['content']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/messages', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_notifications_messages(self, **kwargs):
        """
        标记所有私信为已读
        标记所有私信为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_notifications_messages(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_notifications_messages_with_http_info(**kwargs)
        else:
            (data) = self.put_v5_notifications_messages_with_http_info(**kwargs)
            return data

    def put_v5_notifications_messages_with_http_info(self, **kwargs):
        """
        标记所有私信为已读
        标记所有私信为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_notifications_messages_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_notifications_messages" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/messages', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_notifications_threads(self, **kwargs):
        """
        标记所有通知为已读
        标记所有通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_notifications_threads(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_notifications_threads_with_http_info(**kwargs)
        else:
            (data) = self.put_v5_notifications_threads_with_http_info(**kwargs)
            return data

    def put_v5_notifications_threads_with_http_info(self, **kwargs):
        """
        标记所有通知为已读
        标记所有通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_notifications_threads_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_notifications_threads" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/notifications/threads', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_repos_owner_repo_notifications(self, owner, repo, **kwargs):
        """
        标记一个项目里的通知为已读
        标记一个项目里的通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_notifications(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_repos_owner_repo_notifications_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.put_v5_repos_owner_repo_notifications_with_http_info(owner, repo, **kwargs)
            return data

    def put_v5_repos_owner_repo_notifications_with_http_info(self, owner, repo, **kwargs):
        """
        标记一个项目里的通知为已读
        标记一个项目里的通知为已读
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_repos_owner_repo_notifications_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_repos_owner_repo_notifications" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_repos_owner_repo_notifications`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_repos_owner_repo_notifications`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/notifications', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_user_starred_owner_repo(self, owner, repo, **kwargs):
        """
        star 一个项目
        star 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_starred_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.put_v5_user_starred_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def put_v5_user_starred_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        star 一个项目
        star 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_starred_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_user_starred_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_user_starred_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_user_starred_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/starred/{owner}/{repo}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def put_v5_user_subscriptions_owner_repo(self, owner, repo, **kwargs):
        """
        watch 一个项目
        watch 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_subscriptions_owner_repo(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.put_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.put_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, **kwargs)
            return data

    def put_v5_user_subscriptions_owner_repo_with_http_info(self, owner, repo, **kwargs):
        """
        watch 一个项目
        watch 一个项目
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.put_v5_user_subscriptions_owner_repo_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_v5_user_subscriptions_owner_repo" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `put_v5_user_subscriptions_owner_repo`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `put_v5_user_subscriptions_owner_repo`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/user/subscriptions/{owner}/{repo}', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

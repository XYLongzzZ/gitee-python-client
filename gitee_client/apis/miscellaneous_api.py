# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..api_client import ApiClient


class MiscellaneousApi(object):
    """

    Do not edit the class manually.

    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_v5_emojis(self, **kwargs):
        """
        列出可使用的 Emoji
        列出可使用的 Emoji
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_emojis(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_emojis_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_emojis_with_http_info(**kwargs)
            return data

    def get_v5_emojis_with_http_info(self, **kwargs):
        """
        列出可使用的 Emoji
        列出可使用的 Emoji
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_emojis_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_emojis" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/emojis', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gitignore_templates(self, **kwargs):
        """
        列出可使用的 .gitignore 模板
        列出可使用的 .gitignore 模板
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gitignore_templates_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_gitignore_templates_with_http_info(**kwargs)
            return data

    def get_v5_gitignore_templates_with_http_info(self, **kwargs):
        """
        列出可使用的 .gitignore 模板
        列出可使用的 .gitignore 模板
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gitignore_templates" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gitignore/templates', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gitignore_templates_name(self, name, **kwargs):
        """
        获取一个 .gitignore 模板
        获取一个 .gitignore 模板
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates_name(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: .gitignore 模板名 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gitignore_templates_name_with_http_info(name, **kwargs)
        else:
            (data) = self.get_v5_gitignore_templates_name_with_http_info(name, **kwargs)
            return data

    def get_v5_gitignore_templates_name_with_http_info(self, name, **kwargs):
        """
        获取一个 .gitignore 模板
        获取一个 .gitignore 模板
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates_name_with_http_info(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: .gitignore 模板名 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['name', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gitignore_templates_name" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `get_v5_gitignore_templates_name`")


        collection_formats = {}

        path_params = {}
        if 'name' in params:
            path_params['name'] = params['name']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gitignore/templates/{name}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_gitignore_templates_name_raw(self, name, **kwargs):
        """
        获取一个 .gitignore 模板原始文件
        获取一个 .gitignore 模板原始文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates_name_raw(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: .gitignore 模板名 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_gitignore_templates_name_raw_with_http_info(name, **kwargs)
        else:
            (data) = self.get_v5_gitignore_templates_name_raw_with_http_info(name, **kwargs)
            return data

    def get_v5_gitignore_templates_name_raw_with_http_info(self, name, **kwargs):
        """
        获取一个 .gitignore 模板原始文件
        获取一个 .gitignore 模板原始文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_gitignore_templates_name_raw_with_http_info(name, async=True)
        >>> result = thread.get()

        :param async bool
        :param str name: .gitignore 模板名 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['name', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_gitignore_templates_name_raw" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'name' is set
        if ('name' not in params) or (params['name'] is None):
            raise ValueError("Missing the required parameter `name` when calling `get_v5_gitignore_templates_name_raw`")


        collection_formats = {}

        path_params = {}
        if 'name' in params:
            path_params['name'] = params['name']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/gitignore/templates/{name}/raw', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_licenses(self, **kwargs):
        """
        列出可使用的开源许可协议
        列出可使用的开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_licenses_with_http_info(**kwargs)
        else:
            (data) = self.get_v5_licenses_with_http_info(**kwargs)
            return data

    def get_v5_licenses_with_http_info(self, **kwargs):
        """
        列出可使用的开源许可协议
        列出可使用的开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses_with_http_info(async=True)
        >>> result = thread.get()

        :param async bool
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_licenses" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/licenses', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_licenses_license(self, license, **kwargs):
        """
        获取一个开源许可协议
        获取一个开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses_license(license, async=True)
        >>> result = thread.get()

        :param async bool
        :param str license: 协议名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_licenses_license_with_http_info(license, **kwargs)
        else:
            (data) = self.get_v5_licenses_license_with_http_info(license, **kwargs)
            return data

    def get_v5_licenses_license_with_http_info(self, license, **kwargs):
        """
        获取一个开源许可协议
        获取一个开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses_license_with_http_info(license, async=True)
        >>> result = thread.get()

        :param async bool
        :param str license: 协议名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['license', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_licenses_license" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'license' is set
        if ('license' not in params) or (params['license'] is None):
            raise ValueError("Missing the required parameter `license` when calling `get_v5_licenses_license`")


        collection_formats = {}

        path_params = {}
        if 'license' in params:
            path_params['license'] = params['license']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/licenses/{license}', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_licenses_license_raw(self, license, **kwargs):
        """
        获取一个开源许可协议原始文件
        获取一个开源许可协议原始文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses_license_raw(license, async=True)
        >>> result = thread.get()

        :param async bool
        :param str license: 协议名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_licenses_license_raw_with_http_info(license, **kwargs)
        else:
            (data) = self.get_v5_licenses_license_raw_with_http_info(license, **kwargs)
            return data

    def get_v5_licenses_license_raw_with_http_info(self, license, **kwargs):
        """
        获取一个开源许可协议原始文件
        获取一个开源许可协议原始文件
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_licenses_license_raw_with_http_info(license, async=True)
        >>> result = thread.get()

        :param async bool
        :param str license: 协议名称 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['license', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_licenses_license_raw" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'license' is set
        if ('license' not in params) or (params['license'] is None):
            raise ValueError("Missing the required parameter `license` when calling `get_v5_licenses_license_raw`")


        collection_formats = {}

        path_params = {}
        if 'license' in params:
            path_params['license'] = params['license']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/licenses/{license}/raw', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def get_v5_repos_owner_repo_license(self, owner, repo, **kwargs):
        """
        获取一个项目使用的开源许可协议
        获取一个项目使用的开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_license(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.get_v5_repos_owner_repo_license_with_http_info(owner, repo, **kwargs)
        else:
            (data) = self.get_v5_repos_owner_repo_license_with_http_info(owner, repo, **kwargs)
            return data

    def get_v5_repos_owner_repo_license_with_http_info(self, owner, repo, **kwargs):
        """
        获取一个项目使用的开源许可协议
        获取一个项目使用的开源许可协议
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.get_v5_repos_owner_repo_license_with_http_info(owner, repo, async=True)
        >>> result = thread.get()

        :param async bool
        :param str owner: 用户名(username/login) (required)
        :param str repo: 项目路径(path) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['owner', 'repo', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_v5_repos_owner_repo_license" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'owner' is set
        if ('owner' not in params) or (params['owner'] is None):
            raise ValueError("Missing the required parameter `owner` when calling `get_v5_repos_owner_repo_license`")
        # verify the required parameter 'repo' is set
        if ('repo' not in params) or (params['repo'] is None):
            raise ValueError("Missing the required parameter `repo` when calling `get_v5_repos_owner_repo_license`")


        collection_formats = {}

        path_params = {}
        if 'owner' in params:
            path_params['owner'] = params['owner']
        if 'repo' in params:
            path_params['repo'] = params['repo']

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/repos/{owner}/{repo}/license', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def post_v5_markdown(self, text, **kwargs):
        """
        渲染 Markdown 文本
        渲染 Markdown 文本
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_markdown(text, async=True)
        >>> result = thread.get()

        :param async bool
        :param str text: Markdown 文本 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.post_v5_markdown_with_http_info(text, **kwargs)
        else:
            (data) = self.post_v5_markdown_with_http_info(text, **kwargs)
            return data

    def post_v5_markdown_with_http_info(self, text, **kwargs):
        """
        渲染 Markdown 文本
        渲染 Markdown 文本
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.post_v5_markdown_with_http_info(text, async=True)
        >>> result = thread.get()

        :param async bool
        :param str text: Markdown 文本 (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['text', 'access_token']
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_v5_markdown" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'text' is set
        if ('text' not in params) or (params['text'] is None):
            raise ValueError("Missing the required parameter `text` when calling `post_v5_markdown`")


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'access_token' in params:
            form_params.append(('access_token', params['access_token']))
        if 'text' in params:
            form_params.append(('text', params['text']))

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/v5/markdown', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        async=params.get('async'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

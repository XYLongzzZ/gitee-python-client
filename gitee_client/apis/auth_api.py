# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""

from __future__ import absolute_import

import requests


# python 2 and python 3 compatibility library


class AuthApi(object):
    def get_token(self, username, password, redirect_uri, client_id, client_secret, scope):
        query_params = {"username": username,
                        "password": password,
                        "redirect_uri": redirect_uri,
                        "client_id": client_id,
                        "client_secret": client_secret,
                        "grant_type": "password",
                        "scope": scope
                        }

        return requests.post("https://gitee.com/oauth/token", query_params, json=True).json()

    def get_token(self, code, redirect_uri, client_id, client_secret, scope):
        query_params = {"code": code,
                        "redirect_uri": redirect_uri,
                        "client_id": client_id,
                        "client_secret": client_secret,
                        "grant_type": "code",
                        "scope": scope
                        }

        return requests.post("https://gitee.com/oauth/token", query_params, json=True).json()

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class UserAddress(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'tel': 'str',
        'address': 'str',
        'province': 'str',
        'city': 'str',
        'zip_code': 'str',
        'comment': 'str'
    }

    attribute_map = {
        'name': 'name',
        'tel': 'tel',
        'address': 'address',
        'province': 'province',
        'city': 'city',
        'zip_code': 'zip_code',
        'comment': 'comment'
    }

    def __init__(self, name=None, tel=None, address=None, province=None, city=None, zip_code=None, comment=None):
        """
        UserAddress - a model defined in Swagger
        """

        self._name = None
        self._tel = None
        self._address = None
        self._province = None
        self._city = None
        self._zip_code = None
        self._comment = None
        self.discriminator = None

        if name is not None:
          self.name = name
        if tel is not None:
          self.tel = tel
        if address is not None:
          self.address = address
        if province is not None:
          self.province = province
        if city is not None:
          self.city = city
        if zip_code is not None:
          self.zip_code = zip_code
        if comment is not None:
          self.comment = comment

    @property
    def name(self):
        """
        Gets the name of this UserAddress.

        :return: The name of this UserAddress.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this UserAddress.

        :param name: The name of this UserAddress.
        :type: str
        """

        self._name = name

    @property
    def tel(self):
        """
        Gets the tel of this UserAddress.

        :return: The tel of this UserAddress.
        :rtype: str
        """
        return self._tel

    @tel.setter
    def tel(self, tel):
        """
        Sets the tel of this UserAddress.

        :param tel: The tel of this UserAddress.
        :type: str
        """

        self._tel = tel

    @property
    def address(self):
        """
        Gets the address of this UserAddress.

        :return: The address of this UserAddress.
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """
        Sets the address of this UserAddress.

        :param address: The address of this UserAddress.
        :type: str
        """

        self._address = address

    @property
    def province(self):
        """
        Gets the province of this UserAddress.

        :return: The province of this UserAddress.
        :rtype: str
        """
        return self._province

    @province.setter
    def province(self, province):
        """
        Sets the province of this UserAddress.

        :param province: The province of this UserAddress.
        :type: str
        """

        self._province = province

    @property
    def city(self):
        """
        Gets the city of this UserAddress.

        :return: The city of this UserAddress.
        :rtype: str
        """
        return self._city

    @city.setter
    def city(self, city):
        """
        Sets the city of this UserAddress.

        :param city: The city of this UserAddress.
        :type: str
        """

        self._city = city

    @property
    def zip_code(self):
        """
        Gets the zip_code of this UserAddress.

        :return: The zip_code of this UserAddress.
        :rtype: str
        """
        return self._zip_code

    @zip_code.setter
    def zip_code(self, zip_code):
        """
        Sets the zip_code of this UserAddress.

        :param zip_code: The zip_code of this UserAddress.
        :type: str
        """

        self._zip_code = zip_code

    @property
    def comment(self):
        """
        Gets the comment of this UserAddress.

        :return: The comment of this UserAddress.
        :rtype: str
        """
        return self._comment

    @comment.setter
    def comment(self, comment):
        """
        Sets the comment of this UserAddress.

        :param comment: The comment of this UserAddress.
        :type: str
        """

        self._comment = comment

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, UserAddress):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class Compare(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'base_commit': 'str',
        'merge_base_commit': 'str',
        'commits': 'str',
        'files': 'str'
    }

    attribute_map = {
        'base_commit': 'base_commit',
        'merge_base_commit': 'merge_base_commit',
        'commits': 'commits',
        'files': 'files'
    }

    def __init__(self, base_commit=None, merge_base_commit=None, commits=None, files=None):
        """
        Compare - a model defined in Swagger
        """

        self._base_commit = None
        self._merge_base_commit = None
        self._commits = None
        self._files = None
        self.discriminator = None

        if base_commit is not None:
          self.base_commit = base_commit
        if merge_base_commit is not None:
          self.merge_base_commit = merge_base_commit
        if commits is not None:
          self.commits = commits
        if files is not None:
          self.files = files

    @property
    def base_commit(self):
        """
        Gets the base_commit of this Compare.

        :return: The base_commit of this Compare.
        :rtype: str
        """
        return self._base_commit

    @base_commit.setter
    def base_commit(self, base_commit):
        """
        Sets the base_commit of this Compare.

        :param base_commit: The base_commit of this Compare.
        :type: str
        """

        self._base_commit = base_commit

    @property
    def merge_base_commit(self):
        """
        Gets the merge_base_commit of this Compare.

        :return: The merge_base_commit of this Compare.
        :rtype: str
        """
        return self._merge_base_commit

    @merge_base_commit.setter
    def merge_base_commit(self, merge_base_commit):
        """
        Sets the merge_base_commit of this Compare.

        :param merge_base_commit: The merge_base_commit of this Compare.
        :type: str
        """

        self._merge_base_commit = merge_base_commit

    @property
    def commits(self):
        """
        Gets the commits of this Compare.

        :return: The commits of this Compare.
        :rtype: str
        """
        return self._commits

    @commits.setter
    def commits(self, commits):
        """
        Sets the commits of this Compare.

        :param commits: The commits of this Compare.
        :type: str
        """

        self._commits = commits

    @property
    def files(self):
        """
        Gets the files of this Compare.

        :return: The files of this Compare.
        :rtype: str
        """
        return self._files

    @files.setter
    def files(self, files):
        """
        Sets the files of this Compare.

        :param files: The files of this Compare.
        :type: str
        """

        self._files = files

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Compare):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

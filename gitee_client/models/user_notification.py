# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class UserNotification(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'repository': 'ProjectBasic',
        'unread': 'str',
        'mute': 'str',
        'subject': 'str',
        'updated_at': 'str',
        'url': 'str'
    }

    attribute_map = {
        'id': 'id',
        'repository': 'repository',
        'unread': 'unread',
        'mute': 'mute',
        'subject': 'subject',
        'updated_at': 'updated_at',
        'url': 'url'
    }

    def __init__(self, id=None, repository=None, unread=None, mute=None, subject=None, updated_at=None, url=None):
        """
        UserNotification - a model defined in Swagger
        """

        self._id = None
        self._repository = None
        self._unread = None
        self._mute = None
        self._subject = None
        self._updated_at = None
        self._url = None
        self.discriminator = None

        if id is not None:
          self.id = id
        if repository is not None:
          self.repository = repository
        if unread is not None:
          self.unread = unread
        if mute is not None:
          self.mute = mute
        if subject is not None:
          self.subject = subject
        if updated_at is not None:
          self.updated_at = updated_at
        if url is not None:
          self.url = url

    @property
    def id(self):
        """
        Gets the id of this UserNotification.

        :return: The id of this UserNotification.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this UserNotification.

        :param id: The id of this UserNotification.
        :type: str
        """

        self._id = id

    @property
    def repository(self):
        """
        Gets the repository of this UserNotification.

        :return: The repository of this UserNotification.
        :rtype: ProjectBasic
        """
        return self._repository

    @repository.setter
    def repository(self, repository):
        """
        Sets the repository of this UserNotification.

        :param repository: The repository of this UserNotification.
        :type: ProjectBasic
        """

        self._repository = repository

    @property
    def unread(self):
        """
        Gets the unread of this UserNotification.

        :return: The unread of this UserNotification.
        :rtype: str
        """
        return self._unread

    @unread.setter
    def unread(self, unread):
        """
        Sets the unread of this UserNotification.

        :param unread: The unread of this UserNotification.
        :type: str
        """

        self._unread = unread

    @property
    def mute(self):
        """
        Gets the mute of this UserNotification.

        :return: The mute of this UserNotification.
        :rtype: str
        """
        return self._mute

    @mute.setter
    def mute(self, mute):
        """
        Sets the mute of this UserNotification.

        :param mute: The mute of this UserNotification.
        :type: str
        """

        self._mute = mute

    @property
    def subject(self):
        """
        Gets the subject of this UserNotification.

        :return: The subject of this UserNotification.
        :rtype: str
        """
        return self._subject

    @subject.setter
    def subject(self, subject):
        """
        Sets the subject of this UserNotification.

        :param subject: The subject of this UserNotification.
        :type: str
        """

        self._subject = subject

    @property
    def updated_at(self):
        """
        Gets the updated_at of this UserNotification.

        :return: The updated_at of this UserNotification.
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """
        Sets the updated_at of this UserNotification.

        :param updated_at: The updated_at of this UserNotification.
        :type: str
        """

        self._updated_at = updated_at

    @property
    def url(self):
        """
        Gets the url of this UserNotification.

        :return: The url of this UserNotification.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this UserNotification.

        :param url: The url of this UserNotification.
        :type: str
        """

        self._url = url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, UserNotification):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

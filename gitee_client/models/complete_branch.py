# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class CompleteBranch(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'commit': 'str',
        'links': 'str',
        'protected': 'str',
        'protection_url': 'str'
    }

    attribute_map = {
        'name': 'name',
        'commit': 'commit',
        'links': '_links',
        'protected': 'protected',
        'protection_url': 'protection_url'
    }

    def __init__(self, name=None, commit=None, links=None, protected=None, protection_url=None):
        """
        CompleteBranch - a model defined in Swagger
        """

        self._name = None
        self._commit = None
        self._links = None
        self._protected = None
        self._protection_url = None
        self.discriminator = None

        if name is not None:
          self.name = name
        if commit is not None:
          self.commit = commit
        if links is not None:
          self.links = links
        if protected is not None:
          self.protected = protected
        if protection_url is not None:
          self.protection_url = protection_url

    @property
    def name(self):
        """
        Gets the name of this CompleteBranch.

        :return: The name of this CompleteBranch.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this CompleteBranch.

        :param name: The name of this CompleteBranch.
        :type: str
        """

        self._name = name

    @property
    def commit(self):
        """
        Gets the commit of this CompleteBranch.

        :return: The commit of this CompleteBranch.
        :rtype: str
        """
        return self._commit

    @commit.setter
    def commit(self, commit):
        """
        Sets the commit of this CompleteBranch.

        :param commit: The commit of this CompleteBranch.
        :type: str
        """

        self._commit = commit

    @property
    def links(self):
        """
        Gets the links of this CompleteBranch.

        :return: The links of this CompleteBranch.
        :rtype: str
        """
        return self._links

    @links.setter
    def links(self, links):
        """
        Sets the links of this CompleteBranch.

        :param links: The links of this CompleteBranch.
        :type: str
        """

        self._links = links

    @property
    def protected(self):
        """
        Gets the protected of this CompleteBranch.

        :return: The protected of this CompleteBranch.
        :rtype: str
        """
        return self._protected

    @protected.setter
    def protected(self, protected):
        """
        Sets the protected of this CompleteBranch.

        :param protected: The protected of this CompleteBranch.
        :type: str
        """

        self._protected = protected

    @property
    def protection_url(self):
        """
        Gets the protection_url of this CompleteBranch.

        :return: The protection_url of this CompleteBranch.
        :rtype: str
        """
        return self._protection_url

    @protection_url.setter
    def protection_url(self, protection_url):
        """
        Sets the protection_url of this CompleteBranch.

        :param protection_url: The protection_url of this CompleteBranch.
        :type: str
        """

        self._protection_url = protection_url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, CompleteBranch):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class PullRequest(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'url': 'str',
        'html_url': 'str',
        'diff_url': 'str',
        'patch_url': 'str',
        'issue_url': 'str',
        'commits_url': 'str',
        'review_comments_url': 'str',
        'review_comment_url': 'str',
        'comments_url': 'str',
        'statuses_url': 'str',
        'number': 'str',
        'state': 'str',
        'title': 'str',
        'body': 'str',
        'assignee': 'str',
        'milestone': 'str',
        'locked': 'str',
        'created_at': 'str',
        'updated_at': 'str',
        'closed_at': 'str',
        'merged_at': 'str',
        'head': 'str',
        'base': 'str',
        'links': 'str',
        'user': 'str'
    }

    attribute_map = {
        'id': 'id',
        'url': 'url',
        'html_url': 'html_url',
        'diff_url': 'diff_url',
        'patch_url': 'patch_url',
        'issue_url': 'issue_url',
        'commits_url': 'commits_url',
        'review_comments_url': 'review_comments_url',
        'review_comment_url': 'review_comment_url',
        'comments_url': 'comments_url',
        'statuses_url': 'statuses_url',
        'number': 'number',
        'state': 'state',
        'title': 'title',
        'body': 'body',
        'assignee': 'assignee',
        'milestone': 'milestone',
        'locked': 'locked',
        'created_at': 'created_at',
        'updated_at': 'updated_at',
        'closed_at': 'closed_at',
        'merged_at': 'merged_at',
        'head': 'head',
        'base': 'base',
        'links': '_links',
        'user': 'user'
    }

    def __init__(self, id=None, url=None, html_url=None, diff_url=None, patch_url=None, issue_url=None, commits_url=None, review_comments_url=None, review_comment_url=None, comments_url=None, statuses_url=None, number=None, state=None, title=None, body=None, assignee=None, milestone=None, locked=None, created_at=None, updated_at=None, closed_at=None, merged_at=None, head=None, base=None, links=None, user=None):
        """
        PullRequest - a model defined in Swagger
        """

        self._id = None
        self._url = None
        self._html_url = None
        self._diff_url = None
        self._patch_url = None
        self._issue_url = None
        self._commits_url = None
        self._review_comments_url = None
        self._review_comment_url = None
        self._comments_url = None
        self._statuses_url = None
        self._number = None
        self._state = None
        self._title = None
        self._body = None
        self._assignee = None
        self._milestone = None
        self._locked = None
        self._created_at = None
        self._updated_at = None
        self._closed_at = None
        self._merged_at = None
        self._head = None
        self._base = None
        self._links = None
        self._user = None
        self.discriminator = None

        if id is not None:
          self.id = id
        if url is not None:
          self.url = url
        if html_url is not None:
          self.html_url = html_url
        if diff_url is not None:
          self.diff_url = diff_url
        if patch_url is not None:
          self.patch_url = patch_url
        if issue_url is not None:
          self.issue_url = issue_url
        if commits_url is not None:
          self.commits_url = commits_url
        if review_comments_url is not None:
          self.review_comments_url = review_comments_url
        if review_comment_url is not None:
          self.review_comment_url = review_comment_url
        if comments_url is not None:
          self.comments_url = comments_url
        if statuses_url is not None:
          self.statuses_url = statuses_url
        if number is not None:
          self.number = number
        if state is not None:
          self.state = state
        if title is not None:
          self.title = title
        if body is not None:
          self.body = body
        if assignee is not None:
          self.assignee = assignee
        if milestone is not None:
          self.milestone = milestone
        if locked is not None:
          self.locked = locked
        if created_at is not None:
          self.created_at = created_at
        if updated_at is not None:
          self.updated_at = updated_at
        if closed_at is not None:
          self.closed_at = closed_at
        if merged_at is not None:
          self.merged_at = merged_at
        if head is not None:
          self.head = head
        if base is not None:
          self.base = base
        if links is not None:
          self.links = links
        if user is not None:
          self.user = user

    @property
    def id(self):
        """
        Gets the id of this PullRequest.

        :return: The id of this PullRequest.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this PullRequest.

        :param id: The id of this PullRequest.
        :type: str
        """

        self._id = id

    @property
    def url(self):
        """
        Gets the url of this PullRequest.

        :return: The url of this PullRequest.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this PullRequest.

        :param url: The url of this PullRequest.
        :type: str
        """

        self._url = url

    @property
    def html_url(self):
        """
        Gets the html_url of this PullRequest.

        :return: The html_url of this PullRequest.
        :rtype: str
        """
        return self._html_url

    @html_url.setter
    def html_url(self, html_url):
        """
        Sets the html_url of this PullRequest.

        :param html_url: The html_url of this PullRequest.
        :type: str
        """

        self._html_url = html_url

    @property
    def diff_url(self):
        """
        Gets the diff_url of this PullRequest.

        :return: The diff_url of this PullRequest.
        :rtype: str
        """
        return self._diff_url

    @diff_url.setter
    def diff_url(self, diff_url):
        """
        Sets the diff_url of this PullRequest.

        :param diff_url: The diff_url of this PullRequest.
        :type: str
        """

        self._diff_url = diff_url

    @property
    def patch_url(self):
        """
        Gets the patch_url of this PullRequest.

        :return: The patch_url of this PullRequest.
        :rtype: str
        """
        return self._patch_url

    @patch_url.setter
    def patch_url(self, patch_url):
        """
        Sets the patch_url of this PullRequest.

        :param patch_url: The patch_url of this PullRequest.
        :type: str
        """

        self._patch_url = patch_url

    @property
    def issue_url(self):
        """
        Gets the issue_url of this PullRequest.

        :return: The issue_url of this PullRequest.
        :rtype: str
        """
        return self._issue_url

    @issue_url.setter
    def issue_url(self, issue_url):
        """
        Sets the issue_url of this PullRequest.

        :param issue_url: The issue_url of this PullRequest.
        :type: str
        """

        self._issue_url = issue_url

    @property
    def commits_url(self):
        """
        Gets the commits_url of this PullRequest.

        :return: The commits_url of this PullRequest.
        :rtype: str
        """
        return self._commits_url

    @commits_url.setter
    def commits_url(self, commits_url):
        """
        Sets the commits_url of this PullRequest.

        :param commits_url: The commits_url of this PullRequest.
        :type: str
        """

        self._commits_url = commits_url

    @property
    def review_comments_url(self):
        """
        Gets the review_comments_url of this PullRequest.

        :return: The review_comments_url of this PullRequest.
        :rtype: str
        """
        return self._review_comments_url

    @review_comments_url.setter
    def review_comments_url(self, review_comments_url):
        """
        Sets the review_comments_url of this PullRequest.

        :param review_comments_url: The review_comments_url of this PullRequest.
        :type: str
        """

        self._review_comments_url = review_comments_url

    @property
    def review_comment_url(self):
        """
        Gets the review_comment_url of this PullRequest.

        :return: The review_comment_url of this PullRequest.
        :rtype: str
        """
        return self._review_comment_url

    @review_comment_url.setter
    def review_comment_url(self, review_comment_url):
        """
        Sets the review_comment_url of this PullRequest.

        :param review_comment_url: The review_comment_url of this PullRequest.
        :type: str
        """

        self._review_comment_url = review_comment_url

    @property
    def comments_url(self):
        """
        Gets the comments_url of this PullRequest.

        :return: The comments_url of this PullRequest.
        :rtype: str
        """
        return self._comments_url

    @comments_url.setter
    def comments_url(self, comments_url):
        """
        Sets the comments_url of this PullRequest.

        :param comments_url: The comments_url of this PullRequest.
        :type: str
        """

        self._comments_url = comments_url

    @property
    def statuses_url(self):
        """
        Gets the statuses_url of this PullRequest.

        :return: The statuses_url of this PullRequest.
        :rtype: str
        """
        return self._statuses_url

    @statuses_url.setter
    def statuses_url(self, statuses_url):
        """
        Sets the statuses_url of this PullRequest.

        :param statuses_url: The statuses_url of this PullRequest.
        :type: str
        """

        self._statuses_url = statuses_url

    @property
    def number(self):
        """
        Gets the number of this PullRequest.

        :return: The number of this PullRequest.
        :rtype: str
        """
        return self._number

    @number.setter
    def number(self, number):
        """
        Sets the number of this PullRequest.

        :param number: The number of this PullRequest.
        :type: str
        """

        self._number = number

    @property
    def state(self):
        """
        Gets the state of this PullRequest.

        :return: The state of this PullRequest.
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """
        Sets the state of this PullRequest.

        :param state: The state of this PullRequest.
        :type: str
        """

        self._state = state

    @property
    def title(self):
        """
        Gets the title of this PullRequest.

        :return: The title of this PullRequest.
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """
        Sets the title of this PullRequest.

        :param title: The title of this PullRequest.
        :type: str
        """

        self._title = title

    @property
    def body(self):
        """
        Gets the body of this PullRequest.

        :return: The body of this PullRequest.
        :rtype: str
        """
        return self._body

    @body.setter
    def body(self, body):
        """
        Sets the body of this PullRequest.

        :param body: The body of this PullRequest.
        :type: str
        """

        self._body = body

    @property
    def assignee(self):
        """
        Gets the assignee of this PullRequest.

        :return: The assignee of this PullRequest.
        :rtype: str
        """
        return self._assignee

    @assignee.setter
    def assignee(self, assignee):
        """
        Sets the assignee of this PullRequest.

        :param assignee: The assignee of this PullRequest.
        :type: str
        """

        self._assignee = assignee

    @property
    def milestone(self):
        """
        Gets the milestone of this PullRequest.

        :return: The milestone of this PullRequest.
        :rtype: str
        """
        return self._milestone

    @milestone.setter
    def milestone(self, milestone):
        """
        Sets the milestone of this PullRequest.

        :param milestone: The milestone of this PullRequest.
        :type: str
        """

        self._milestone = milestone

    @property
    def locked(self):
        """
        Gets the locked of this PullRequest.

        :return: The locked of this PullRequest.
        :rtype: str
        """
        return self._locked

    @locked.setter
    def locked(self, locked):
        """
        Sets the locked of this PullRequest.

        :param locked: The locked of this PullRequest.
        :type: str
        """

        self._locked = locked

    @property
    def created_at(self):
        """
        Gets the created_at of this PullRequest.

        :return: The created_at of this PullRequest.
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """
        Sets the created_at of this PullRequest.

        :param created_at: The created_at of this PullRequest.
        :type: str
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """
        Gets the updated_at of this PullRequest.

        :return: The updated_at of this PullRequest.
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """
        Sets the updated_at of this PullRequest.

        :param updated_at: The updated_at of this PullRequest.
        :type: str
        """

        self._updated_at = updated_at

    @property
    def closed_at(self):
        """
        Gets the closed_at of this PullRequest.

        :return: The closed_at of this PullRequest.
        :rtype: str
        """
        return self._closed_at

    @closed_at.setter
    def closed_at(self, closed_at):
        """
        Sets the closed_at of this PullRequest.

        :param closed_at: The closed_at of this PullRequest.
        :type: str
        """

        self._closed_at = closed_at

    @property
    def merged_at(self):
        """
        Gets the merged_at of this PullRequest.

        :return: The merged_at of this PullRequest.
        :rtype: str
        """
        return self._merged_at

    @merged_at.setter
    def merged_at(self, merged_at):
        """
        Sets the merged_at of this PullRequest.

        :param merged_at: The merged_at of this PullRequest.
        :type: str
        """

        self._merged_at = merged_at

    @property
    def head(self):
        """
        Gets the head of this PullRequest.

        :return: The head of this PullRequest.
        :rtype: str
        """
        return self._head

    @head.setter
    def head(self, head):
        """
        Sets the head of this PullRequest.

        :param head: The head of this PullRequest.
        :type: str
        """

        self._head = head

    @property
    def base(self):
        """
        Gets the base of this PullRequest.

        :return: The base of this PullRequest.
        :rtype: str
        """
        return self._base

    @base.setter
    def base(self, base):
        """
        Sets the base of this PullRequest.

        :param base: The base of this PullRequest.
        :type: str
        """

        self._base = base

    @property
    def links(self):
        """
        Gets the links of this PullRequest.

        :return: The links of this PullRequest.
        :rtype: str
        """
        return self._links

    @links.setter
    def links(self, links):
        """
        Sets the links of this PullRequest.

        :param links: The links of this PullRequest.
        :type: str
        """

        self._links = links

    @property
    def user(self):
        """
        Gets the user of this PullRequest.

        :return: The user of this PullRequest.
        :rtype: str
        """
        return self._user

    @user.setter
    def user(self, user):
        """
        Sets the user of this PullRequest.

        :param user: The user of this PullRequest.
        :type: str
        """

        self._user = user

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, PullRequest):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class UserMessage(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'sender': 'str',
        'unread': 'str',
        'content': 'str',
        'updated_at': 'str',
        'url': 'str'
    }

    attribute_map = {
        'id': 'id',
        'sender': 'sender',
        'unread': 'unread',
        'content': 'content',
        'updated_at': 'updated_at',
        'url': 'url'
    }

    def __init__(self, id=None, sender=None, unread=None, content=None, updated_at=None, url=None):
        """
        UserMessage - a model defined in Swagger
        """

        self._id = None
        self._sender = None
        self._unread = None
        self._content = None
        self._updated_at = None
        self._url = None
        self.discriminator = None

        if id is not None:
          self.id = id
        if sender is not None:
          self.sender = sender
        if unread is not None:
          self.unread = unread
        if content is not None:
          self.content = content
        if updated_at is not None:
          self.updated_at = updated_at
        if url is not None:
          self.url = url

    @property
    def id(self):
        """
        Gets the id of this UserMessage.

        :return: The id of this UserMessage.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this UserMessage.

        :param id: The id of this UserMessage.
        :type: str
        """

        self._id = id

    @property
    def sender(self):
        """
        Gets the sender of this UserMessage.

        :return: The sender of this UserMessage.
        :rtype: str
        """
        return self._sender

    @sender.setter
    def sender(self, sender):
        """
        Sets the sender of this UserMessage.

        :param sender: The sender of this UserMessage.
        :type: str
        """

        self._sender = sender

    @property
    def unread(self):
        """
        Gets the unread of this UserMessage.

        :return: The unread of this UserMessage.
        :rtype: str
        """
        return self._unread

    @unread.setter
    def unread(self, unread):
        """
        Sets the unread of this UserMessage.

        :param unread: The unread of this UserMessage.
        :type: str
        """

        self._unread = unread

    @property
    def content(self):
        """
        Gets the content of this UserMessage.

        :return: The content of this UserMessage.
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """
        Sets the content of this UserMessage.

        :param content: The content of this UserMessage.
        :type: str
        """

        self._content = content

    @property
    def updated_at(self):
        """
        Gets the updated_at of this UserMessage.

        :return: The updated_at of this UserMessage.
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """
        Sets the updated_at of this UserMessage.

        :param updated_at: The updated_at of this UserMessage.
        :type: str
        """

        self._updated_at = updated_at

    @property
    def url(self):
        """
        Gets the url of this UserMessage.

        :return: The url of this UserMessage.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this UserMessage.

        :param url: The url of this UserMessage.
        :type: str
        """

        self._url = url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, UserMessage):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class CodeForks(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'user': 'str',
        'url': 'str',
        'id': 'str',
        'created_at': 'str',
        'updated_at': 'str'
    }

    attribute_map = {
        'user': 'user',
        'url': 'url',
        'id': 'id',
        'created_at': 'created_at',
        'updated_at': 'updated_at'
    }

    def __init__(self, user=None, url=None, id=None, created_at=None, updated_at=None):
        """
        CodeForks - a model defined in Swagger
        """

        self._user = None
        self._url = None
        self._id = None
        self._created_at = None
        self._updated_at = None
        self.discriminator = None

        if user is not None:
          self.user = user
        if url is not None:
          self.url = url
        if id is not None:
          self.id = id
        if created_at is not None:
          self.created_at = created_at
        if updated_at is not None:
          self.updated_at = updated_at

    @property
    def user(self):
        """
        Gets the user of this CodeForks.

        :return: The user of this CodeForks.
        :rtype: str
        """
        return self._user

    @user.setter
    def user(self, user):
        """
        Sets the user of this CodeForks.

        :param user: The user of this CodeForks.
        :type: str
        """

        self._user = user

    @property
    def url(self):
        """
        Gets the url of this CodeForks.

        :return: The url of this CodeForks.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this CodeForks.

        :param url: The url of this CodeForks.
        :type: str
        """

        self._url = url

    @property
    def id(self):
        """
        Gets the id of this CodeForks.

        :return: The id of this CodeForks.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this CodeForks.

        :param id: The id of this CodeForks.
        :type: str
        """

        self._id = id

    @property
    def created_at(self):
        """
        Gets the created_at of this CodeForks.

        :return: The created_at of this CodeForks.
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """
        Sets the created_at of this CodeForks.

        :param created_at: The created_at of this CodeForks.
        :type: str
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """
        Gets the updated_at of this CodeForks.

        :return: The updated_at of this CodeForks.
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """
        Sets the updated_at of this CodeForks.

        :param updated_at: The updated_at of this CodeForks.
        :type: str
        """

        self._updated_at = updated_at

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, CodeForks):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

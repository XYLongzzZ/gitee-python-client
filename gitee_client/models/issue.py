# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class Issue(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'url': 'str',
        'repository_url': 'str',
        'labels_url': 'str',
        'events_url': 'str',
        'html_url': 'str',
        'number': 'str',
        'state': 'str',
        'title': 'str',
        'body': 'str',
        'user': 'str',
        'labels': 'Label',
        'assignee': 'str',
        'repository': 'str',
        'milestone': 'Milestone',
        'created_at': 'datetime',
        'updated_at': 'datetime',
        'comments': 'int'
    }

    attribute_map = {
        'id': 'id',
        'url': 'url',
        'repository_url': 'repository_url',
        'labels_url': 'labels_url',
        'events_url': 'events_url',
        'html_url': 'html_url',
        'number': 'number',
        'state': 'state',
        'title': 'title',
        'body': 'body',
        'user': 'user',
        'labels': 'labels',
        'assignee': 'assignee',
        'repository': 'repository',
        'milestone': 'milestone',
        'created_at': 'created_at',
        'updated_at': 'updated_at',
        'comments': 'comments'
    }

    def __init__(self, id=None, url=None, repository_url=None, labels_url=None, events_url=None, html_url=None, number=None, state=None, title=None, body=None, user=None, labels=None, assignee=None, repository=None, milestone=None, created_at=None, updated_at=None, comments=None):
        """
        Issue - a model defined in Swagger
        """

        self._id = None
        self._url = None
        self._repository_url = None
        self._labels_url = None
        self._events_url = None
        self._html_url = None
        self._number = None
        self._state = None
        self._title = None
        self._body = None
        self._user = None
        self._labels = None
        self._assignee = None
        self._repository = None
        self._milestone = None
        self._created_at = None
        self._updated_at = None
        self._comments = None
        self.discriminator = None

        if id is not None:
          self.id = id
        if url is not None:
          self.url = url
        if repository_url is not None:
          self.repository_url = repository_url
        if labels_url is not None:
          self.labels_url = labels_url
        if events_url is not None:
          self.events_url = events_url
        if html_url is not None:
          self.html_url = html_url
        if number is not None:
          self.number = number
        if state is not None:
          self.state = state
        if title is not None:
          self.title = title
        if body is not None:
          self.body = body
        if user is not None:
          self.user = user
        if labels is not None:
          self.labels = labels
        if assignee is not None:
          self.assignee = assignee
        if repository is not None:
          self.repository = repository
        if milestone is not None:
          self.milestone = milestone
        if created_at is not None:
          self.created_at = created_at
        if updated_at is not None:
          self.updated_at = updated_at
        if comments is not None:
          self.comments = comments

    @property
    def id(self):
        """
        Gets the id of this Issue.

        :return: The id of this Issue.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this Issue.

        :param id: The id of this Issue.
        :type: int
        """

        self._id = id

    @property
    def url(self):
        """
        Gets the url of this Issue.

        :return: The url of this Issue.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this Issue.

        :param url: The url of this Issue.
        :type: str
        """

        self._url = url

    @property
    def repository_url(self):
        """
        Gets the repository_url of this Issue.

        :return: The repository_url of this Issue.
        :rtype: str
        """
        return self._repository_url

    @repository_url.setter
    def repository_url(self, repository_url):
        """
        Sets the repository_url of this Issue.

        :param repository_url: The repository_url of this Issue.
        :type: str
        """

        self._repository_url = repository_url

    @property
    def labels_url(self):
        """
        Gets the labels_url of this Issue.

        :return: The labels_url of this Issue.
        :rtype: str
        """
        return self._labels_url

    @labels_url.setter
    def labels_url(self, labels_url):
        """
        Sets the labels_url of this Issue.

        :param labels_url: The labels_url of this Issue.
        :type: str
        """

        self._labels_url = labels_url

    @property
    def events_url(self):
        """
        Gets the events_url of this Issue.

        :return: The events_url of this Issue.
        :rtype: str
        """
        return self._events_url

    @events_url.setter
    def events_url(self, events_url):
        """
        Sets the events_url of this Issue.

        :param events_url: The events_url of this Issue.
        :type: str
        """

        self._events_url = events_url

    @property
    def html_url(self):
        """
        Gets the html_url of this Issue.

        :return: The html_url of this Issue.
        :rtype: str
        """
        return self._html_url

    @html_url.setter
    def html_url(self, html_url):
        """
        Sets the html_url of this Issue.

        :param html_url: The html_url of this Issue.
        :type: str
        """

        self._html_url = html_url

    @property
    def number(self):
        """
        Gets the number of this Issue.

        :return: The number of this Issue.
        :rtype: str
        """
        return self._number

    @number.setter
    def number(self, number):
        """
        Sets the number of this Issue.

        :param number: The number of this Issue.
        :type: str
        """

        self._number = number

    @property
    def state(self):
        """
        Gets the state of this Issue.

        :return: The state of this Issue.
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """
        Sets the state of this Issue.

        :param state: The state of this Issue.
        :type: str
        """

        self._state = state

    @property
    def title(self):
        """
        Gets the title of this Issue.

        :return: The title of this Issue.
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """
        Sets the title of this Issue.

        :param title: The title of this Issue.
        :type: str
        """

        self._title = title

    @property
    def body(self):
        """
        Gets the body of this Issue.

        :return: The body of this Issue.
        :rtype: str
        """
        return self._body

    @body.setter
    def body(self, body):
        """
        Sets the body of this Issue.

        :param body: The body of this Issue.
        :type: str
        """

        self._body = body

    @property
    def user(self):
        """
        Gets the user of this Issue.

        :return: The user of this Issue.
        :rtype: str
        """
        return self._user

    @user.setter
    def user(self, user):
        """
        Sets the user of this Issue.

        :param user: The user of this Issue.
        :type: str
        """

        self._user = user

    @property
    def labels(self):
        """
        Gets the labels of this Issue.

        :return: The labels of this Issue.
        :rtype: Label
        """
        return self._labels

    @labels.setter
    def labels(self, labels):
        """
        Sets the labels of this Issue.

        :param labels: The labels of this Issue.
        :type: Label
        """

        self._labels = labels

    @property
    def assignee(self):
        """
        Gets the assignee of this Issue.

        :return: The assignee of this Issue.
        :rtype: str
        """
        return self._assignee

    @assignee.setter
    def assignee(self, assignee):
        """
        Sets the assignee of this Issue.

        :param assignee: The assignee of this Issue.
        :type: str
        """

        self._assignee = assignee

    @property
    def repository(self):
        """
        Gets the repository of this Issue.

        :return: The repository of this Issue.
        :rtype: str
        """
        return self._repository

    @repository.setter
    def repository(self, repository):
        """
        Sets the repository of this Issue.

        :param repository: The repository of this Issue.
        :type: str
        """

        self._repository = repository

    @property
    def milestone(self):
        """
        Gets the milestone of this Issue.

        :return: The milestone of this Issue.
        :rtype: Milestone
        """
        return self._milestone

    @milestone.setter
    def milestone(self, milestone):
        """
        Sets the milestone of this Issue.

        :param milestone: The milestone of this Issue.
        :type: Milestone
        """

        self._milestone = milestone

    @property
    def created_at(self):
        """
        Gets the created_at of this Issue.

        :return: The created_at of this Issue.
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """
        Sets the created_at of this Issue.

        :param created_at: The created_at of this Issue.
        :type: datetime
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """
        Gets the updated_at of this Issue.

        :return: The updated_at of this Issue.
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """
        Sets the updated_at of this Issue.

        :param updated_at: The updated_at of this Issue.
        :type: datetime
        """

        self._updated_at = updated_at

    @property
    def comments(self):
        """
        Gets the comments of this Issue.

        :return: The comments of this Issue.
        :rtype: int
        """
        return self._comments

    @comments.setter
    def comments(self, comments):
        """
        Sets the comments of this Issue.

        :param comments: The comments of this Issue.
        :type: int
        """

        self._comments = comments

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Issue):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

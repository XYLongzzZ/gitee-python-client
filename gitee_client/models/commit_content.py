# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class CommitContent(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'content': 'ContentBasic',
        'commit': 'Commit'
    }

    attribute_map = {
        'content': 'content',
        'commit': 'commit'
    }

    def __init__(self, content=None, commit=None):
        """
        CommitContent - a model defined in Swagger
        """

        self._content = None
        self._commit = None
        self.discriminator = None

        if content is not None:
          self.content = content
        if commit is not None:
          self.commit = commit

    @property
    def content(self):
        """
        Gets the content of this CommitContent.

        :return: The content of this CommitContent.
        :rtype: ContentBasic
        """
        return self._content

    @content.setter
    def content(self, content):
        """
        Sets the content of this CommitContent.

        :param content: The content of this CommitContent.
        :type: ContentBasic
        """

        self._content = content

    @property
    def commit(self):
        """
        Gets the commit of this CommitContent.

        :return: The commit of this CommitContent.
        :rtype: Commit
        """
        return self._commit

    @commit.setter
    def commit(self, commit):
        """
        Sets the commit of this CommitContent.

        :param commit: The commit of this CommitContent.
        :type: Commit
        """

        self._commit = commit

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, CommitContent):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

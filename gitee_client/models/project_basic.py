# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class ProjectBasic(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'full_name': 'str',
        'url': 'str',
        'path': 'str',
        'name': 'str',
        'owner': 'str',
        'description': 'str',
        'private': 'str',
        'fork': 'str',
        'html_url': 'str'
    }

    attribute_map = {
        'id': 'id',
        'full_name': 'full_name',
        'url': 'url',
        'path': 'path',
        'name': 'name',
        'owner': 'owner',
        'description': 'description',
        'private': 'private',
        'fork': 'fork',
        'html_url': 'html_url'
    }

    def __init__(self, id=None, full_name=None, url=None, path=None, name=None, owner=None, description=None, private=None, fork=None, html_url=None):
        """
        ProjectBasic - a model defined in Swagger
        """

        self._id = None
        self._full_name = None
        self._url = None
        self._path = None
        self._name = None
        self._owner = None
        self._description = None
        self._private = None
        self._fork = None
        self._html_url = None
        self.discriminator = None

        if id is not None:
          self.id = id
        if full_name is not None:
          self.full_name = full_name
        if url is not None:
          self.url = url
        if path is not None:
          self.path = path
        if name is not None:
          self.name = name
        if owner is not None:
          self.owner = owner
        if description is not None:
          self.description = description
        if private is not None:
          self.private = private
        if fork is not None:
          self.fork = fork
        if html_url is not None:
          self.html_url = html_url

    @property
    def id(self):
        """
        Gets the id of this ProjectBasic.

        :return: The id of this ProjectBasic.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this ProjectBasic.

        :param id: The id of this ProjectBasic.
        :type: int
        """

        self._id = id

    @property
    def full_name(self):
        """
        Gets the full_name of this ProjectBasic.

        :return: The full_name of this ProjectBasic.
        :rtype: str
        """
        return self._full_name

    @full_name.setter
    def full_name(self, full_name):
        """
        Sets the full_name of this ProjectBasic.

        :param full_name: The full_name of this ProjectBasic.
        :type: str
        """

        self._full_name = full_name

    @property
    def url(self):
        """
        Gets the url of this ProjectBasic.

        :return: The url of this ProjectBasic.
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """
        Sets the url of this ProjectBasic.

        :param url: The url of this ProjectBasic.
        :type: str
        """

        self._url = url

    @property
    def path(self):
        """
        Gets the path of this ProjectBasic.

        :return: The path of this ProjectBasic.
        :rtype: str
        """
        return self._path

    @path.setter
    def path(self, path):
        """
        Sets the path of this ProjectBasic.

        :param path: The path of this ProjectBasic.
        :type: str
        """

        self._path = path

    @property
    def name(self):
        """
        Gets the name of this ProjectBasic.

        :return: The name of this ProjectBasic.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ProjectBasic.

        :param name: The name of this ProjectBasic.
        :type: str
        """

        self._name = name

    @property
    def owner(self):
        """
        Gets the owner of this ProjectBasic.

        :return: The owner of this ProjectBasic.
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """
        Sets the owner of this ProjectBasic.

        :param owner: The owner of this ProjectBasic.
        :type: str
        """

        self._owner = owner

    @property
    def description(self):
        """
        Gets the description of this ProjectBasic.

        :return: The description of this ProjectBasic.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this ProjectBasic.

        :param description: The description of this ProjectBasic.
        :type: str
        """

        self._description = description

    @property
    def private(self):
        """
        Gets the private of this ProjectBasic.

        :return: The private of this ProjectBasic.
        :rtype: str
        """
        return self._private

    @private.setter
    def private(self, private):
        """
        Sets the private of this ProjectBasic.

        :param private: The private of this ProjectBasic.
        :type: str
        """

        self._private = private

    @property
    def fork(self):
        """
        Gets the fork of this ProjectBasic.

        :return: The fork of this ProjectBasic.
        :rtype: str
        """
        return self._fork

    @fork.setter
    def fork(self, fork):
        """
        Sets the fork of this ProjectBasic.

        :param fork: The fork of this ProjectBasic.
        :type: str
        """

        self._fork = fork

    @property
    def html_url(self):
        """
        Gets the html_url of this ProjectBasic.

        :return: The html_url of this ProjectBasic.
        :rtype: str
        """
        return self._html_url

    @html_url.setter
    def html_url(self, html_url):
        """
        Sets the html_url of this ProjectBasic.

        :param html_url: The html_url of this ProjectBasic.
        :type: str
        """

        self._html_url = html_url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ProjectBasic):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

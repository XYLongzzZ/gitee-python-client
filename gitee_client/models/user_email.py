# coding: utf-8

"""
    码云 Open API



    OpenAPI spec version: 5.0.1
    

"""


from pprint import pformat
from six import iteritems
import re


class UserEmail(object):
    """

    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'email': 'str',
        'unconfirmed_email': 'str'
    }

    attribute_map = {
        'email': 'email',
        'unconfirmed_email': 'unconfirmed_email'
    }

    def __init__(self, email=None, unconfirmed_email=None):
        """
        UserEmail - a model defined in Swagger
        """

        self._email = None
        self._unconfirmed_email = None
        self.discriminator = None

        if email is not None:
          self.email = email
        if unconfirmed_email is not None:
          self.unconfirmed_email = unconfirmed_email

    @property
    def email(self):
        """
        Gets the email of this UserEmail.

        :return: The email of this UserEmail.
        :rtype: str
        """
        return self._email

    @email.setter
    def email(self, email):
        """
        Sets the email of this UserEmail.

        :param email: The email of this UserEmail.
        :type: str
        """

        self._email = email

    @property
    def unconfirmed_email(self):
        """
        Gets the unconfirmed_email of this UserEmail.

        :return: The unconfirmed_email of this UserEmail.
        :rtype: str
        """
        return self._unconfirmed_email

    @unconfirmed_email.setter
    def unconfirmed_email(self, unconfirmed_email):
        """
        Sets the unconfirmed_email of this UserEmail.

        :param unconfirmed_email: The unconfirmed_email of this UserEmail.
        :type: str
        """

        self._unconfirmed_email = unconfirmed_email

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, UserEmail):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

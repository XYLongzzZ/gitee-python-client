# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**url** | **str** |  | [optional] 
**repository_url** | **str** |  | [optional] 
**labels_url** | **str** |  | [optional] 
**events_url** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**number** | **str** |  | [optional] 
**state** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**body** | **str** |  | [optional] 
**user** | **str** |  | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**assignee** | **str** |  | [optional] 
**repository** | **str** |  | [optional] 
**milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**comments** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



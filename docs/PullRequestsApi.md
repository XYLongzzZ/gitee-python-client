# gitee_client.PullRequestsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_v5_repos_owner_repo_pulls_comments_id**](PullRequestsApi.md#delete_v5_repos_owner_repo_pulls_comments_id) | **DELETE** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**delete_v5_repos_owner_repo_pulls_number_requested_reviewers**](PullRequestsApi.md#delete_v5_repos_owner_repo_pulls_number_requested_reviewers) | **DELETE** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 移除审查人员
[**get_v5_repos_owner_repo_pulls**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls) | **GET** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**get_v5_repos_owner_repo_pulls_comments**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_comments) | **GET** /v5/repos/{owner}/{repo}/pulls/comments | 获取该项目下的所有Pull Request评论
[**get_v5_repos_owner_repo_pulls_comments_id**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_comments_id) | **GET** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**get_v5_repos_owner_repo_pulls_number**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number) | **GET** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**get_v5_repos_owner_repo_pulls_number_comments**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number_comments) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**get_v5_repos_owner_repo_pulls_number_commits**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number_commits) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**get_v5_repos_owner_repo_pulls_number_files**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number_files) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**get_v5_repos_owner_repo_pulls_number_merge**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number_merge) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**get_v5_repos_owner_repo_pulls_number_requested_reviewers**](PullRequestsApi.md#get_v5_repos_owner_repo_pulls_number_requested_reviewers) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 获取审查人员的列表
[**patch_v5_repos_owner_repo_pulls_comments_id**](PullRequestsApi.md#patch_v5_repos_owner_repo_pulls_comments_id) | **PATCH** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**patch_v5_repos_owner_repo_pulls_number**](PullRequestsApi.md#patch_v5_repos_owner_repo_pulls_number) | **PATCH** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**post_v5_repos_owner_repo_pulls**](PullRequestsApi.md#post_v5_repos_owner_repo_pulls) | **POST** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
[**post_v5_repos_owner_repo_pulls_number_comments**](PullRequestsApi.md#post_v5_repos_owner_repo_pulls_number_comments) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**post_v5_repos_owner_repo_pulls_number_requested_reviewers**](PullRequestsApi.md#post_v5_repos_owner_repo_pulls_number_requested_reviewers) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 增加审查人员
[**put_v5_repos_owner_repo_pulls_number_merge**](PullRequestsApi.md#put_v5_repos_owner_repo_pulls_number_merge) | **PUT** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request


# **delete_v5_repos_owner_repo_pulls_comments_id**
> delete_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, access_token=access_token)

删除评论

删除评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除评论
    api_instance.delete_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling PullRequestsApi->delete_v5_repos_owner_repo_pulls_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_pulls_number_requested_reviewers**
> delete_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, access_token=access_token)

移除审查人员

移除审查人员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
reviewers = ['reviewers_example'] # list[str] | 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对username换行即可
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 移除审查人员
    api_instance.delete_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, access_token=access_token)
except ApiException as e:
    print("Exception when calling PullRequestsApi->delete_v5_repos_owner_repo_pulls_number_requested_reviewers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **reviewers** | [**list[str]**](str.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对username换行即可 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls**
> list[PullRequest] get_v5_repos_owner_repo_pulls(owner, repo, access_token=access_token, state=state, head=head, base=base, sort=sort, direction=direction, page=page, per_page=per_page)

获取Pull Request列表

获取Pull Request列表

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'open' # str | 可选。Pull Request 状态 (optional) (default to open)
head = 'head_example' # str | 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch (optional)
base = 'base_example' # str | 可选。Pull Request 提交目标分支的名称。 (optional)
sort = 'created' # str | 可选。排序字段，默认按创建时间 (optional) (default to created)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取Pull Request列表
    api_response = api_instance.get_v5_repos_owner_repo_pulls(owner, repo, access_token=access_token, state=state, head=head, base=base, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| 可选。Pull Request 状态 | [optional] [default to open]
 **head** | **str**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | [optional] 
 **base** | **str**| 可选。Pull Request 提交目标分支的名称。 | [optional] 
 **sort** | **str**| 可选。排序字段，默认按创建时间 | [optional] [default to created]
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[PullRequest]**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_comments**
> list[PullRequestComments] get_v5_repos_owner_repo_pulls_comments(owner, repo, access_token=access_token, sort=sort, direction=direction, since=since, page=page, per_page=per_page)

获取该项目下的所有Pull Request评论

获取该项目下的所有Pull Request评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'created' # str | 可选。按创建时间/更新时间排序 (optional) (default to created)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
since = 'since_example' # str | 起始的更新时间，要求时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取该项目下的所有Pull Request评论
    api_response = api_instance.get_v5_repos_owner_repo_pulls_comments(owner, repo, access_token=access_token, sort=sort, direction=direction, since=since, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 可选。按创建时间/更新时间排序 | [optional] [default to created]
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **since** | **str**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[PullRequestComments]**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_comments_id**
> PullRequestComments get_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, access_token=access_token)

获取Pull Request的某个评论

获取Pull Request的某个评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取Pull Request的某个评论
    api_response = api_instance.get_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number**
> PullRequest get_v5_repos_owner_repo_pulls_number(owner, repo, number, access_token=access_token)

获取单个Pull Request

获取单个Pull Request

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取单个Pull Request
    api_response = api_instance.get_v5_repos_owner_repo_pulls_number(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number_comments**
> list[PullRequestComments] get_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, access_token=access_token, page=page, per_page=per_page)

获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取某个Pull Request的所有评论
    api_response = api_instance.get_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[PullRequestComments]**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number_commits**
> list[PullRequestCommits] get_v5_repos_owner_repo_pulls_number_commits(owner, repo, number, access_token=access_token)

获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取某Pull Request的所有Commit信息。最多显示250条Commit
    api_response = api_instance.get_v5_repos_owner_repo_pulls_number_commits(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number_commits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[PullRequestCommits]**](PullRequestCommits.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number_files**
> list[PullRequestFiles] get_v5_repos_owner_repo_pulls_number_files(owner, repo, number, access_token=access_token)

Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # Pull Request Commit文件列表。最多显示300条diff
    api_response = api_instance.get_v5_repos_owner_repo_pulls_number_files(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[PullRequestFiles]**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number_merge**
> get_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, access_token=access_token)

判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 判断Pull Request是否已经合并
    api_instance.get_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, access_token=access_token)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number_merge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pulls_number_requested_reviewers**
> UserBasic get_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, access_token=access_token, page=page, per_page=per_page)

获取审查人员的列表

获取审查人员的列表

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取审查人员的列表
    api_response = api_instance.get_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_v5_repos_owner_repo_pulls_number_requested_reviewers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_pulls_comments_id**
> PullRequestComments patch_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, body, access_token=access_token)

编辑评论

编辑评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
body = 'body_example' # str | 必填。评论内容
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 编辑评论
    api_response = api_instance.patch_v5_repos_owner_repo_pulls_comments_id(owner, repo, id, body, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->patch_v5_repos_owner_repo_pulls_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **body** | **str**| 必填。评论内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_pulls_number**
> PullRequest patch_v5_repos_owner_repo_pulls_number(owner, repo, number, access_token=access_token, title=title, body=body, state=state)

更新Pull Request信息

更新Pull Request信息

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)
title = 'title_example' # str | 可选。Pull Request 标题 (optional)
body = 'body_example' # str | 可选。Pull Request 内容 (optional)
state = 'state_example' # str | 可选。Pull Request 状态 (optional)

try: 
    # 更新Pull Request信息
    api_response = api_instance.patch_v5_repos_owner_repo_pulls_number(owner, repo, number, access_token=access_token, title=title, body=body, state=state)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->patch_v5_repos_owner_repo_pulls_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **title** | **str**| 可选。Pull Request 标题 | [optional] 
 **body** | **str**| 可选。Pull Request 内容 | [optional] 
 **state** | **str**| 可选。Pull Request 状态 | [optional] 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_pulls**
> PullRequest post_v5_repos_owner_repo_pulls(owner, repo, title, head, base, access_token=access_token, body=body, issue=issue)

创建Pull Request

创建Pull Request

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
title = 'title_example' # str | 必填。Pull Request 标题
head = 'head_example' # str | 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch
base = 'base_example' # str | 必填。Pull Request 提交目标分支的名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
body = 'body_example' # str | 可选。Pull Request 内容 (optional)
issue = 'issue_example' # str | 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 (optional)

try: 
    # 创建Pull Request
    api_response = api_instance.post_v5_repos_owner_repo_pulls(owner, repo, title, head, base, access_token=access_token, body=body, issue=issue)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_v5_repos_owner_repo_pulls: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **title** | **str**| 必填。Pull Request 标题 | 
 **head** | **str**| 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
 **base** | **str**| 必填。Pull Request 提交目标分支的名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **body** | **str**| 可选。Pull Request 内容 | [optional] 
 **issue** | **str**| 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 | [optional] 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_pulls_number_comments**
> PullRequestComments post_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, body, access_token=access_token, commit_id=commit_id, path=path, position=position)

提交Pull Request评论

提交Pull Request评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
body = 'body_example' # str | 必填。评论内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
commit_id = 'commit_id_example' # str | 可选。PR代码评论的commit id (optional)
path = 'path_example' # str | 可选。PR代码评论的文件名 (optional)
position = 56 # int | 可选。PR代码评论diff中的行数 (optional)

try: 
    # 提交Pull Request评论
    api_response = api_instance.post_v5_repos_owner_repo_pulls_number_comments(owner, repo, number, body, access_token=access_token, commit_id=commit_id, path=path, position=position)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_v5_repos_owner_repo_pulls_number_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **body** | **str**| 必填。评论内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **commit_id** | **str**| 可选。PR代码评论的commit id | [optional] 
 **path** | **str**| 可选。PR代码评论的文件名 | [optional] 
 **position** | **int**| 可选。PR代码评论diff中的行数 | [optional] 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_pulls_number_requested_reviewers**
> PullRequest post_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, access_token=access_token)

增加审查人员

增加审查人员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
reviewers = ['reviewers_example'] # list[str] | 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对标签名换行即可
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 增加审查人员
    api_response = api_instance.post_v5_repos_owner_repo_pulls_number_requested_reviewers(owner, repo, number, reviewers, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_v5_repos_owner_repo_pulls_number_requested_reviewers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **reviewers** | [**list[str]**](str.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对标签名换行即可 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_v5_repos_owner_repo_pulls_number_merge**
> put_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, access_token=access_token)

合并Pull Request

合并Pull Request

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.PullRequestsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个PR，即本项目PR的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 合并Pull Request
    api_instance.put_v5_repos_owner_repo_pulls_number_merge(owner, repo, number, access_token=access_token)
except ApiException as e:
    print("Exception when calling PullRequestsApi->put_v5_repos_owner_repo_pulls_number_merge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个PR，即本项目PR的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


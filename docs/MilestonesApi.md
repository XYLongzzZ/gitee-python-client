# gitee_client.MilestonesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_v5_repos_owner_repo_milestones_number**](MilestonesApi.md#delete_v5_repos_owner_repo_milestones_number) | **DELETE** /v5/repos/{owner}/{repo}/milestones/{number} | 删除项目单个里程碑
[**get_v5_repos_owner_repo_milestones**](MilestonesApi.md#get_v5_repos_owner_repo_milestones) | **GET** /v5/repos/{owner}/{repo}/milestones | 获取项目所有里程碑
[**get_v5_repos_owner_repo_milestones_number**](MilestonesApi.md#get_v5_repos_owner_repo_milestones_number) | **GET** /v5/repos/{owner}/{repo}/milestones/{number} | 获取项目单个里程碑
[**patch_v5_repos_owner_repo_milestones_number**](MilestonesApi.md#patch_v5_repos_owner_repo_milestones_number) | **PATCH** /v5/repos/{owner}/{repo}/milestones/{number} | 更新项目里程碑
[**post_v5_repos_owner_repo_milestones**](MilestonesApi.md#post_v5_repos_owner_repo_milestones) | **POST** /v5/repos/{owner}/{repo}/milestones | 创建项目里程碑


# **delete_v5_repos_owner_repo_milestones_number**
> delete_v5_repos_owner_repo_milestones_number(owner, repo, number, access_token=access_token)

删除项目单个里程碑

删除项目单个里程碑

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.MilestonesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个里程碑，即本项目里程碑的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除项目单个里程碑
    api_instance.delete_v5_repos_owner_repo_milestones_number(owner, repo, number, access_token=access_token)
except ApiException as e:
    print("Exception when calling MilestonesApi->delete_v5_repos_owner_repo_milestones_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个里程碑，即本项目里程碑的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_milestones**
> list[Milestone] get_v5_repos_owner_repo_milestones(owner, repo, access_token=access_token, state=state, sort=sort, direction=direction, page=page, per_page=per_page)

获取项目所有里程碑

获取项目所有里程碑

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.MilestonesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'open' # str | 里程碑状态: open, closed, all。默认: open (optional) (default to open)
sort = 'due_on' # str | 排序方式: due_on (optional) (default to due_on)
direction = 'direction_example' # str | 升序(asc)或是降序(desc)。默认: asc (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取项目所有里程碑
    api_response = api_instance.get_v5_repos_owner_repo_milestones(owner, repo, access_token=access_token, state=state, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->get_v5_repos_owner_repo_milestones: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open]
 **sort** | **str**| 排序方式: due_on | [optional] [default to due_on]
 **direction** | **str**| 升序(asc)或是降序(desc)。默认: asc | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[Milestone]**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_milestones_number**
> Milestone get_v5_repos_owner_repo_milestones_number(owner, repo, number, access_token=access_token)

获取项目单个里程碑

获取项目单个里程碑

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.MilestonesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个里程碑，即本项目里程碑的序数
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目单个里程碑
    api_response = api_instance.get_v5_repos_owner_repo_milestones_number(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->get_v5_repos_owner_repo_milestones_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个里程碑，即本项目里程碑的序数 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_milestones_number**
> Milestone patch_v5_repos_owner_repo_milestones_number(owner, repo, number, title, access_token=access_token, state=state, description=description, due_on=due_on)

更新项目里程碑

更新项目里程碑

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.MilestonesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 56 # int | 第几个里程碑，即本项目里程碑的序数
title = 'title_example' # str | 里程碑标题
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'open' # str | 里程碑状态: open, closed, all。默认: open (optional) (default to open)
description = 'description_example' # str | 里程碑具体描述 (optional)
due_on = 'due_on_example' # str | 里程碑的截止日期 (optional)

try: 
    # 更新项目里程碑
    api_response = api_instance.patch_v5_repos_owner_repo_milestones_number(owner, repo, number, title, access_token=access_token, state=state, description=description, due_on=due_on)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->patch_v5_repos_owner_repo_milestones_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **int**| 第几个里程碑，即本项目里程碑的序数 | 
 **title** | **str**| 里程碑标题 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open]
 **description** | **str**| 里程碑具体描述 | [optional] 
 **due_on** | **str**| 里程碑的截止日期 | [optional] 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_milestones**
> Milestone post_v5_repos_owner_repo_milestones(owner, repo, title, access_token=access_token, state=state, description=description, due_on=due_on)

创建项目里程碑

创建项目里程碑

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.MilestonesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
title = 'title_example' # str | 里程碑标题
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'open' # str | 里程碑状态: open, closed, all。默认: open (optional) (default to open)
description = 'description_example' # str | 里程碑具体描述 (optional)
due_on = 'due_on_example' # str | 里程碑的截止日期 (optional)

try: 
    # 创建项目里程碑
    api_response = api_instance.post_v5_repos_owner_repo_milestones(owner, repo, title, access_token=access_token, state=state, description=description, due_on=due_on)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->post_v5_repos_owner_repo_milestones: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **title** | **str**| 里程碑标题 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open]
 **description** | **str**| 里程碑具体描述 | [optional] 
 **due_on** | **str**| 里程碑的截止日期 | [optional] 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# gitee_client.LabelsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_v5_repos_owner_repo_issues_number_labels**](LabelsApi.md#delete_v5_repos_owner_repo_issues_number_labels) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
[**delete_v5_repos_owner_repo_issues_number_labels_name**](LabelsApi.md#delete_v5_repos_owner_repo_issues_number_labels_name) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
[**delete_v5_repos_owner_repo_labels_name**](LabelsApi.md#delete_v5_repos_owner_repo_labels_name) | **DELETE** /v5/repos/{owner}/{repo}/labels/{name} | 删除一个项目标签
[**get_v5_repos_owner_repo_issues_number_labels**](LabelsApi.md#get_v5_repos_owner_repo_issues_number_labels) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/labels | 获取项目Issue的所有标签
[**get_v5_repos_owner_repo_labels**](LabelsApi.md#get_v5_repos_owner_repo_labels) | **GET** /v5/repos/{owner}/{repo}/labels | 获取项目所有标签
[**get_v5_repos_owner_repo_labels_name**](LabelsApi.md#get_v5_repos_owner_repo_labels_name) | **GET** /v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
[**patch_v5_repos_owner_repo_labels_original_name**](LabelsApi.md#patch_v5_repos_owner_repo_labels_original_name) | **PATCH** /v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个项目标签
[**post_v5_repos_owner_repo_issues_number_labels**](LabelsApi.md#post_v5_repos_owner_repo_issues_number_labels) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
[**post_v5_repos_owner_repo_labels**](LabelsApi.md#post_v5_repos_owner_repo_labels) | **POST** /v5/repos/{owner}/{repo}/labels | 创建项目标签
[**put_v5_repos_owner_repo_issues_number_labels**](LabelsApi.md#put_v5_repos_owner_repo_issues_number_labels) | **PUT** /v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签


# **delete_v5_repos_owner_repo_issues_number_labels**
> delete_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)

删除Issue所有标签

删除Issue所有标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除Issue所有标签
    api_instance.delete_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)
except ApiException as e:
    print("Exception when calling LabelsApi->delete_v5_repos_owner_repo_issues_number_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_issues_number_labels_name**
> delete_v5_repos_owner_repo_issues_number_labels_name(owner, repo, number, name, access_token=access_token)

删除Issue标签

删除Issue标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
name = 'name_example' # str | 标签名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除Issue标签
    api_instance.delete_v5_repos_owner_repo_issues_number_labels_name(owner, repo, number, name, access_token=access_token)
except ApiException as e:
    print("Exception when calling LabelsApi->delete_v5_repos_owner_repo_issues_number_labels_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **name** | **str**| 标签名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_labels_name**
> delete_v5_repos_owner_repo_labels_name(owner, repo, name, access_token=access_token)

删除一个项目标签

删除一个项目标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
name = 'name_example' # str | 标签名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除一个项目标签
    api_instance.delete_v5_repos_owner_repo_labels_name(owner, repo, name, access_token=access_token)
except ApiException as e:
    print("Exception when calling LabelsApi->delete_v5_repos_owner_repo_labels_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **name** | **str**| 标签名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues_number_labels**
> list[Label] get_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)

获取项目Issue的所有标签

获取项目Issue的所有标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目Issue的所有标签
    api_response = api_instance.get_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->get_v5_repos_owner_repo_issues_number_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[Label]**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_labels**
> list[Label] get_v5_repos_owner_repo_labels(owner, repo, access_token=access_token)

获取项目所有标签

获取项目所有标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目所有标签
    api_response = api_instance.get_v5_repos_owner_repo_labels(owner, repo, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->get_v5_repos_owner_repo_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[Label]**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_labels_name**
> Label get_v5_repos_owner_repo_labels_name(owner, repo, name, access_token=access_token)

根据标签名称获取单个标签

根据标签名称获取单个标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
name = 'name_example' # str | 标签名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 根据标签名称获取单个标签
    api_response = api_instance.get_v5_repos_owner_repo_labels_name(owner, repo, name, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->get_v5_repos_owner_repo_labels_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **name** | **str**| 标签名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_labels_original_name**
> Label patch_v5_repos_owner_repo_labels_original_name(owner, repo, original_name, access_token=access_token, name=name, color=color)

更新一个项目标签

更新一个项目标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
original_name = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
name = 'name_example' # str | The name of a label (optional)
color = 'color_example' # str | The color of a label (optional)

try: 
    # 更新一个项目标签
    api_response = api_instance.patch_v5_repos_owner_repo_labels_original_name(owner, repo, original_name, access_token=access_token, name=name, color=color)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->patch_v5_repos_owner_repo_labels_original_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **original_name** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **name** | **str**| The name of a label | [optional] 
 **color** | **str**| The color of a label | [optional] 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_issues_number_labels**
> Label post_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)

创建Issue标签

创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 创建Issue标签
    api_response = api_instance.post_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->post_v5_repos_owner_repo_issues_number_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_labels**
> Label post_v5_repos_owner_repo_labels(owner, repo, name, color, access_token=access_token)

创建项目标签

创建项目标签

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
name = 'name_example' # str | 标签名称
color = 'color_example' # str | 标签颜色。为6位的数字，如: 000000
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 创建项目标签
    api_response = api_instance.post_v5_repos_owner_repo_labels(owner, repo, name, color, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->post_v5_repos_owner_repo_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **name** | **str**| 标签名称 | 
 **color** | **str**| 标签颜色。为6位的数字，如: 000000 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_v5_repos_owner_repo_issues_number_labels**
> Label put_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)

替换Issue所有标签

替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.LabelsApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 替换Issue所有标签
    api_response = api_instance.put_v5_repos_owner_repo_issues_number_labels(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->put_v5_repos_owner_repo_issues_number_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


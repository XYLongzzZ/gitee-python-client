# UserNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**repository** | [**ProjectBasic**](ProjectBasic.md) |  | [optional] 
**unread** | **str** |  | [optional] 
**mute** | **str** |  | [optional] 
**subject** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



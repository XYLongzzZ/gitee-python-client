# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**diff_url** | **str** |  | [optional] 
**patch_url** | **str** |  | [optional] 
**issue_url** | **str** |  | [optional] 
**commits_url** | **str** |  | [optional] 
**review_comments_url** | **str** |  | [optional] 
**review_comment_url** | **str** |  | [optional] 
**comments_url** | **str** |  | [optional] 
**statuses_url** | **str** |  | [optional] 
**number** | **str** |  | [optional] 
**state** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**body** | **str** |  | [optional] 
**assignee** | **str** |  | [optional] 
**milestone** | **str** |  | [optional] 
**locked** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**closed_at** | **str** |  | [optional] 
**merged_at** | **str** |  | [optional] 
**head** | **str** |  | [optional] 
**base** | **str** |  | [optional] 
**links** | **str** |  | [optional] 
**user** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



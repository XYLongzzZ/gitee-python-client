# UserDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**avatar_url** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**followers_url** | **str** |  | [optional] 
**following_url** | **str** |  | [optional] 
**gists_url** | **str** |  | [optional] 
**starred_url** | **str** |  | [optional] 
**subscriptions_url** | **str** |  | [optional] 
**organizations_url** | **str** |  | [optional] 
**repos_url** | **str** |  | [optional] 
**events_url** | **str** |  | [optional] 
**received_events_url** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**site_admin** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**blog** | **str** |  | [optional] 
**weibo** | **str** |  | [optional] 
**bio** | **str** |  | [optional] 
**public_repos** | **str** |  | [optional] 
**public_gists** | **str** |  | [optional] 
**followers** | **str** |  | [optional] 
**following** | **str** |  | [optional] 
**stared** | **str** |  | [optional] 
**watched** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**unconfirmed_email** | **str** |  | [optional] 
**phone** | **str** |  | [optional] 
**private_token** | **str** |  | [optional] 
**total_repos** | **str** |  | [optional] 
**owned_repos** | **str** |  | [optional] 
**total_private_repos** | **str** |  | [optional] 
**owned_private_repos** | **str** |  | [optional] 
**private_gists** | **str** |  | [optional] 
**address** | [**UserAddress**](UserAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



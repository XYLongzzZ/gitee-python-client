# gitee_client.RepositoriesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_v5_repos_owner_repo**](RepositoriesApi.md#delete_v5_repos_owner_repo) | **DELETE** /v5/repos/{owner}/{repo} | 删除一个项目
[**delete_v5_repos_owner_repo_branches_branch_protection**](RepositoriesApi.md#delete_v5_repos_owner_repo_branches_branch_protection) | **DELETE** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
[**delete_v5_repos_owner_repo_collaborators_username**](RepositoriesApi.md#delete_v5_repos_owner_repo_collaborators_username) | **DELETE** /v5/repos/{owner}/{repo}/collaborators/{username} | 移除项目成员
[**delete_v5_repos_owner_repo_comments_id**](RepositoriesApi.md#delete_v5_repos_owner_repo_comments_id) | **DELETE** /v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
[**delete_v5_repos_owner_repo_contents_path**](RepositoriesApi.md#delete_v5_repos_owner_repo_contents_path) | **DELETE** /v5/repos/{owner}/{repo}/contents/{path} | 删除文件
[**delete_v5_repos_owner_repo_keys_id**](RepositoriesApi.md#delete_v5_repos_owner_repo_keys_id) | **DELETE** /v5/repos/{owner}/{repo}/keys/{id} | 删除一个项目公钥
[**delete_v5_repos_owner_repo_releases_id**](RepositoriesApi.md#delete_v5_repos_owner_repo_releases_id) | **DELETE** /v5/repos/{owner}/{repo}/releases/{id} | 删除项目Release
[**get_v5_orgs_org_repos**](RepositoriesApi.md#get_v5_orgs_org_repos) | **GET** /v5/orgs/{org}/repos | 获取一个组织的项目
[**get_v5_repos_owner_repo**](RepositoriesApi.md#get_v5_repos_owner_repo) | **GET** /v5/repos/{owner}/{repo} | 列出授权用户的某个项目
[**get_v5_repos_owner_repo_branches**](RepositoriesApi.md#get_v5_repos_owner_repo_branches) | **GET** /v5/repos/{owner}/{repo}/branches | 获取所有分支
[**get_v5_repos_owner_repo_branches_branch**](RepositoriesApi.md#get_v5_repos_owner_repo_branches_branch) | **GET** /v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
[**get_v5_repos_owner_repo_collaborators**](RepositoriesApi.md#get_v5_repos_owner_repo_collaborators) | **GET** /v5/repos/{owner}/{repo}/collaborators | 获取项目的所有成员
[**get_v5_repos_owner_repo_collaborators_username**](RepositoriesApi.md#get_v5_repos_owner_repo_collaborators_username) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为项目成员
[**get_v5_repos_owner_repo_collaborators_username_permission**](RepositoriesApi.md#get_v5_repos_owner_repo_collaborators_username_permission) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看项目成员的权限
[**get_v5_repos_owner_repo_comments**](RepositoriesApi.md#get_v5_repos_owner_repo_comments) | **GET** /v5/repos/{owner}/{repo}/comments | 获取项目的Commit评论
[**get_v5_repos_owner_repo_comments_id**](RepositoriesApi.md#get_v5_repos_owner_repo_comments_id) | **GET** /v5/repos/{owner}/{repo}/comments/{id} | 获取项目的某条Commit评论
[**get_v5_repos_owner_repo_commits**](RepositoriesApi.md#get_v5_repos_owner_repo_commits) | **GET** /v5/repos/{owner}/{repo}/commits | 项目的所有提交
[**get_v5_repos_owner_repo_commits_ref_comments**](RepositoriesApi.md#get_v5_repos_owner_repo_commits_ref_comments) | **GET** /v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
[**get_v5_repos_owner_repo_commits_sha**](RepositoriesApi.md#get_v5_repos_owner_repo_commits_sha) | **GET** /v5/repos/{owner}/{repo}/commits/{sha} | 项目的某个提交
[**get_v5_repos_owner_repo_compare_base___head**](RepositoriesApi.md#get_v5_repos_owner_repo_compare_base___head) | **GET** /v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
[**get_v5_repos_owner_repo_contents_path**](RepositoriesApi.md#get_v5_repos_owner_repo_contents_path) | **GET** /v5/repos/{owner}/{repo}/contents(/{path}) | 获取仓库具体路径下的内容
[**get_v5_repos_owner_repo_contributors**](RepositoriesApi.md#get_v5_repos_owner_repo_contributors) | **GET** /v5/repos/{owner}/{repo}/contributors | 获取项目贡献者
[**get_v5_repos_owner_repo_forks**](RepositoriesApi.md#get_v5_repos_owner_repo_forks) | **GET** /v5/repos/{owner}/{repo}/forks | 查看项目的Forks
[**get_v5_repos_owner_repo_keys**](RepositoriesApi.md#get_v5_repos_owner_repo_keys) | **GET** /v5/repos/{owner}/{repo}/keys | 展示项目的公钥
[**get_v5_repos_owner_repo_keys_id**](RepositoriesApi.md#get_v5_repos_owner_repo_keys_id) | **GET** /v5/repos/{owner}/{repo}/keys/{id} | 获取项目的单个公钥
[**get_v5_repos_owner_repo_pages**](RepositoriesApi.md#get_v5_repos_owner_repo_pages) | **GET** /v5/repos/{owner}/{repo}/pages | 获取Pages信息
[**get_v5_repos_owner_repo_readme**](RepositoriesApi.md#get_v5_repos_owner_repo_readme) | **GET** /v5/repos/{owner}/{repo}/readme | 获取仓库README
[**get_v5_repos_owner_repo_releases**](RepositoriesApi.md#get_v5_repos_owner_repo_releases) | **GET** /v5/repos/{owner}/{repo}/releases | 获取项目的所有Releases
[**get_v5_repos_owner_repo_releases_id**](RepositoriesApi.md#get_v5_repos_owner_repo_releases_id) | **GET** /v5/repos/{owner}/{repo}/releases/{id} | 获取项目的单个Releases
[**get_v5_repos_owner_repo_releases_latest**](RepositoriesApi.md#get_v5_repos_owner_repo_releases_latest) | **GET** /v5/repos/{owner}/{repo}/releases/latest | 获取项目的最后更新的Release
[**get_v5_repos_owner_repo_releases_tags_tag**](RepositoriesApi.md#get_v5_repos_owner_repo_releases_tags_tag) | **GET** /v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取项目的Release
[**get_v5_repos_owner_repo_tags**](RepositoriesApi.md#get_v5_repos_owner_repo_tags) | **GET** /v5/repos/{owner}/{repo}/tags | 列出项目所有的tags
[**get_v5_user_repos**](RepositoriesApi.md#get_v5_user_repos) | **GET** /v5/user/repos | 列出授权用户的所有项目
[**get_v5_users_username_repos**](RepositoriesApi.md#get_v5_users_username_repos) | **GET** /v5/users/{username}/repos | 获取某个用户的公开项目
[**patch_v5_repos_owner_repo**](RepositoriesApi.md#patch_v5_repos_owner_repo) | **PATCH** /v5/repos/{owner}/{repo} | 更新项目设置
[**patch_v5_repos_owner_repo_comments_id**](RepositoriesApi.md#patch_v5_repos_owner_repo_comments_id) | **PATCH** /v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
[**patch_v5_repos_owner_repo_releases_id**](RepositoriesApi.md#patch_v5_repos_owner_repo_releases_id) | **PATCH** /v5/repos/{owner}/{repo}/releases/{id} | 更新项目Release
[**post_v5_orgs_org_repos**](RepositoriesApi.md#post_v5_orgs_org_repos) | **POST** /v5/orgs/{org}/repos | 创建组织项目
[**post_v5_repos_owner_repo_commits_sha_comments**](RepositoriesApi.md#post_v5_repos_owner_repo_commits_sha_comments) | **POST** /v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
[**post_v5_repos_owner_repo_contents_path**](RepositoriesApi.md#post_v5_repos_owner_repo_contents_path) | **POST** /v5/repos/{owner}/{repo}/contents/{path} | 新建文件
[**post_v5_repos_owner_repo_forks**](RepositoriesApi.md#post_v5_repos_owner_repo_forks) | **POST** /v5/repos/{owner}/{repo}/forks | Fork一个项目
[**post_v5_repos_owner_repo_keys**](RepositoriesApi.md#post_v5_repos_owner_repo_keys) | **POST** /v5/repos/{owner}/{repo}/keys | 为项目添加公钥
[**post_v5_repos_owner_repo_pages_builds**](RepositoriesApi.md#post_v5_repos_owner_repo_pages_builds) | **POST** /v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
[**post_v5_repos_owner_repo_releases**](RepositoriesApi.md#post_v5_repos_owner_repo_releases) | **POST** /v5/repos/{owner}/{repo}/releases | 创建项目Release
[**post_v5_user_repos**](RepositoriesApi.md#post_v5_user_repos) | **POST** /v5/user/repos | 创建一个项目
[**put_v5_repos_owner_repo_branches_branch_protection**](RepositoriesApi.md#put_v5_repos_owner_repo_branches_branch_protection) | **PUT** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
[**put_v5_repos_owner_repo_collaborators_username**](RepositoriesApi.md#put_v5_repos_owner_repo_collaborators_username) | **PUT** /v5/repos/{owner}/{repo}/collaborators/{username} | 添加项目成员
[**put_v5_repos_owner_repo_contents_path**](RepositoriesApi.md#put_v5_repos_owner_repo_contents_path) | **PUT** /v5/repos/{owner}/{repo}/contents/{path} | 更新文件


# **delete_v5_repos_owner_repo**
> delete_v5_repos_owner_repo(owner, repo, access_token=access_token)

删除一个项目

删除一个项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除一个项目
    api_instance.delete_v5_repos_owner_repo(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_branches_branch_protection**
> delete_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, access_token=access_token)

取消保护分支的设置

取消保护分支的设置

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
branch = 'branch_example' # str | 分支名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 取消保护分支的设置
    api_instance.delete_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_branches_branch_protection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **branch** | **str**| 分支名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_collaborators_username**
> delete_v5_repos_owner_repo_collaborators_username(owner, repo, username, access_token=access_token)

移除项目成员

移除项目成员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
username = 'username_example' # str | 用户名(username/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 移除项目成员
    api_instance.delete_v5_repos_owner_repo_collaborators_username(owner, repo, username, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_collaborators_username: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **username** | **str**| 用户名(username/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_comments_id**
> delete_v5_repos_owner_repo_comments_id(owner, repo, id, access_token=access_token)

删除Commit评论

删除Commit评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除Commit评论
    api_instance.delete_v5_repos_owner_repo_comments_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_contents_path**
> CommitContent delete_v5_repos_owner_repo_contents_path(owner, repo, path, sha, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)

删除文件

删除文件

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
path = 'path_example' # str | 文件的路径
sha = 'sha_example' # str | 文件被替换的sha值
message = 'message_example' # str | 提交信息
access_token = 'access_token_example' # str | 用户授权码 (optional)
branch = 'branch_example' # str | 分支名称。默认为项目对默认分支 (optional)
committer_name = 'committer_name_example' # str | Committer的名字，默认为当前用户的名字 (optional)
committer_email = 'committer_email_example' # str | Committer的邮箱，默认为当前用户的邮箱 (optional)
author_name = 'author_name_example' # str | Author的名字，默认为当前用户的名字 (optional)
author_email = 'author_email_example' # str | Author的邮箱，默认为当前用户的邮箱 (optional)

try: 
    # 删除文件
    api_response = api_instance.delete_v5_repos_owner_repo_contents_path(owner, repo, path, sha, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_contents_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **path** | **str**| 文件的路径 | 
 **sha** | **str**| 文件被替换的sha值 | 
 **message** | **str**| 提交信息 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **branch** | **str**| 分支名称。默认为项目对默认分支 | [optional] 
 **committer_name** | **str**| Committer的名字，默认为当前用户的名字 | [optional] 
 **committer_email** | **str**| Committer的邮箱，默认为当前用户的邮箱 | [optional] 
 **author_name** | **str**| Author的名字，默认为当前用户的名字 | [optional] 
 **author_email** | **str**| Author的邮箱，默认为当前用户的邮箱 | [optional] 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_keys_id**
> delete_v5_repos_owner_repo_keys_id(owner, repo, id, access_token=access_token)

删除一个项目公钥

删除一个项目公钥

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 公钥 ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除一个项目公钥
    api_instance.delete_v5_repos_owner_repo_keys_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_keys_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 公钥 ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_v5_repos_owner_repo_releases_id**
> delete_v5_repos_owner_repo_releases_id(owner, repo, id, access_token=access_token)

删除项目Release

删除项目Release

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除项目Release
    api_instance.delete_v5_repos_owner_repo_releases_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_v5_repos_owner_repo_releases_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_orgs_org_repos**
> Project get_v5_orgs_org_repos(org, access_token=access_token, type=type, page=page, per_page=per_page)

获取一个组织的项目

获取一个组织的项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
org = 'org_example' # str | 组织的路径(path/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)
type = 'all' # str | 筛选项目的类型，可以是 all, public, private。默认: all (optional) (default to all)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取一个组织的项目
    api_response = api_instance.get_v5_orgs_org_repos(org, access_token=access_token, type=type, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_orgs_org_repos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **str**| 组织的路径(path/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **type** | **str**| 筛选项目的类型，可以是 all, public, private。默认: all | [optional] [default to all]
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo**
> get_v5_repos_owner_repo(owner, repo, access_token=access_token)

列出授权用户的某个项目

列出授权用户的某个项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 列出授权用户的某个项目
    api_instance.get_v5_repos_owner_repo(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_branches**
> list[Branch] get_v5_repos_owner_repo_branches(owner, repo, access_token=access_token)

获取所有分支

获取所有分支

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取所有分支
    api_response = api_instance.get_v5_repos_owner_repo_branches(owner, repo, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_branches: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[Branch]**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_branches_branch**
> CompleteBranch get_v5_repos_owner_repo_branches_branch(owner, repo, branch, access_token=access_token)

获取单个分支

获取单个分支

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
branch = 'branch_example' # str | 分支名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取单个分支
    api_response = api_instance.get_v5_repos_owner_repo_branches_branch(owner, repo, branch, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_branches_branch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **branch** | **str**| 分支名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_collaborators**
> get_v5_repos_owner_repo_collaborators(owner, repo, access_token=access_token)

获取项目的所有成员

获取项目的所有成员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目的所有成员
    api_instance.get_v5_repos_owner_repo_collaborators(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_collaborators: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_collaborators_username**
> get_v5_repos_owner_repo_collaborators_username(owner, repo, username, access_token=access_token)

判断用户是否为项目成员

判断用户是否为项目成员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
username = 'username_example' # str | 用户名(username/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 判断用户是否为项目成员
    api_instance.get_v5_repos_owner_repo_collaborators_username(owner, repo, username, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_collaborators_username: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **username** | **str**| 用户名(username/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_collaborators_username_permission**
> get_v5_repos_owner_repo_collaborators_username_permission(owner, repo, username, access_token=access_token)

查看项目成员的权限

查看项目成员的权限

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
username = 'username_example' # str | 用户名(username/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 查看项目成员的权限
    api_instance.get_v5_repos_owner_repo_collaborators_username_permission(owner, repo, username, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_collaborators_username_permission: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **username** | **str**| 用户名(username/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_comments**
> get_v5_repos_owner_repo_comments(owner, repo, access_token=access_token, page=page, per_page=per_page)

获取项目的Commit评论

获取项目的Commit评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取项目的Commit评论
    api_instance.get_v5_repos_owner_repo_comments(owner, repo, access_token=access_token, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_comments_id**
> get_v5_repos_owner_repo_comments_id(owner, repo, id, access_token=access_token)

获取项目的某条Commit评论

获取项目的某条Commit评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目的某条Commit评论
    api_instance.get_v5_repos_owner_repo_comments_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_commits**
> list[RepoCommit] get_v5_repos_owner_repo_commits(owner, repo, access_token=access_token, sha=sha, path=path, author=author, since=since, until=until, page=page, per_page=per_page)

项目的所有提交

项目的所有提交

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sha = 'sha_example' # str | 提交起始的SHA值或者分支名. 默认: 项目的默认分支 (optional)
path = 'path_example' # str | 包含该文件的提交 (optional)
author = 'author_example' # str | 提交作者的邮箱或个性地址(username/login) (optional)
since = 'since_example' # str | 提交的起始时间，时间格式为 ISO 8601 (optional)
until = 'until_example' # str | 提交的最后时间，时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 项目的所有提交
    api_response = api_instance.get_v5_repos_owner_repo_commits(owner, repo, access_token=access_token, sha=sha, path=path, author=author, since=since, until=until, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_commits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sha** | **str**| 提交起始的SHA值或者分支名. 默认: 项目的默认分支 | [optional] 
 **path** | **str**| 包含该文件的提交 | [optional] 
 **author** | **str**| 提交作者的邮箱或个性地址(username/login) | [optional] 
 **since** | **str**| 提交的起始时间，时间格式为 ISO 8601 | [optional] 
 **until** | **str**| 提交的最后时间，时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[RepoCommit]**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_commits_ref_comments**
> get_v5_repos_owner_repo_commits_ref_comments(owner, repo, ref, access_token=access_token, page=page, per_page=per_page)

获取单个Commit的评论

获取单个Commit的评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
ref = 'ref_example' # str | Commit的Reference
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取单个Commit的评论
    api_instance.get_v5_repos_owner_repo_commits_ref_comments(owner, repo, ref, access_token=access_token, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_commits_ref_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **ref** | **str**| Commit的Reference | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_commits_sha**
> RepoCommit get_v5_repos_owner_repo_commits_sha(owner, repo, sha, access_token=access_token)

项目的某个提交

项目的某个提交

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
sha = 'sha_example' # str | 提交的SHA值或者分支名
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 项目的某个提交
    api_response = api_instance.get_v5_repos_owner_repo_commits_sha(owner, repo, sha, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_commits_sha: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **sha** | **str**| 提交的SHA值或者分支名 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**RepoCommit**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_compare_base___head**
> Compare get_v5_repos_owner_repo_compare_base___head(owner, repo, base, head, access_token=access_token)

两个Commits之间对比的版本差异

两个Commits之间对比的版本差异

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
base = 'base_example' # str | Commit提交的SHA值或者分支名作为对比起点
head = 'head_example' # str | Commit提交的SHA值或者分支名作为对比终点
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 两个Commits之间对比的版本差异
    api_response = api_instance.get_v5_repos_owner_repo_compare_base___head(owner, repo, base, head, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_compare_base___head: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **base** | **str**| Commit提交的SHA值或者分支名作为对比起点 | 
 **head** | **str**| Commit提交的SHA值或者分支名作为对比终点 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Compare**](Compare.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_contents_path**
> list[Content] get_v5_repos_owner_repo_contents_path(owner, repo, path, access_token=access_token, ref=ref)

获取仓库具体路径下的内容

获取仓库具体路径下的内容

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
path = 'path_example' # str | 文件的路径
access_token = 'access_token_example' # str | 用户授权码 (optional)
ref = 'ref_example' # str | 分支、tag或commit。默认: 项目的默认分支(通常是master) (optional)

try: 
    # 获取仓库具体路径下的内容
    api_response = api_instance.get_v5_repos_owner_repo_contents_path(owner, repo, path, access_token=access_token, ref=ref)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_contents_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **path** | **str**| 文件的路径 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **ref** | **str**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | [optional] 

### Return type

[**list[Content]**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_contributors**
> get_v5_repos_owner_repo_contributors(owner, repo, access_token=access_token)

获取项目贡献者

获取项目贡献者

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目贡献者
    api_instance.get_v5_repos_owner_repo_contributors(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_contributors: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_forks**
> get_v5_repos_owner_repo_forks(owner, repo, access_token=access_token, sort=sort, page=page, per_page=per_page)

查看项目的Forks

查看项目的Forks

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'newest' # str | 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) (optional) (default to newest)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 查看项目的Forks
    api_instance.get_v5_repos_owner_repo_forks(owner, repo, access_token=access_token, sort=sort, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_forks: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | [optional] [default to newest]
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_keys**
> list[SSHKey] get_v5_repos_owner_repo_keys(owner, repo, access_token=access_token, page=page, per_page=per_page)

展示项目的公钥

展示项目的公钥

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 展示项目的公钥
    api_response = api_instance.get_v5_repos_owner_repo_keys(owner, repo, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[SSHKey]**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_keys_id**
> SSHKey get_v5_repos_owner_repo_keys_id(owner, repo, id, access_token=access_token)

获取项目的单个公钥

获取项目的单个公钥

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 公钥 ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目的单个公钥
    api_response = api_instance.get_v5_repos_owner_repo_keys_id(owner, repo, id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_keys_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 公钥 ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_pages**
> get_v5_repos_owner_repo_pages(owner, repo, access_token=access_token)

获取Pages信息

获取Pages信息

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取Pages信息
    api_instance.get_v5_repos_owner_repo_pages(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_pages: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_readme**
> Content get_v5_repos_owner_repo_readme(owner, repo, access_token=access_token, ref=ref)

获取仓库README

获取仓库README

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
ref = 'ref_example' # str | 分支、tag或commit。默认: 项目的默认分支(通常是master) (optional)

try: 
    # 获取仓库README
    api_response = api_instance.get_v5_repos_owner_repo_readme(owner, repo, access_token=access_token, ref=ref)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_readme: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **ref** | **str**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | [optional] 

### Return type

[**Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_releases**
> list[Release] get_v5_repos_owner_repo_releases(owner, repo, access_token=access_token, page=page, per_page=per_page)

获取项目的所有Releases

获取项目的所有Releases

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取项目的所有Releases
    api_response = api_instance.get_v5_repos_owner_repo_releases(owner, repo, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_releases: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[Release]**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_releases_id**
> Release get_v5_repos_owner_repo_releases_id(owner, repo, id, access_token=access_token)

获取项目的单个Releases

获取项目的单个Releases

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 发行版本的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目的单个Releases
    api_response = api_instance.get_v5_repos_owner_repo_releases_id(owner, repo, id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_releases_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 发行版本的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_releases_latest**
> Release get_v5_repos_owner_repo_releases_latest(owner, repo, access_token=access_token)

获取项目的最后更新的Release

获取项目的最后更新的Release

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目的最后更新的Release
    api_response = api_instance.get_v5_repos_owner_repo_releases_latest(owner, repo, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_releases_latest: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_releases_tags_tag**
> Release get_v5_repos_owner_repo_releases_tags_tag(owner, repo, tag, access_token=access_token)

根据Tag名称获取项目的Release

根据Tag名称获取项目的Release

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
tag = 'tag_example' # str | Tag 名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 根据Tag名称获取项目的Release
    api_response = api_instance.get_v5_repos_owner_repo_releases_tags_tag(owner, repo, tag, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_releases_tags_tag: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **tag** | **str**| Tag 名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_tags**
> get_v5_repos_owner_repo_tags(owner, repo, access_token=access_token)

列出项目所有的tags

列出项目所有的tags

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 列出项目所有的tags
    api_instance.get_v5_repos_owner_repo_tags(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_repos_owner_repo_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_user_repos**
> get_v5_user_repos(access_token=access_token, visibility=visibility, affiliation=affiliation, type=type, sort=sort, direction=direction, page=page, per_page=per_page)

列出授权用户的所有项目

列出授权用户的所有项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
access_token = 'access_token_example' # str | 用户授权码 (optional)
visibility = 'visibility_example' # str | 公开(public)、私有(private)或者所有(all)，默认: 所有(all) (optional)
affiliation = 'affiliation_example' # str | owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member (optional)
type = 'type_example' # str | 不能与visibility或affiliation参数一并使用，否则会报422错误 (optional)
sort = 'full_name' # str | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name (optional) (default to full_name)
direction = 'direction_example' # str | 如果sort参数为full_name，用升序(asc)。否则降序(desc) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 列出授权用户的所有项目
    api_instance.get_v5_user_repos(access_token=access_token, visibility=visibility, affiliation=affiliation, type=type, sort=sort, direction=direction, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_user_repos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_token** | **str**| 用户授权码 | [optional] 
 **visibility** | **str**| 公开(public)、私有(private)或者所有(all)，默认: 所有(all) | [optional] 
 **affiliation** | **str**| owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | [optional] 
 **type** | **str**| 不能与visibility或affiliation参数一并使用，否则会报422错误 | [optional] 
 **sort** | **str**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [optional] [default to full_name]
 **direction** | **str**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_users_username_repos**
> get_v5_users_username_repos(username, access_token=access_token, type=type, sort=sort, direction=direction, page=page, per_page=per_page)

获取某个用户的公开项目

获取某个用户的公开项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
username = 'username_example' # str | 用户名(username/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)
type = 'all' # str | 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all) (optional) (default to all)
sort = 'full_name' # str | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name (optional) (default to full_name)
direction = 'direction_example' # str | 如果sort参数为full_name，用升序(asc)。否则降序(desc) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取某个用户的公开项目
    api_instance.get_v5_users_username_repos(username, access_token=access_token, type=type, sort=sort, direction=direction, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_v5_users_username_repos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| 用户名(username/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **type** | **str**| 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all) | [optional] [default to all]
 **sort** | **str**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [optional] [default to full_name]
 **direction** | **str**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo**
> patch_v5_repos_owner_repo(owner, repo, name, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, default_branch=default_branch)

更新项目设置

更新项目设置

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
name = 'name_example' # str | 项目名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
description = 'description_example' # str | 项目描述 (optional)
homepage = 'homepage_example' # str | 项目所在地址 (optional)
has_issues = true # bool | 允许提Issue与否。默认: 允许(true) (optional) (default to true)
has_wiki = true # bool | 提供Wiki与否。默认: 提供(true) (optional) (default to true)
private = true # bool | 项目公开或私有。 (optional)
default_branch = 'default_branch_example' # str | 更新默认分支 (optional)

try: 
    # 更新项目设置
    api_instance.patch_v5_repos_owner_repo(owner, repo, name, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, default_branch=default_branch)
except ApiException as e:
    print("Exception when calling RepositoriesApi->patch_v5_repos_owner_repo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **name** | **str**| 项目名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **description** | **str**| 项目描述 | [optional] 
 **homepage** | **str**| 项目所在地址 | [optional] 
 **has_issues** | **bool**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **has_wiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **private** | **bool**| 项目公开或私有。 | [optional] 
 **default_branch** | **str**| 更新默认分支 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_comments_id**
> patch_v5_repos_owner_repo_comments_id(owner, repo, id, body, access_token=access_token)

更新Commit评论

更新Commit评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
body = 'body_example' # str | 评论的内容
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 更新Commit评论
    api_instance.patch_v5_repos_owner_repo_comments_id(owner, repo, id, body, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->patch_v5_repos_owner_repo_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **body** | **str**| 评论的内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_releases_id**
> Release patch_v5_repos_owner_repo_releases_id(owner, repo, tag_name, name, body, id, access_token=access_token, prerelease=prerelease)

更新项目Release

更新项目Release

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
tag_name = 'tag_name_example' # str | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
name = 'name_example' # str | Release 名称
body = 'body_example' # str | Release 描述
id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
prerelease = true # bool | 是否为预览版本。默认: false（非预览版本） (optional)

try: 
    # 更新项目Release
    api_response = api_instance.patch_v5_repos_owner_repo_releases_id(owner, repo, tag_name, name, body, id, access_token=access_token, prerelease=prerelease)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->patch_v5_repos_owner_repo_releases_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **tag_name** | **str**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
 **name** | **str**| Release 名称 | 
 **body** | **str**| Release 描述 | 
 **id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **prerelease** | **bool**| 是否为预览版本。默认: false（非预览版本） | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_orgs_org_repos**
> post_v5_orgs_org_repos(name, org, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, auto_init=auto_init, gitignore_template=gitignore_template, license_template=license_template)

创建组织项目

创建组织项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
name = 'name_example' # str | 项目名称
org = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
description = 'description_example' # str | 项目描述 (optional)
homepage = 'homepage_example' # str | 项目所在地址 (optional)
has_issues = true # bool | 允许提Issue与否。默认: 允许(true) (optional) (default to true)
has_wiki = true # bool | 提供Wiki与否。默认: 提供(true) (optional) (default to true)
private = true # bool | 项目公开或私有。默认: 公开(false) (optional)
auto_init = true # bool | 值为true时则会用README初始化仓库。默认: 不初始化(false) (optional)
gitignore_template = 'gitignore_template_example' # str | Git Ingore模版 (optional)
license_template = 'license_template_example' # str | Git Ingore模版 (optional)

try: 
    # 创建组织项目
    api_instance.post_v5_orgs_org_repos(name, org, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, auto_init=auto_init, gitignore_template=gitignore_template, license_template=license_template)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_orgs_org_repos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| 项目名称 | 
 **org** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **description** | **str**| 项目描述 | [optional] 
 **homepage** | **str**| 项目所在地址 | [optional] 
 **has_issues** | **bool**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **has_wiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **private** | **bool**| 项目公开或私有。默认: 公开(false) | [optional] 
 **auto_init** | **bool**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional] 
 **gitignore_template** | **str**| Git Ingore模版 | [optional] 
 **license_template** | **str**| Git Ingore模版 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_commits_sha_comments**
> post_v5_repos_owner_repo_commits_sha_comments(owner, repo, sha, body, access_token=access_token, path=path, position=position)

创建Commit评论

创建Commit评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
sha = 'sha_example' # str | 评论的sha值
body = 'body_example' # str | 评论的内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
path = 'path_example' # str | 文件的相对路径 (optional)
position = 56 # int | Diff的相对行数 (optional)

try: 
    # 创建Commit评论
    api_instance.post_v5_repos_owner_repo_commits_sha_comments(owner, repo, sha, body, access_token=access_token, path=path, position=position)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_commits_sha_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **sha** | **str**| 评论的sha值 | 
 **body** | **str**| 评论的内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **path** | **str**| 文件的相对路径 | [optional] 
 **position** | **int**| Diff的相对行数 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_contents_path**
> CommitContent post_v5_repos_owner_repo_contents_path(owner, repo, path, content, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)

新建文件

新建文件

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
path = 'path_example' # str | 文件的路径
content = 'content_example' # str | 文件内容, 要用base64编码
message = 'message_example' # str | 提交信息
access_token = 'access_token_example' # str | 用户授权码 (optional)
branch = 'branch_example' # str | 分支名称。默认为项目对默认分支 (optional)
committer_name = 'committer_name_example' # str | Committer的名字，默认为当前用户的名字 (optional)
committer_email = 'committer_email_example' # str | Committer的邮箱，默认为当前用户的邮箱 (optional)
author_name = 'author_name_example' # str | Author的名字，默认为当前用户的名字 (optional)
author_email = 'author_email_example' # str | Author的邮箱，默认为当前用户的邮箱 (optional)

try: 
    # 新建文件
    api_response = api_instance.post_v5_repos_owner_repo_contents_path(owner, repo, path, content, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_contents_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **path** | **str**| 文件的路径 | 
 **content** | **str**| 文件内容, 要用base64编码 | 
 **message** | **str**| 提交信息 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **branch** | **str**| 分支名称。默认为项目对默认分支 | [optional] 
 **committer_name** | **str**| Committer的名字，默认为当前用户的名字 | [optional] 
 **committer_email** | **str**| Committer的邮箱，默认为当前用户的邮箱 | [optional] 
 **author_name** | **str**| Author的名字，默认为当前用户的名字 | [optional] 
 **author_email** | **str**| Author的邮箱，默认为当前用户的邮箱 | [optional] 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_forks**
> post_v5_repos_owner_repo_forks(owner, repo, access_token=access_token, organization=organization)

Fork一个项目

Fork一个项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
organization = 'organization_example' # str | 组织地址，不填写默认Fork到用户个性地址 (optional)

try: 
    # Fork一个项目
    api_instance.post_v5_repos_owner_repo_forks(owner, repo, access_token=access_token, organization=organization)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_forks: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **organization** | **str**| 组织地址，不填写默认Fork到用户个性地址 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_keys**
> SSHKey post_v5_repos_owner_repo_keys(owner, repo, access_token=access_token)

为项目添加公钥

为项目添加公钥

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 为项目添加公钥
    api_response = api_instance.post_v5_repos_owner_repo_keys(owner, repo, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_pages_builds**
> post_v5_repos_owner_repo_pages_builds(owner, repo, access_token=access_token)

请求建立Pages

请求建立Pages

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 请求建立Pages
    api_instance.post_v5_repos_owner_repo_pages_builds(owner, repo, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_pages_builds: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_releases**
> Release post_v5_repos_owner_repo_releases(owner, repo, tag_name, name, body, target_commitish, access_token=access_token, prerelease=prerelease)

创建项目Release

创建项目Release

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
tag_name = 'tag_name_example' # str | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
name = 'name_example' # str | Release 名称
body = 'body_example' # str | Release 描述
target_commitish = 'target_commitish_example' # str | 分支名称或者commit SHA, 默认是当前默认分支
access_token = 'access_token_example' # str | 用户授权码 (optional)
prerelease = true # bool | 是否为预览版本。默认: false（非预览版本） (optional)

try: 
    # 创建项目Release
    api_response = api_instance.post_v5_repos_owner_repo_releases(owner, repo, tag_name, name, body, target_commitish, access_token=access_token, prerelease=prerelease)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_repos_owner_repo_releases: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **tag_name** | **str**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
 **name** | **str**| Release 名称 | 
 **body** | **str**| Release 描述 | 
 **target_commitish** | **str**| 分支名称或者commit SHA, 默认是当前默认分支 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **prerelease** | **bool**| 是否为预览版本。默认: false（非预览版本） | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_user_repos**
> post_v5_user_repos(name, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, auto_init=auto_init, gitignore_template=gitignore_template, license_template=license_template)

创建一个项目

创建一个项目

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
name = 'name_example' # str | 项目名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
description = 'description_example' # str | 项目描述 (optional)
homepage = 'homepage_example' # str | 项目所在地址 (optional)
has_issues = true # bool | 允许提Issue与否。默认: 允许(true) (optional) (default to true)
has_wiki = true # bool | 提供Wiki与否。默认: 提供(true) (optional) (default to true)
private = true # bool | 项目公开或私有。默认: 公开(false) (optional)
auto_init = true # bool | 值为true时则会用README初始化仓库。默认: 不初始化(false) (optional)
gitignore_template = 'gitignore_template_example' # str | Git Ingore模版 (optional)
license_template = 'license_template_example' # str | License模版 (optional)

try: 
    # 创建一个项目
    api_instance.post_v5_user_repos(name, access_token=access_token, description=description, homepage=homepage, has_issues=has_issues, has_wiki=has_wiki, private=private, auto_init=auto_init, gitignore_template=gitignore_template, license_template=license_template)
except ApiException as e:
    print("Exception when calling RepositoriesApi->post_v5_user_repos: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| 项目名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **description** | **str**| 项目描述 | [optional] 
 **homepage** | **str**| 项目所在地址 | [optional] 
 **has_issues** | **bool**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **has_wiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **private** | **bool**| 项目公开或私有。默认: 公开(false) | [optional] 
 **auto_init** | **bool**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional] 
 **gitignore_template** | **str**| Git Ingore模版 | [optional] 
 **license_template** | **str**| License模版 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_v5_repos_owner_repo_branches_branch_protection**
> CompleteBranch put_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, access_token=access_token)

设置分支保护

设置分支保护

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
branch = 'branch_example' # str | 分支名称
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 设置分支保护
    api_response = api_instance.put_v5_repos_owner_repo_branches_branch_protection(owner, repo, branch, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->put_v5_repos_owner_repo_branches_branch_protection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **branch** | **str**| 分支名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_v5_repos_owner_repo_collaborators_username**
> put_v5_repos_owner_repo_collaborators_username(owner, repo, username, permission, access_token=access_token)

添加项目成员

添加项目成员

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
username = 'username_example' # str | 用户名(username/login)
permission = 'push' # str | 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push (default to push)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 添加项目成员
    api_instance.put_v5_repos_owner_repo_collaborators_username(owner, repo, username, permission, access_token=access_token)
except ApiException as e:
    print("Exception when calling RepositoriesApi->put_v5_repos_owner_repo_collaborators_username: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **username** | **str**| 用户名(username/login) | 
 **permission** | **str**| 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push | [default to push]
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_v5_repos_owner_repo_contents_path**
> CommitContent put_v5_repos_owner_repo_contents_path(owner, repo, path, content, sha, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)

更新文件

更新文件

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.RepositoriesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
path = 'path_example' # str | 文件的路径
content = 'content_example' # str | 文件内容, 要用base64编码
sha = 'sha_example' # str | 文件被替换的sha值
message = 'message_example' # str | 提交信息
access_token = 'access_token_example' # str | 用户授权码 (optional)
branch = 'branch_example' # str | 分支名称。默认为项目对默认分支 (optional)
committer_name = 'committer_name_example' # str | Committer的名字，默认为当前用户的名字 (optional)
committer_email = 'committer_email_example' # str | Committer的邮箱，默认为当前用户的邮箱 (optional)
author_name = 'author_name_example' # str | Author的名字，默认为当前用户的名字 (optional)
author_email = 'author_email_example' # str | Author的邮箱，默认为当前用户的邮箱 (optional)

try: 
    # 更新文件
    api_response = api_instance.put_v5_repos_owner_repo_contents_path(owner, repo, path, content, sha, message, access_token=access_token, branch=branch, committer_name=committer_name, committer_email=committer_email, author_name=author_name, author_email=author_email)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->put_v5_repos_owner_repo_contents_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **path** | **str**| 文件的路径 | 
 **content** | **str**| 文件内容, 要用base64编码 | 
 **sha** | **str**| 文件被替换的sha值 | 
 **message** | **str**| 提交信息 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **branch** | **str**| 分支名称。默认为项目对默认分支 | [optional] 
 **committer_name** | **str**| Committer的名字，默认为当前用户的名字 | [optional] 
 **committer_email** | **str**| Committer的邮箱，默认为当前用户的邮箱 | [optional] 
 **author_name** | **str**| Author的名字，默认为当前用户的名字 | [optional] 
 **author_email** | **str**| Author的邮箱，默认为当前用户的邮箱 | [optional] 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


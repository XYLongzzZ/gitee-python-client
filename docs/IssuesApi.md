# gitee_client.IssuesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_v5_repos_owner_repo_issues_comments_id**](IssuesApi.md#delete_v5_repos_owner_repo_issues_comments_id) | **DELETE** /v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
[**get_v5_issues**](IssuesApi.md#get_v5_issues) | **GET** /v5/issues | 获取当前授权用户的所有Issue
[**get_v5_orgs_org_issues**](IssuesApi.md#get_v5_orgs_org_issues) | **GET** /v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
[**get_v5_repos_owner_repo_issues**](IssuesApi.md#get_v5_repos_owner_repo_issues) | **GET** /v5/repos/{owner}/{repo}/issues | 项目的所有Issues
[**get_v5_repos_owner_repo_issues_comments**](IssuesApi.md#get_v5_repos_owner_repo_issues_comments) | **GET** /v5/repos/{owner}/{repo}/issues/comments | 获取项目所有Issue的评论
[**get_v5_repos_owner_repo_issues_comments_id**](IssuesApi.md#get_v5_repos_owner_repo_issues_comments_id) | **GET** /v5/repos/{owner}/{repo}/issues/comments/{id} | 获取项目Issue某条评论
[**get_v5_repos_owner_repo_issues_number**](IssuesApi.md#get_v5_repos_owner_repo_issues_number) | **GET** /v5/repos/{owner}/{repo}/issues/{number} | 项目的某个Issue
[**get_v5_repos_owner_repo_issues_number_comments**](IssuesApi.md#get_v5_repos_owner_repo_issues_number_comments) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/comments | 获取项目某个Issue所有的评论
[**get_v5_user_issues**](IssuesApi.md#get_v5_user_issues) | **GET** /v5/user/issues | 获取当前授权用户的所有Issues
[**patch_v5_repos_owner_repo_issues_comments_id**](IssuesApi.md#patch_v5_repos_owner_repo_issues_comments_id) | **PATCH** /v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
[**patch_v5_repos_owner_repo_issues_number**](IssuesApi.md#patch_v5_repos_owner_repo_issues_number) | **PATCH** /v5/repos/{owner}/{repo}/issues/{number} | 更新Issue
[**post_v5_repos_owner_repo_issues**](IssuesApi.md#post_v5_repos_owner_repo_issues) | **POST** /v5/repos/{owner}/{repo}/issues | 创建Issue
[**post_v5_repos_owner_repo_issues_number_comments**](IssuesApi.md#post_v5_repos_owner_repo_issues_number_comments) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论


# **delete_v5_repos_owner_repo_issues_comments_id**
> delete_v5_repos_owner_repo_issues_comments_id(owner, repo, id, access_token=access_token)

删除Issue某条评论

删除Issue某条评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 删除Issue某条评论
    api_instance.delete_v5_repos_owner_repo_issues_comments_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_v5_repos_owner_repo_issues_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_issues**
> list[Issue] get_v5_issues(access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)

获取当前授权用户的所有Issue

获取当前授权用户的所有Issue

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
access_token = 'access_token_example' # str | 用户授权码 (optional)
filter = 'assigned' # str | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional) (default to assigned)
state = 'open' # str | Issue的状态: open, closed, or all。 默认: open (optional) (default to open)
labels = 'labels_example' # str | 用逗号分开的标签。如: bug,performance (optional)
sort = 'created' # str | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional) (default to created)
direction = 'desc' # str | 排序方式: 升序(asc)，降序(desc)。默认: desc (optional) (default to desc)
since = 'since_example' # str | 起始的更新时间，要求时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取当前授权用户的所有Issue
    api_response = api_instance.get_v5_issues(access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_token** | **str**| 用户授权码 | [optional] 
 **filter** | **str**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned]
 **state** | **str**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open]
 **labels** | **str**| 用逗号分开的标签。如: bug,performance | [optional] 
 **sort** | **str**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created]
 **direction** | **str**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc]
 **since** | **str**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_orgs_org_issues**
> list[Issue] get_v5_orgs_org_issues(org, access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)

获取当前用户某个组织的Issues

获取当前用户某个组织的Issues

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
org = 'org_example' # str | 组织的路径(path/login)
access_token = 'access_token_example' # str | 用户授权码 (optional)
filter = 'assigned' # str | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional) (default to assigned)
state = 'open' # str | Issue的状态: open, closed, or all。 默认: open (optional) (default to open)
labels = 'labels_example' # str | 用逗号分开的标签。如: bug,performance (optional)
sort = 'created' # str | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional) (default to created)
direction = 'desc' # str | 排序方式: 升序(asc)，降序(desc)。默认: desc (optional) (default to desc)
since = 'since_example' # str | 起始的更新时间，要求时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取当前用户某个组织的Issues
    api_response = api_instance.get_v5_orgs_org_issues(org, access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_orgs_org_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **str**| 组织的路径(path/login) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **filter** | **str**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned]
 **state** | **str**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open]
 **labels** | **str**| 用逗号分开的标签。如: bug,performance | [optional] 
 **sort** | **str**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created]
 **direction** | **str**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc]
 **since** | **str**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues**
> list[Issue] get_v5_repos_owner_repo_issues(owner, repo, access_token=access_token, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page, milestone=milestone, assignee=assignee, creator=creator)

项目的所有Issues

项目的所有Issues

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'open' # str | Issue的状态: open, closed, or all。 默认: open (optional) (default to open)
labels = 'labels_example' # str | 用逗号分开的标签。如: bug,performance (optional)
sort = 'created' # str | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional) (default to created)
direction = 'desc' # str | 排序方式: 升序(asc)，降序(desc)。默认: desc (optional) (default to desc)
since = 'since_example' # str | 起始的更新时间，要求时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)
milestone = 'milestone_example' # str | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 (optional)
assignee = 'assignee_example' # str | 用户的username。 none为没指派者, *为所有带有指派者的 (optional)
creator = 'creator_example' # str | 创建Issues的用户username (optional)

try: 
    # 项目的所有Issues
    api_response = api_instance.get_v5_repos_owner_repo_issues(owner, repo, access_token=access_token, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page, milestone=milestone, assignee=assignee, creator=creator)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_repos_owner_repo_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open]
 **labels** | **str**| 用逗号分开的标签。如: bug,performance | [optional] 
 **sort** | **str**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created]
 **direction** | **str**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc]
 **since** | **str**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]
 **milestone** | **str**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional] 
 **assignee** | **str**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional] 
 **creator** | **str**| 创建Issues的用户username | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues_comments**
> get_v5_repos_owner_repo_issues_comments(owner, repo, access_token=access_token, sort=sort, direction=direction, since=since, page=page, per_page=per_page)

获取项目所有Issue的评论

获取项目所有Issue的评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'created' # str | Either created or updated. Default: created (optional) (default to created)
direction = 'asc' # str | Either asc or desc. Ignored without the sort parameter. (optional) (default to asc)
since = 'since_example' # str | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取项目所有Issue的评论
    api_instance.get_v5_repos_owner_repo_issues_comments(owner, repo, access_token=access_token, sort=sort, direction=direction, since=since, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_repos_owner_repo_issues_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| Either created or updated. Default: created | [optional] [default to created]
 **direction** | **str**| Either asc or desc. Ignored without the sort parameter. | [optional] [default to asc]
 **since** | **str**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues_comments_id**
> get_v5_repos_owner_repo_issues_comments_id(owner, repo, id, access_token=access_token)

获取项目Issue某条评论

获取项目Issue某条评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 获取项目Issue某条评论
    api_instance.get_v5_repos_owner_repo_issues_comments_id(owner, repo, id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_repos_owner_repo_issues_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues_number**
> Issue get_v5_repos_owner_repo_issues_number(owner, repo, number, access_token=access_token)

项目的某个Issue

项目的某个Issue

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 项目的某个Issue
    api_response = api_instance.get_v5_repos_owner_repo_issues_number(owner, repo, number, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_repos_owner_repo_issues_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_repos_owner_repo_issues_number_comments**
> get_v5_repos_owner_repo_issues_number_comments(owner, repo, number, access_token=access_token, since=since, page=page, per_page=per_page)

获取项目某个Issue所有的评论

获取项目某个Issue所有的评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
access_token = 'access_token_example' # str | 用户授权码 (optional)
since = 'since_example' # str | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取项目某个Issue所有的评论
    api_instance.get_v5_repos_owner_repo_issues_number_comments(owner, repo, number, access_token=access_token, since=since, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_repos_owner_repo_issues_number_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **since** | **str**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v5_user_issues**
> list[Issue] get_v5_user_issues(access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)

获取当前授权用户的所有Issues

获取当前授权用户的所有Issues

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
access_token = 'access_token_example' # str | 用户授权码 (optional)
filter = 'assigned' # str | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional) (default to assigned)
state = 'open' # str | Issue的状态: open, closed, or all。 默认: open (optional) (default to open)
labels = 'labels_example' # str | 用逗号分开的标签。如: bug,performance (optional)
sort = 'created' # str | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional) (default to created)
direction = 'desc' # str | 排序方式: 升序(asc)，降序(desc)。默认: desc (optional) (default to desc)
since = 'since_example' # str | 起始的更新时间，要求时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 20 # int | 每页的数量 (optional) (default to 20)

try: 
    # 获取当前授权用户的所有Issues
    api_response = api_instance.get_v5_user_issues(access_token=access_token, filter=filter, state=state, labels=labels, sort=sort, direction=direction, since=since, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_v5_user_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_token** | **str**| 用户授权码 | [optional] 
 **filter** | **str**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned]
 **state** | **str**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open]
 **labels** | **str**| 用逗号分开的标签。如: bug,performance | [optional] 
 **sort** | **str**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created]
 **direction** | **str**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc]
 **since** | **str**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量 | [optional] [default to 20]

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_issues_comments_id**
> patch_v5_repos_owner_repo_issues_comments_id(owner, repo, id, body, access_token=access_token)

更新Issue某条评论

更新Issue某条评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
id = 56 # int | 评论的ID
body = 'body_example' # str | The contents of the comment.
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 更新Issue某条评论
    api_instance.patch_v5_repos_owner_repo_issues_comments_id(owner, repo, id, body, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->patch_v5_repos_owner_repo_issues_comments_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **id** | **int**| 评论的ID | 
 **body** | **str**| The contents of the comment. | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_v5_repos_owner_repo_issues_number**
> Issue patch_v5_repos_owner_repo_issues_number(owner, repo, number, title, access_token=access_token, state=state, body=body, assignee=assignee, milestone=milestone, labels=labels)

更新Issue

更新Issue

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
title = 'title_example' # str | Issue标题
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'state_example' # str | Issue 状态，open、started、closed 或 approved (optional)
body = 'body_example' # str | Issue描述 (optional)
assignee = 'assignee_example' # str | Issue负责人的username (optional)
milestone = 56 # int | 所属里程碑的number(第几个) (optional)
labels = ['labels_example'] # list[str] | 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance (optional)

try: 
    # 更新Issue
    api_response = api_instance.patch_v5_repos_owner_repo_issues_number(owner, repo, number, title, access_token=access_token, state=state, body=body, assignee=assignee, milestone=milestone, labels=labels)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->patch_v5_repos_owner_repo_issues_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **title** | **str**| Issue标题 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| Issue 状态，open、started、closed 或 approved | [optional] 
 **body** | **str**| Issue描述 | [optional] 
 **assignee** | **str**| Issue负责人的username | [optional] 
 **milestone** | **int**| 所属里程碑的number(第几个) | [optional] 
 **labels** | [**list[str]**](str.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_issues**
> Issue post_v5_repos_owner_repo_issues(owner, repo, title, access_token=access_token, body=body, assignee=assignee, milestone=milestone, labels=labels)

创建Issue

创建Issue

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
title = 'title_example' # str | Issue标题
access_token = 'access_token_example' # str | 用户授权码 (optional)
body = 'body_example' # str | Issue描述 (optional)
assignee = 'assignee_example' # str | Issue负责人的username (optional)
milestone = 56 # int | 所属里程碑的number(第几个) (optional)
labels = ['labels_example'] # list[str] | 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance (optional)

try: 
    # 创建Issue
    api_response = api_instance.post_v5_repos_owner_repo_issues(owner, repo, title, access_token=access_token, body=body, assignee=assignee, milestone=milestone, labels=labels)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_v5_repos_owner_repo_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **title** | **str**| Issue标题 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **body** | **str**| Issue描述 | [optional] 
 **assignee** | **str**| Issue负责人的username | [optional] 
 **milestone** | **int**| 所属里程碑的number(第几个) | [optional] 
 **labels** | [**list[str]**](str.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_v5_repos_owner_repo_issues_number_comments**
> post_v5_repos_owner_repo_issues_number_comments(owner, repo, number, body, access_token=access_token)

创建某个Issue评论

创建某个Issue评论

### Example 
```python
from __future__ import print_function
import time
import gitee_client
from gitee_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_client.IssuesApi()
owner = 'owner_example' # str | 用户名(username/login)
repo = 'repo_example' # str | 项目路径(path)
number = 'number_example' # str | Issue 编号(区分大小写)
body = 'body_example' # str | The contents of the comment.
access_token = 'access_token_example' # str | 用户授权码 (optional)

try: 
    # 创建某个Issue评论
    api_instance.post_v5_repos_owner_repo_issues_number_comments(owner, repo, number, body, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->post_v5_repos_owner_repo_issues_number_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| 用户名(username/login) | 
 **repo** | **str**| 项目路径(path) | 
 **number** | **str**| Issue 编号(区分大小写) | 
 **body** | **str**| The contents of the comment. | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

